package com.example.pruebaMaven.simulationTests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.HandlerPescador;
import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;
import edu.mondragon.mufish.simulacion.SimuladorPescador;
import edu.mondragon.mufish.simulacion.Zona;

public class TestControladorZonas
{
    ControladorDeZonas contZo;
    @Before
    public void beforeClass(){
        contZo=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
    }
    @Test
    public void testGetListaZonas(){
        assertEquals(contZo.getListaZonas(), contZo.listaZonas);
    }
    @Test
    public void testGetListaZonasPeligro(){
        assertEquals(contZo.getListaZonasPeligro(), contZo.listaZonasPeligro);
    }
    @Test
    public void testActualizarLista(){
        contZo.actualizarLista();
        assertEquals(contZo.listaZonas, ControladorDeZonas.staticListaZonas);
        assertEquals(contZo.listaZonasPeligro, ControladorDeZonas.staticListaZonasPeligro);
    }
    @Test
    public void testGetStaticListaZonas(){
        assertEquals(contZo.getStaticListaZonas(), ControladorDeZonas.staticListaZonas);
    }
    @Test
    public void testGetStaticListaZonasPeligro(){
        assertEquals(contZo.getStaticListaZonasPeligro(), ControladorDeZonas.staticListaZonasPeligro);
    }
    @Test
    public void testGuardarZonaPorBarcoDeUsuario(){
        Zona z=new Zona(1,new Posicion(1f,1f),100,"Bien","Luna",1);
        try {
            contZo.annadirZona(z);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            contZo.guardarZonaPorBarcoDeUsuario(1, 1, z);
            contZo.guardarZonaPorBarcoDeUsuario(1, 2, z);
            contZo.guardarZonaPorBarcoDeUsuario(1, 1, z);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testGetMapaZonasPorBarcoDeUsuario(){
        Map<Integer, Map<Integer, Zona>> mapa=new HashMap<Integer, Map<Integer, Zona>>();
        assertEquals(contZo.getMapaZonasPorBarcoDeUsuario(),mapa );
    }
    @Test
    public void testListeners(){
        PropertyChangeListener listener=new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                
            }
        };
        contZo.annadirListener(listener);
        contZo.eliminarListener(listener);
    }
    @Test
    public void testPedirZona(){
        Zona z=new Zona(1,new Posicion(1f,1f),100,"Bien","Lubina",1000);
        Zona zo=new Zona(1,new Posicion(1f,1f),100,"Bien","Luna",1);
        SimuladorPescador pescador2=new SimuladorPescador(new HandlerPescador(), 1, 1,new Barco(0,new Posicion(1f,1f),"Pescador"),new Posicion(4f,5f),contZo);
        Zona z3=null;
        pescador2.setPez("Luna");
        SimuladorPescador pescador=new SimuladorPescador(new HandlerPescador(), 1, 1,new Barco(0,new Posicion(1f,1f),"Pescador"),new Posicion(4f,5f),contZo);
        Zona z2=null;
        try {
        	contZo.annadirZona(z);
        	contZo.annadirZona(zo);
			z2=contZo.pedirZona(pescador);
			z3=contZo.pedirZona(pescador2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        assertEquals(z,z2);
        assertNull(z3);
    }
    @Test
    public void testPedirZonaUsuario(){
        Zona z=new Zona(1,new Posicion(1f,1f),100,"Bien","Luna",1100);
        Zona zo=new Zona(1,new Posicion(1f,1f),100,"Bien","Tiburon",11);
        Zona z2=null;
        Zona z3=null;
        try {
            contZo.annadirZona(z);
            contZo.annadirZona(zo);
            z3=contZo.pedirZonaUsuario("Tiburon", 100);
            z2=contZo.pedirZonaUsuario("Luna", 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(z,z2);
        assertNull(z3);
    }
    @Test
    public void testVerZonas(){
        try {
            contZo.verZonas();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Test
    public void testVerZonasEnFalta(){
        contZo.verZonasEnFalta();
    }
    @Test
    public void testRevisarEstadoZonas(){
        Zona z=new Zona(1,new Posicion(1f,1f),100,"Bien","Luna",1);
        try {
            contZo.listaZonasEnFalta.add("Lubina");
            contZo.annadirZona(z);
            contZo.revisarEstadosZonas();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testZonaPeligro(){
        try {
            Zona z=new Zona(1, new Posicion(1f,1f), 1, "Contaminado", null, -1);
            contZo.annadirZonaPeligro(z);
            contZo.eliminarZonaPeligro(z);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testZona(){
        try {
            Zona z=new Zona(1, new Posicion(1f,1f), 1, "Contaminado", "Lubina", 100);
            contZo.annadirZona(z);
            contZo.eliminarZona(z);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
}
