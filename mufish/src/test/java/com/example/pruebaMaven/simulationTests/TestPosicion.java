package com.example.pruebaMaven.simulationTests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.Zona;

/**
 * Unit test for simple App.
 */
public class TestPosicion
{
    Posicion posicion;
    @Before
    public void beforeClass(){
        posicion=new Posicion(1f,1f);
    }
    @Test
    public void testCrearPosicion()
    {
        assertTrue(posicion.equals(new Posicion(1f,1f)));
        assertFalse(posicion.equals(new Posicion(2f,1f)));
        assertFalse(posicion.equals(new Posicion(1f,2f)));
    }
    @Test
    public void testGetPosX()
    {
        assertEquals(posicion.getPosX(), 1);
    }
    @Test
    public void testIncX()
    {
        posicion.incX(1);
        assertEquals(posicion.getPosX(), 2);
    }
    @Test
    public void testSetPosX()
    {
        posicion.setPosX(2);
        assertEquals(posicion.getPosX(), 2f);
    }
    @Test
    public void testGetPosY()
    {
        assertEquals(posicion.getPosY(), 1);
    }
    @Test
    public void testIncY()
    {
        posicion.incY(1);
        assertEquals(posicion.getPosY(), 2);
    }
   
    @Test
    public void testSetPosY()
    {
        posicion.setPosY(2);
        assertEquals(posicion.getPosY(), 2f);
    }
    @Test
    public void testToString()
    {
        assertEquals(posicion.toString(),"(1.0,1.0)");
    }
    @Test
    public void testDistancia()
    {
        Posicion posicion2=new Posicion(4,5);
        assertEquals(posicion.distancia(posicion2),5);
    }
    @Test
    public void testEquals()
    {
        
        assertTrue(posicion.equals(new Posicion(1f,1f)));
        assertFalse(posicion.equals(new Posicion(2f,1f)));
        assertFalse(posicion.equals(new Posicion(1f,2f)));
        assertFalse(posicion.equals(null));
        assertFalse(posicion.equals(new Zona(1,new Posicion(1f,1f),1,"a","b",1)));
    }
    @Test
    public void testHashCode()
    {
        assertEquals(posicion.hashCode(),new Posicion(1f,1f).hashCode());
    }
}
