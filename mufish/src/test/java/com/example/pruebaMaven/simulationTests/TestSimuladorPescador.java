package com.example.pruebaMaven.simulationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.HandlerPescador;
import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorPescador;
import edu.mondragon.mufish.simulacion.Zona;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;


/**
 * Unit test for simple App.
 */
public class TestSimuladorPescador
{
    SimuladorPescador pescador;
    @Before
    public void beforeClass(){
        ControladorDeZonas controladorZonas=new ControladorDeZonas(new HandlerZonas(), new HandlerZonasPeligro());
        try {
            controladorZonas.annadirZona(new Zona(1, new Posicion(500f,500f), 1, "Bien", "Luna", 10000));
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        pescador=new SimuladorPescador(new HandlerPescador(), 1, 1,new Barco(0,new Posicion(1f,1f),"Pescador"),new Posicion(4f,5f),controladorZonas);        
    }
    @Test
    public void testBarcoEnPosicion(){
        assertEquals(pescador.barcoEnPosicion(0),true);
        assertEquals(pescador.barcoEnPosicion(50f),false);
    }
    @Test
    public void testMaquinaDeEstados(){
        try {
            pescador.maquinaDeEstados();
            pescador.setPez("Luna");
            pescador.setEstado("Con Objetivo");
            pescador.maquinaDeEstados();
            pescador.setEstado("Hacia Objetivo");
            pescador.maquinaDeEstados();
            pescador.setEstado("Pescando");
            pescador.maquinaDeEstados();
            pescador.setEstado("Vuelta Puerto");
            pescador.maquinaDeEstados();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testCalculosTrayecto()
    {
        assertEquals(4f,pescador.calculosTrayecto());
    }
    @Test
    public void testGetPez(){
        assertEquals(pescador.getPez(), "Lubina");
    }
    @Test
    public void testGetEstado(){
        assertEquals(pescador.getEstado(), "Sin Objetivo");
    }
    @Test
    public void testSetEstado(){

        assertEquals(pescador.getEstado(), "Sin Objetivo");
        pescador.setEstado("A");
        assertEquals(pescador.getEstado(), "A");
    }
    @Test
    public void testSetTipo(){
        assertEquals(pescador.getPez(), "Lubina");
        pescador.setPez("Pe");
        assertEquals(pescador.getPez(), "Pe");
    }
    @Test
    public void testGetCantidadPeces(){
        assertEquals(pescador.getCantidadPeces(), 100);
    }
    @Test
    public void testSetCantidadPeces(){
        assertEquals(pescador.getCantidadPeces(), 100);
        pescador.setCantidadPeces(10);
        assertEquals(pescador.getCantidadPeces(), 10);
    }
}
