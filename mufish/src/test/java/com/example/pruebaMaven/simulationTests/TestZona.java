package com.example.pruebaMaven.simulationTests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.Zona;

public class TestZona
{
    Zona zona;
    @Before
    public void beforeClass(){
        zona=new Zona(1, new Posicion(1f,1f), 1, "Bien","Lubina",1);
    }
    @Test
    public void testCrearZona()
    {
        assertTrue(zona.equals(new Zona(1, new Posicion(1f,1f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(2, new Posicion(1f,1f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(2f,1f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,2f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,1f), 2, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,1f), 1, "Mal","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,1f), 1, "Mal","XD",1)));
    }
    @Test
    public void testGetId()
    {
        assertEquals(zona.getId(), 1);
    }
    @Test
    public void testSetId()
    {
        zona.setId(2);
        assertEquals(zona.getId(), 2);
    }
    @Test
    public void testGetMaxPeces()
    {
        assertEquals(zona.getMaxPeces(), 1);
    }
    @Test
    public void testSetMaxPeces()
    {
        zona.setMaxPeces(2);
        assertEquals(zona.getMaxPeces(), 2);
    }
    @Test
    public void testGetPosicion()
    {
        assertEquals(zona.getPosicion(),new Posicion(1f,1f));
    }
    @Test
    public void testSetPosicion()
    {
        zona.setPosicion(new Posicion(2f,2f));
        assertEquals(zona.getPosicion(), new Posicion(2f,2f));
    }
    @Test
    public void testSetEstado()
    {
        zona.setEstado("Mal");
        assertEquals(zona.getEstado(), "Mal");
    }
    @Test
    public void testGetEstado()
    {
        
        assertEquals(zona.getEstado(),"Bien");
    }
    @Test
    public void testSetRadio()
    {
        zona.setRadio(2);
        assertEquals(zona.getRadio(), 2);
    }
    @Test
    public void testGetRadio()
    {
        assertEquals(zona.getRadio(),1);
    }
    @Test
    public void testGetPez()
    {
        assertEquals(zona.getPez(), "Lubina");
    }
    @Test
    public void testSetPez()
    {
        zona.setPez("Sardina");
        assertEquals(zona.getPez(),"Sardina");
    }
    
    @Test
    public void testToString()
    {
        assertEquals(zona.toString(),"/1,1.0,1.0,1,Bien,Lubina");
    }
    @Test
    public void testEquals()
    {
        assertTrue(zona.equals(new Zona(1, new Posicion(1f,1f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(2, new Posicion(1f,1f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(2f,1f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,2f), 1, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,1f), 2, "Bien","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,1f), 1, "Mal","Lubina",1)));
        assertFalse(zona.equals(new Zona(1, new Posicion(1f,1f), 1, "Mal","XD",1)));
        assertFalse(zona.equals(null));
    }
    @Test
    public void testHashCode(){
        assertEquals(zona.hashCode(), zona.getId());
    }
}
