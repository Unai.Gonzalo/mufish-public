package com.example.pruebaMaven.simulationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.HandlerGuardacostas;
import edu.mondragon.mufish.HandlerPescador;
import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorGuardacostas;
import edu.mondragon.mufish.simulacion.SimuladorPescador;
import edu.mondragon.mufish.simulacion.Zona;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;


/**
 * Unit test for simple App.
 */
public class TestSimuladorGuardacostas
{
    SimuladorGuardacostas guardacostas;
    @Before
    public void beforeClass(){
        ControladorDeZonas controladorZonas=new ControladorDeZonas(new HandlerZonas(), new HandlerZonasPeligro());
        try {
            controladorZonas.annadirZonaPeligro(new Zona(1, new Posicion(500f,500f), 1, "Contaminado", null, -1));
            controladorZonas.annadirZonaPeligro(new Zona(2, new Posicion(501f,500f), 1, "Contaminado", null, -1));
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        guardacostas=new SimuladorGuardacostas(new HandlerGuardacostas(),1,1,new Barco(1,new Posicion(1f,1f),"Guardacostas"),controladorZonas);        
    }
    @Test
    public void testBarcoEnPosicion(){
        assertEquals(guardacostas.barcoEnPosicion(0),true);
        assertEquals(guardacostas.barcoEnPosicion(50f),false);
    }
    @Test
    public void testMaquinaDeEstados(){
        try {
            guardacostas.maquinaDeEstados();
            guardacostas.setEstado("Buscando por el mar");
            for(int i=0;i<11;i++){
                guardacostas.maquinaDeEstados();
            }
            guardacostas.setEstado("Envia informacion del estado");
            guardacostas.maquinaDeEstados();
            guardacostas.setEstado("Vuelta puerto");
            guardacostas.maquinaDeEstados();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testCalculosTrayecto()
    {
        assertEquals(4f,guardacostas.calculosTrayecto());
    }
    @Test
    public void testGetEstado(){
        assertEquals(guardacostas.getEstado(), "En puerto");
    }
    @Test
    public void testSetEstado(){

        assertEquals(guardacostas.getEstado(), "En puerto");
        guardacostas.setEstado("A");
        assertEquals(guardacostas.getEstado(), "A");
    }
    @Test
    public void testRun() {
    	guardacostas.interrupt();
    	guardacostas.run();
    	try {
			guardacostas.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}
