package com.example.pruebaMaven.simulationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeDibujables;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.Zona;



/**
 * Unit test for simple App.
 */
public class TestControladorDeDibujables
{
    ControladorDeDibujables controlador;
    @Before
    public void beforeClass(){
        controlador=new ControladorDeDibujables();
    }
    @Test
    public void testCambiarListaBarcos()
    {
        Barco barco1=new Barco(1,new Posicion(1f,1f),"Pescador");
        Barco barco2=new Barco(2,new Posicion(1f,1f),"Pescador");
        assertEquals(controlador.cambiarListaBarcos(barco1.toString()),"B/"+barco1.getPosicion().getPosX()+","+barco1.getPosicion().getPosY()+","+barco1.getTipo()+"/B");
        assertEquals(controlador.cambiarListaBarcos(barco2.toString()),"B/"+barco2.getPosicion().getPosX()+","+barco2.getPosicion().getPosY()+","+barco2.getTipo()+"/"+barco2.getPosicion().getPosX()+","+barco2.getPosicion().getPosY()+","+barco2.getTipo()+"/B");
    }
    @Test
    public void testObtenerString()
    {
        Barco barco=new Barco(1,new Posicion(1f,1f),"Pescador");
        controlador.cambiarListaBarcos(barco.toString());
        assertEquals(controlador.obtenerString(), "B/"+barco.getPosicion().getPosX()+","+barco.getPosicion().getPosY()+","+barco.getTipo()+"/B");
    }
    @Test
    public void testListaZonasToString(){
        List<Zona> lista=new ArrayList<>();
        Zona z1=new Zona(1,new Posicion(1f,1f),1,"1","1",1);
        Zona z2=new Zona(2,new Posicion(2f,2f),2,"2","2",2);
        lista.add(z1);
        lista.add(z2);
        assertEquals(controlador.listaZonasToString(lista),"Z"+ z1.toString()+z2.toString()+"/Z");
    }
}
