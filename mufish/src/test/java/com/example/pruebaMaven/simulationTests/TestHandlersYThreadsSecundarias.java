package com.example.pruebaMaven.simulationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketExtension;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistration;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import edu.mondragon.mufish.HandlerGuardacostas;
import edu.mondragon.mufish.HandlerPescador;
import edu.mondragon.mufish.HandlerUsuario;
import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.ThreadUsuario;
import edu.mondragon.mufish.ThreadUsuarioBarco;
import edu.mondragon.mufish.ThreadUsuarioZona;
import edu.mondragon.mufish.webSocketConfig;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;
import edu.mondragon.mufish.simulacion.Zona;



/**
 * Unit test for simple App.
 */
public class TestHandlersYThreadsSecundarias
{
    WebSocketSession sesion;
    @Before
    public void beforeClass(){
        sesion=new WebSocketSession() {

            @Override
            public String getId() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public URI getUri() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public HttpHeaders getHandshakeHeaders() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Map<String, Object> getAttributes() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Principal getPrincipal() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public InetSocketAddress getLocalAddress() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public InetSocketAddress getRemoteAddress() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getAcceptedProtocol() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void setTextMessageSizeLimit(int messageSizeLimit) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public int getTextMessageSizeLimit() {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public void setBinaryMessageSizeLimit(int messageSizeLimit) {
                // TODO Auto-generated method stub
                
            }

            @Override
            public int getBinaryMessageSizeLimit() {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public List<WebSocketExtension> getExtensions() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void sendMessage(WebSocketMessage<?> message) throws IOException {
                // TODO Auto-generated method stub
                
            }

            @Override
            public boolean isOpen() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void close() throws IOException {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void close(CloseStatus status) throws IOException {
                // TODO Auto-generated method stub
                
            }
            
        } ;
        
    }
    @Test
    public void testHandlerPescador()
    {
        HandlerPescador handler=new HandlerPescador();
        handler.setContZonas(new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro()));
        try {
            handler.afterConnectionEstablished(sesion);
            handler.handleMessage(sesion,new TextMessage("B/1,1,1,Pescador"));
            handler.simularActuBarco("B/1,1,1,Pescador");
            handler.afterConnectionClosed(sesion,null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testHandlerGuardacostas()
    {
        HandlerGuardacostas handler=new HandlerGuardacostas();
        handler.setContZonas(new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro()));
        try {
            handler.afterConnectionEstablished(sesion);
            handler.handleMessage(sesion,new TextMessage("B/1,1,1,Guardacostas"));
            handler.simularActuBarco("B/1,1,1,Guardacostas");
            handler.afterConnectionClosed(sesion,null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testHandlerUsuario()
    {
        HandlerUsuario handler=new HandlerUsuario();
        SimuladorEncargado encargado=new SimuladorEncargado(new ControladorDeZonas(new HandlerZonas(), new HandlerZonasPeligro()));
        handler.setEncargado(encargado);
        try {
            handler.afterConnectionEstablished(sesion);
            handler.handleTextMessage(sesion,new TextMessage("E"));
            handler.handleTextMessage(sesion,new TextMessage("NE"));
            handler.handleTextMessage(sesion,new TextMessage("B/1,1,1,Pescador"));
            handler.handleTextMessage(sesion,new TextMessage("G/1,1,1,Guardacostas"));
            handler.afterConnectionClosed(sesion,null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testHandlerZonasYThreadUsuario()
    {
        HandlerZonas handler=new HandlerZonas();
        List<String> listaString=new ArrayList<>();
        listaString.add("Hola");
        List<Zona> listaZona=new ArrayList<>();
        Zona z=new Zona(1,new Posicion(1,1),100,"Bien","Luna",120);
        listaZona.add(z);
        
        ControladorDeZonas contZ=new ControladorDeZonas(handler, new HandlerZonasPeligro());
        webSocketConfig web=new webSocketConfig();
        web.setControladorDeZonas(contZ);
        try {
            
            contZ.annadirZona(z);
            handler.setContZonas(contZ);
            new ThreadUsuario(10,"Luna",67,1);
            handler.afterConnectionEstablished(sesion);
            handler.simularCreacionDeZonas(listaZona);
            handler.dibujarZonasFalta(listaString);
            handler.handleTextMessage(sesion,new TextMessage("VM"));
            handler.handleTextMessage(sesion,new TextMessage("VZ"));
            Thread.sleep(1000);
            handler.handleTextMessage(sesion,new TextMessage("10"));
            handler.afterConnectionClosed(sesion,null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testHandlerZonasPeligroYThreadUsuario()
    {
        HandlerZonasPeligro handler=new HandlerZonasPeligro();
        List<String> listaString=new ArrayList<>();
        listaString.add("Hola");
        List<Zona> listaZona=new ArrayList<>();
        Zona z=new Zona(1,new Posicion(1,1),100,"Bien","Luna",-1);
        listaZona.add(z);
        
        ControladorDeZonas contZ=new ControladorDeZonas(new HandlerZonas(),handler);
        webSocketConfig web=new webSocketConfig();
        web.setControladorDeZonas(contZ);
        try {
            
            contZ.annadirZona(z);
            handler.setContZonas(contZ);
            new ThreadUsuario(10,"Luna",67,1);
            handler.afterConnectionEstablished(sesion);
            handler.simularCreacionDeZonasPeligro(listaZona);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @Test
    public void testThreadUsuarioBarco()
    {
        new ThreadUsuarioBarco(new Barco(1, new Posicion(4,5), "Pescador"));
        new ThreadUsuarioBarco(new Barco(1, new Posicion(4,5), "Guardacostas"));
    }
    @Test
    public void testThreadUsuarioZona()
    {
        try {
        Zona z1=new Zona(1,new Posicion(1,1),100,"Bien","Luna",100);
        Zona z2=new Zona(1,new Posicion(1,1),100,"Bien","Luna",-1);
        ControladorDeZonas contZ=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
        webSocketConfig.setControladorDeZonas(contZ);
        new ThreadUsuarioZona(z1,"Create_Fishing_Zone");
        Thread.sleep(1000);
        new ThreadUsuarioZona(z1,"Delete_Fishing_Zone");
        Thread.sleep(1000);
        new ThreadUsuarioZona(z2,"Create_Risk_Zone");
        Thread.sleep(1000);
        new ThreadUsuarioZona(z2,"Delete_Risk_Zone");
        } catch (InterruptedException e) {
        
            e.printStackTrace();
        }
    }
    @Test
    public void testWebSocketConfig(){
        webSocketConfig web=new webSocketConfig();
        web.registerWebSocketHandlers(new WebSocketHandlerRegistry() {

            @Override
            public WebSocketHandlerRegistration addHandler(WebSocketHandler webSocketHandler, String... paths) {
                // TODO Auto-generated method stub
                return null;
            }
            
        });
    }
}
