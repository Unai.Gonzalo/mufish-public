package com.example.pruebaMaven.simulationTests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;

public class TestSimuladorEncargado
{
    SimuladorEncargado encargado;
    ControladorDeZonas contZo;
    @Before
    public void beforeClass(){
        contZo=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
        encargado=new SimuladorEncargado(contZo);
    }
    @Test
    public void testRun(){
        encargado.interrupt();
        encargado.run();
        try {
            encargado.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Test
    public void testRevisarEstadoZonas(){
        try {
            encargado.revisarEstadoZonas();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Test
    public void testGetActivo(){
        assertEquals(encargado.getActivo(), true);
    }
    @Test
    public void testSetActivo(){
        assertEquals(encargado.getActivo(), true);
        encargado.setActivo(false);
        assertEquals(encargado.getActivo(), false);
    }
}
