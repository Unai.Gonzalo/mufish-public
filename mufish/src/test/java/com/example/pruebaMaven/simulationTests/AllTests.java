package com.example.pruebaMaven.simulationTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestBarco.class, TestControladorDeDibujables.class, TestPosicion.class, TestSimuladorPescador.class,
		TestZona.class })
public class AllTests {

}
