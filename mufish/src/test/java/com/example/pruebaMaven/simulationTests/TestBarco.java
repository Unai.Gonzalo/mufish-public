package com.example.pruebaMaven.simulationTests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.Posicion;

public class TestBarco
{
    Barco barco;
    @Before
    public void beforeClass(){
        barco=new Barco(1,new Posicion(43.03f,-5.03f),"Pescador");
    }
    @Test
    public void testCrearBarco()
    {
        assertTrue(barco.equals(new Barco(1,new Posicion(43.03f,-5.03f),"Pescador")));
        assertFalse(barco.equals(new Barco(2,new Posicion(43.03f,-5.03f),"Pescador")));
        assertFalse(barco.equals(new Barco(1,new Posicion(43.4f,-5.03f),"Pescador")));
        assertFalse(barco.equals(new Barco(1,new Posicion(43.03f,-5.4f),"Pescador")));
        assertFalse(barco.equals(new Barco(1,new Posicion(43.03f,-5.4f),"AAAA")));
    }
    @Test
    public void testGetId()
    {
        assertEquals(barco.getId(), 1);
    }
    @Test
    public void testSetId()
    {
        barco.setId(2);
        assertEquals(barco.getId(), 2);
    }
    @Test
    public void testGetPosicion()
    {
        assertEquals(barco.getPosicion(),new Posicion(43.03f,-5.03f));
    }
    @Test
    public void testSetPosicion()
    {
        barco.setPosicion(new Posicion(45.03f,-5.03f));
        assertEquals(barco.getPosicion(), new Posicion(45.03f,-5.03f));
    }
    
    @Test
    public void testToString()
    {
        assertEquals(barco.toString(),"B/1,43.03,-5.03,Pescador/");
    }
    @Test
    public void testObtenerBarcoConString()
    {
        assertEquals(barco,Barco.obtenerBarcoConString("B/1,43.03,-5.03,Pescador/","/",","));
    }
    @Test
    public void testEquals()
    {
        assertEquals(barco, new Barco(1,new Posicion(43.03f,-5.03f),"Pescador"));
        assertTrue(barco.equals(new Barco(1,new Posicion(43.03f,-5.03f),"Pescador")));
        assertFalse(barco.equals(new Barco(2,new Posicion(43.03f,-5.03f),"Pescador")));
        assertFalse(barco.equals(new Barco(1,new Posicion(43.4f,-5.03f),"Pescador")));
        assertFalse(barco.equals(new Barco(1,new Posicion(43.03f,-5.4f),"Pescador")));
        assertFalse(barco.equals(new Barco(1,new Posicion(43.03f,-5.4f),"GuardaCostas")));
        assertFalse(barco.equals(null));
    }
    @Test
    public void testHashCode(){
        assertEquals(barco.hashCode(), barco.getId());
    }
    @Test
    public void testGetTipo(){
        assertEquals(barco.getTipo(), "Pescador");
    }
    @Test
    public void testSetTipo(){
        assertEquals(barco.getTipo(), "Pescador");
        barco.setTipo("Pe");
        assertEquals(barco.getTipo(), "Pe");
    }
}
