package com.example.pruebaMaven.securityTests;

import org.junit.Assert;
import org.junit.Test;

import edu.mondragon.mufish.Security.PasswordEncrypter;

public class PasswordEncrypterTest {

    @Test
    public void testGetSalt(){
        String salt = PasswordEncrypter.getSalt(5);
        Assert.assertTrue(salt.length()==5);
    }

    @Test
    public void testGenerateSecurePassword(){
        String passChanged = PasswordEncrypter.generateSecurePassword("1234", "ABCDE");
        Assert.assertFalse(passChanged.equals("1234"));
    }

    @Test
    public void testVerifyUserPassword(){
        String passChanged = PasswordEncrypter.generateSecurePassword("1234", "ABCDE");
        Assert.assertTrue(PasswordEncrypter.verifyUserPassword("1234", passChanged, "ABCDE"));
    }

    @Test
    public void testGenerateSecurePassword2(){
        String pass1 = PasswordEncrypter.generateSecurePassword("1234");
        String pass2 = PasswordEncrypter.generateSecurePassword("1234");
        Assert.assertFalse(pass1.equals(pass2));
    }

    @Test
    public void testGetSalt2(){
        String pass1 = PasswordEncrypter.generateSecurePassword("1234");
        String salt1 = PasswordEncrypter.getSalt();
        String pass2 = PasswordEncrypter.generateSecurePassword("1234");
        String salt2 = PasswordEncrypter.getSalt();
        Assert.assertFalse(salt1.equals(salt2));
    }
    
}
