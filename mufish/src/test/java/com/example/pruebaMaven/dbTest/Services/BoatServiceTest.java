package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;
import edu.mondragon.mufish.Services.BoatService;
import edu.mondragon.mufish.Stubs.BoatRepoStub;

public class BoatServiceTest {
    
    BoatServInt service;
    
    //SPRING BOOT MOCKEATZEKO METODOA (STUB)

    @Before
    public void setUp() throws Exception{
        service = new BoatService(new BoatRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}

    @Test
    public void listTest(){
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void createBoatTest(){
    	service.createBoat("Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba");
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void readBoatTest(){
    	Boat boat = service.readBoat(5);
    	Boat boat2 = service.readBoat(15);
        assertTrue(boat.getBoatId()==5);
        assertNull(boat2);
    }
    
    @Test
    public void readBoatsByUserTest(){
    	List<Boat> boats = service.readBoatsByUser(new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null));
    	List<Boat> boats2 = service.readBoatsByUser(new User(6, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null));
    	assertTrue(boats.size()==3);
    	assertNull(boats2);
    }
    
    @Test
    public void updateBoatTest(){
    	service.updateBoat(5,"Prueba2", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba");
        assertTrue(service.readBoat(5).getName().equals("Prueba2"));
    }
    
    @Test
    public void deleteTest(){
    	service.deleteBoat(5);
        assertTrue(service.list().size()==2);
    }

    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	BoatRepoStub repo = new BoatRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new Boat()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }

}
