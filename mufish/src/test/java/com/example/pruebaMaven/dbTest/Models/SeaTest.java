package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;

public class SeaTest {

    Sea sea;

	@Before
	public void setUp() throws Exception {
		sea = new Sea(5,"Kantauri Itsasoa");
	}

	@After
	public void tearDown() throws Exception {
		sea = null;
	}

	@Test
	public void TestGetId(){
		int id = sea.getSeaId();
		assertEquals(5, id);
	}

	@Test
	public void testGetName(){
		String name = sea.getName();
		assertEquals("Kantauri Itsasoa", name);
	}

	@Test
	public void testSetName(){
		sea.setName("Mar Cantábrico");
		assertEquals("Mar Cantábrico", sea.getName());
	}

	@Test
	public void testAddPort(){
		sea.ports = new ArrayList<>();
		sea.addPort(new Port(5, "Prueba", "Prueba", sea, 1, 1));
		assertEquals(1, sea.getPorts().size());
	}

	@Test
	public void testGetPort(){
		sea.ports = new ArrayList<>();
		sea.addPort(new Port(5, "Prueba", "Prueba", sea, 1, 1));
		assertEquals(new Port(5, "Prueba", "Prueba", sea, 1, 1), sea.getPorts().get(0));
	}

	@Test
	public void testSetPort(){
		List<Port> ports = new ArrayList<>();
		ports.add(new Port(6, "Prueba", "Prueba", sea, 1, 1));
		sea.setPorts(ports);
		Assert.assertTrue(ports.equals(sea.getPorts()));
	}

	@Test
	public void testAddZone(){
		sea.zones = new ArrayList<>();
		sea.addZone(new Zone(5, sea, 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
		assertEquals(1, sea.getZones().size());
	}

	@Test
	public void testGetZone(){
		sea.zones = new ArrayList<>();
		sea.addZone(new Zone(5, sea, 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
		assertEquals(new Zone(5, sea, 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), sea.getZones().get(0));
	}

	@Test
	public void testSetZone(){
		List<Zone> zones = new ArrayList<>();
		zones.add(new Zone(5, sea, 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
		sea.setZones(zones);
		Assert.assertTrue(zones.equals(sea.getZones()));
	}

	@Test
	public void testToString(){
		String string = sea.toString();
		assertEquals("S/5,Kantauri Itsasoa/S", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(sea.equals(sea));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(sea.hashCode()==sea.getSeaId());
	}

}
