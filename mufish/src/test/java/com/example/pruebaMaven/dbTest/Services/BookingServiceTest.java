package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Zone;
import edu.mondragon.mufish.ServiceInterfaces.BookingServInt;
import edu.mondragon.mufish.Services.BookingService;
import edu.mondragon.mufish.Stubs.BookingRepoStub;

public class BookingServiceTest {
	
	BookingServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new BookingService(new BookingRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void createBookingTest(){
    	service.createBooking(new Date(0), "Prueba", 5, new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void readBookingTest(){
    	Booking booking = service.readBooking(5);
    	Booking booking2 = service.readBooking(15);
        assertTrue(booking.getBookingId()==5);
        assertNull(booking2);
    }
    
    @Test
    public void updateBookingTest(){
    	service.updateBooking(5,new Date(0), "Prueba2", 5, new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
        assertTrue(service.readBooking(5).getObjective().equals("Prueba2"));
    }
    
    @Test
    public void deleteTest(){
    	service.deleteBooking(5);
        assertTrue(service.list().size()==2);
    }
    
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	BookingRepoStub repo = new BookingRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new Booking()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }

}
