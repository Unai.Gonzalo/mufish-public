package com.example.pruebaMaven.dbTest.Models;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class FishTest {
    
    Fish fish;

    @Before
	public void setUp() throws Exception {
		fish = new Fish(5,"Prueba");
	}

	@After
	public void tearDown() throws Exception {
		fish = null;
	}

	@Test
	public void testGetId(){
		int id = fish.getFishId();
		assertEquals(5, id);
	}

	@Test
	public void testGetName(){
		String name = fish.getName();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetName(){
		fish.setName("Prueba2");
		assertEquals("Prueba2", fish.getName());
	}

	@Test
	public void testAddZone(){
		fish.zones = new ArrayList<>();
		fish.addZone(new Zone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), fish, 1, 1, 1));
		assertEquals(1, fish.getZones().size());
	}

	@Test
	public void testGetZones(){
		fish.zones = new ArrayList<>();
		fish.addZone(new Zone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), fish, 1, 1, 1));
		assertEquals(new Zone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), fish, 1, 1, 1), fish.getZones().get(0));
	}

    @Test
	public void testSetZones(){
		List<Zone> zones = new ArrayList<>();
		zones.add(new Zone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), fish, 1, 1, 1));
		fish.setZones(zones);
		Assert.assertTrue(zones.equals(fish.getZones()));
	}

	@Test
	public void testToString(){
		String string = fish.toString();
		assertEquals("F/5,Prueba/F", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(fish.equals(fish));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(fish.hashCode()==fish.getFishId());
	}

}
