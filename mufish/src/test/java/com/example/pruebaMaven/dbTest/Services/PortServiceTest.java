package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.ServiceInterfaces.PortServInt;
import edu.mondragon.mufish.Services.PortService;
import edu.mondragon.mufish.Stubs.PortRepoStub;

public class PortServiceTest {

	PortServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new PortService(new PortRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void createPortTest(){
    	service.createPort("Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1);
        assertTrue(service.list().size()==5);
    }
    
    @Test
    public void readPortTest(){
    	Port port = service.readPort(5);
    	Port port2 = service.readPort(15);
        assertTrue(port.getPortId()==5);
        assertNull(port2);
    }
    
    @Test
    public void updatePortTest(){
    	service.updatePort(5,"Prueba2", "Prueba", new Sea(5, "Prueba"), 1, 1);
        assertTrue(service.readPort(5).getName().equals("Prueba2"));
    }
    
    @Test
    public void deletePortTest(){
    	service.deletePort(5);
        assertTrue(service.list().size()==3);
    }
	
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	PortRepoStub repo = new PortRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new Port()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }
}
