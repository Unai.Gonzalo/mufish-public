package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.MessageServInt;
import edu.mondragon.mufish.Services.MessageService;
import edu.mondragon.mufish.Stubs.MessageRepoStub;

public class MessageServiceTest {

	MessageServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new MessageService(new MessageRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void createMessageTest(){
    	service.createMessage(new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"), "Prueba", "Prueba");
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void readMessageTest(){
    	Message message = service.readMessage(5);
    	Message message2 = service.readMessage(15);
        assertTrue(message.getMessageId()==5);
        assertNull(message2);
    }
    
    @Test
    public void updateMessageTest(){
    	service.updateMessage(5,new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"), "Prueba", "Prueba2",Integer.valueOf(1));
        assertTrue(service.readMessage(5).getTitle().equals("Prueba2"));
    }
    
    @Test
    public void deleteMessageTest(){
    	service.deleteMessage(5);
        assertTrue(service.list().size()==2);
    }
    
    @Test
    public void updateMessageStateToUnreadTest(){
    	service.updateMessageStateToUnread(5);
        assertTrue(service.readMessage(5).getStateRead()==0);
    }
    
    @Test
    public void updateMessageStateToReadTest(){
    	service.updateMessageStateToRead(5);
        assertTrue(service.readMessage(5).getStateRead()==1);
    }
    
    @Test
    public void readReceivedMessagesByUserTest(){
    	List<Message> lista = service.readReceivedMessagesByUser(new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
    	List<Message> lista2 = service.readReceivedMessagesByUser(new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
    	assertTrue(lista.size()==3);
    	assertNull(lista2);
    }
    
    @Test
    public void readSentMessagesByUserTest(){
    	List<Message> lista = service.readSentMessagesByUser(new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
    	List<Message> lista2 = service.readSentMessagesByUser(new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
    	assertTrue(lista.size()==3);
    	assertNull(lista2);
    }
    
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	MessageRepoStub repo = new MessageRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new Message()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }
	
}
