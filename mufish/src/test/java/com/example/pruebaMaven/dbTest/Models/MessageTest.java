package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;

public class MessageTest {
    
    Message message;

    @Before
	public void setUp() throws Exception {
		message = new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0);
	}

	@After
	public void tearDown() throws Exception {
		message = null;
	}

	@Test
	public void testGetId(){
		int id = message.getMessageId();
		assertEquals(5, id);
	}

	@Test
	public void testGetTitle(){
		String name = message.getTitle();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetTitle(){
		message.setTitle("Prueba2");
		assertEquals("Prueba2", message.getTitle());
	}

    @Test
	public void testGetNotification(){
		String name = message.getNotification();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetNotification(){
		message.setNotification("Prueba2");
		assertEquals("Prueba2", message.getNotification());
	}

    @Test
	public void testGetStateRead(){
		int sr = message.getStateRead();
		assertEquals(0, sr);
	}

    @Test
	public void testSetStateRead(){
		message.setStateRead(1);
		assertEquals(1, message.getStateRead());
	}

    @Test
	public void testGetSender(){
		User user = message.getSender();
		assertEquals(new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), user);
	}

    @Test
	public void testSetSender(){
		message.setSender(new User(3, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null));
		assertEquals(new User(3, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), message.getSender());
	}

    @Test
	public void testGetReceiver(){
		User user = message.getReceiver();
		assertEquals(new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), user);
	}

    @Test
	public void testSetFisher(){
		message.setReceiver(new User(6, "Prueba2", "Prueba2", "Prueba2", "Prueba2", new Type(5, "Prueba"), 6, null, null));
		assertEquals(new User(6, "Prueba2", "Prueba2", "Prueba2", "Prueba2", new Type(5, "Prueba"), 6, null, null), message.getReceiver());
	}


	@Test
	public void testToString(){
		String string = message.toString();
		assertEquals("M/5,Prueba,Prueba,Prueba/M", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(message.equals(message));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(message.hashCode()==message.getMessageId());
	}
}
