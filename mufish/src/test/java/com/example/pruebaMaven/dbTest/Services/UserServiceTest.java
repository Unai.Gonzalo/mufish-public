package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.UserServInt;
import edu.mondragon.mufish.Services.UserService;
import edu.mondragon.mufish.Stubs.UserRepoStub;

public class UserServiceTest {

	UserServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new UserService(new UserRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void listOthersTest(){
        assertTrue(service.listOthers(new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba")).size()==3);
    }
    
    @Test
    public void createUserTest(){
    	service.createUser("Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), Integer.valueOf(5), "Prueba", "Prueba");
        assertTrue(service.list().size()==5);
    }
    
    @Test
    public void readUserTest(){
    	User user = service.readUser(5);
    	User user2 = service.readUser(15);
        assertTrue(user.getUserId()==5);
        assertNull(user2);
    }
    
    @Test
    public void readUserTest2(){
    	User user = service.readUser("Prueba");
    	User user2 = service.readUser("Pruebaa");
        assertTrue(user.getUsername().equals("Prueba"));
        assertNull(user2);
    }
    
    @Test
    public void readUserTest3(){
    	User user = service.readUser("Prueba","Prueba");
    	User user2 = service.readUser("Pruebaa", "Prueba");
        assertTrue(user.getUsername().equals("Prueba") && user.getPassword().equals("Prueba"));
        assertNull(user2);
    }
    
    @Test
    public void readUserByTypeTest(){
    	List<User> users = service.readUserByType(new Type(5, "Prueba"));
    	List<User> users2 = service.readUserByType(new Type(6, "Prueba2"));
        assertTrue(users.size()==4);
        assertNull(users2);
    }
    
    @Test
    public void updateUserTest(){
    	service.updateUser(5,"Prueba2", "Prueba", "Prueba", new Type(5, "Prueba"), Integer.valueOf(5), "Prueba", "Prueba");
        assertTrue(service.readUser(5).getUsername().equals("Prueba2"));
    }
    
    @Test
    public void deleteUserTest(){
    	service.deleteUser(5);
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	UserRepoStub repo = new UserRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new User()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }
}
