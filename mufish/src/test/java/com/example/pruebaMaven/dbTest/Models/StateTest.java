package com.example.pruebaMaven.dbTest.Models;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;

public class StateTest {
    
    State state;

    @Before
	public void setUp() throws Exception {
		state = new State(5,"Prueba");
	}

	@After
	public void tearDown() throws Exception {
		state = null;
	}

	@Test
	public void testGetId(){
		int id = state.getStateId();
		assertEquals(5, id);
	}

	@Test
	public void testGetName(){
		String name = state.getName();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetName(){
		state.setName("Prueba2");
		assertEquals("Prueba2", state.getName());
	}

	@Test
	public void testAddZone(){
		state.zones = new ArrayList<>();
		state.addZone(new Zone(5, new Sea(5, "Prueba"), 1, state, new Fish(5, "Prueba"), 1, 1, 1));
		assertEquals(1, state.getZones().size());
	}

	@Test
	public void testGetZones(){
		state.zones = new ArrayList<>();
		state.addZone(new Zone(5, new Sea(5, "Prueba"), 1, state, new Fish(5, "Prueba"), 1, 1, 1));
		assertEquals(new Zone(5, new Sea(5, "Prueba"), 1, state, new Fish(5, "Prueba"), 1, 1, 1), state.getZones().get(0));
	}

	@Test
	public void testSetZones(){
		List<Zone> zones = new ArrayList<>();
		zones.add(new Zone(5, new Sea(5, "Prueba"), 1, state, new Fish(5, "Prueba"), 1, 1, 1));
		state.setZones(zones);
		Assert.assertTrue(zones.equals(state.getZones()));
	}

	@Test
	public void testToString(){
		String string = state.toString();
		assertEquals("ST/5,Prueba/ST", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(state.equals(state));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(state.hashCode()==state.getStateId());
	}

}
