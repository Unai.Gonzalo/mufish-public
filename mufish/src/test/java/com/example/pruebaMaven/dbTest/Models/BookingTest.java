package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Zone;

public class BookingTest {
    
    Booking booking;

    @Before
	public void setUp() throws Exception {
		booking = new Booking(5,new Date(0), "Prueba", 5, new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
	}

	@After
	public void tearDown() throws Exception {
		booking = null;
	}

    @Test
	public void testGetId(){
		int id = booking.getBookingId();
		assertEquals(5, id);
	}

	@Test
	public void testGetDate(){
		Date date = booking.getDate();
		assertEquals(new Date(0), date);
	}

    @Test
	public void testSetDate(){
		booking.setDate(new Date(1));
		assertEquals(new Date(1), booking.getDate());
	}

	@Test
	public void testGetObjective(){
		String name = booking.getObjective();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetObjective(){
		booking.setObjective("Prueba2");
		assertEquals("Prueba2", booking.getObjective());
	}

    @Test
	public void testGetQuantity(){
		int q = booking.getQuantity();
		assertEquals(5, q);
	}

    @Test
	public void testSetQuantity(){
		booking.setQuantity(6);
		assertEquals(6, booking.getQuantity());
	}

	@Test
	public void testGetZone(){
		Zone zone = booking.getZone();
		assertEquals(new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), zone);
	}

    @Test
	public void testSetPort(){
		booking.setZone(new Zone(2, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
		assertEquals(new Zone(2, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), booking.getZone());
	}

    @Test
	public void testGetBoat(){
		Boat boat = booking.getBoat();
		assertEquals(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"), boat);
	}

    @Test
	public void testSetBoat(){
		booking.setBoat(new Boat(6, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		assertEquals(new Boat(6, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"), booking.getBoat());
	}

	@Test
	public void testToString(){
		String string = booking.toString();
		assertEquals("BK/1970-01-01,Prueba,5,5/BK", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(booking.equals(booking));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(booking.hashCode()==booking.getBookingId());
	}

}
