package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Zone;

public class BoatTest {

    Boat boat;

    @Before
	public void setUp() throws Exception {
		boat = new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba");
	}

	@After
	public void tearDown() throws Exception {
		boat = null;
	}

	@Test
	public void testGetId(){
		int id = boat.getBoatId();
		assertEquals(5, id);
	}

	@Test
	public void testGetName(){
		String name = boat.getName();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetName(){
		boat.setName("Prueba2");
		assertEquals("Prueba2", boat.getName());
	}

    @Test
	public void testGetPort(){
		Port port = boat.getPort();
		assertEquals(new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), port);
	}

    @Test
	public void testSetPort(){
		boat.setPort(new Port(6, "Prueba2", "Prueba2", new Sea(5, "Prueba"), 1, 1));
		assertEquals(new Port(6, "Prueba2", "Prueba2", new Sea(5, "Prueba"), 1, 1), boat.getPort());
	}

    @Test
	public void testGetFisher(){
		User user = boat.getFisher();
		assertEquals(new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), user);
	}

    @Test
	public void testSetFisher(){
		boat.setFisher(new User(6, "Prueba2", "Prueba2", "Prueba2", "Prueba2", new Type(5, "Prueba"), 6, null, null));
		assertEquals(new User(6, "Prueba2", "Prueba2", "Prueba2", "Prueba2", new Type(5, "Prueba"), 6, null, null), boat.getFisher());
	}

    @Test
	public void testGetPosX(){
		int posX = boat.getPosX();
		assertEquals(1, posX);
	}

	@Test
	public void testSetPosX(){
		boat.setPosX(2);
		assertEquals(2, boat.getPosX());
	}

    @Test
	public void testGetPosY(){
		int posY = boat.getPosY();
		assertEquals(1, posY);
	}

	@Test
	public void testSetPosY(){
		boat.setPosY(2);
		assertEquals(2, boat.getPosY());
	}

	@Test
	public void testGetUrl(){
		String url = boat.getUrl();
		assertEquals("Prueba", url);
	}

	@Test
	public void testSetUrl(){
		boat.setUrl("Prueba2");
		assertEquals("Prueba2", boat.getUrl());
	}

    @Test
	public void testAddBooking(){
		boat.bookings = new ArrayList<>();
		boat.addBooking(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), boat));
		assertEquals(1, boat.getBookings().size());
	}

    @Test
	public void testGetBookings(){
		boat.bookings = new ArrayList<>();
		boat.addBooking(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), boat));
		assertEquals(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), boat), boat.getBookings().get(0));
	}

    @Test
	public void testSetBookings(){
		List<Booking> bookings = new ArrayList<>();
		bookings.add(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), boat));
		boat.setBookings(bookings);
		Assert.assertTrue(bookings.equals(boat.getBookings()));
	}

	@Test
	public void testToString(){
		String string = boat.toString();
		assertEquals("B/5,1,1,Prueba/B", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(boat.equals(boat));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(boat.hashCode()==boat.getBoatId());
	}
    
}
