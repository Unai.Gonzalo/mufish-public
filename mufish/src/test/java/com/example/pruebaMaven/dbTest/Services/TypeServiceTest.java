package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.ServiceInterfaces.TypeServInt;
import edu.mondragon.mufish.Services.TypeService;
import edu.mondragon.mufish.Stubs.TypeRepoStub;

public class TypeServiceTest {

	TypeServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new TypeService(new TypeRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void createTypeTest(){
    	service.createType("Prueba");
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void readTypeTest(){
    	Type type = service.readType(5);
    	Type type2 = service.readType(15);
        assertTrue(type.getTypeId()==5);
        assertNull(type2);
    }
    
    @Test
    public void updateTypeTest(){
    	service.updateType(5,"Prueba2");
        assertTrue(service.readType(5).getName().equals("Prueba2"));
    }
    
    @Test
    public void deleteTypeTest(){
    	service.deleteType(5);
        assertTrue(service.list().size()==2);
    }
    
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	TypeRepoStub repo = new TypeRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new Type()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }
}
