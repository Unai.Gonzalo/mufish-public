package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;

public class UserTest {
    
    User user;

    @Before
	public void setUp() throws Exception {
		user = new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba");
	}

	@After
	public void tearDown() throws Exception {
		user = null;
	}

	@Test
	public void testGetId(){
		int id = user.getUserId();
		assertEquals(5, id);
	}

	@Test
	public void testGetUsername(){
		String name = user.getUsername();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetUsername(){
		user.setUsername("Prueba2");
		assertEquals("Prueba2", user.getUsername());
	}

    @Test
	public void testGetPassword(){
		String name = user.getPassword();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetPassword(){
		user.setPassword("Prueba2");
		assertEquals("Prueba2", user.getPassword());
	}

    @Test
	public void testGetSalt(){
		String name = user.getSalt();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetSalt(){
		user.setSalt("Prueba2");
		assertEquals("Prueba2", user.getSalt());
	}

    @Test
	public void testGetNan(){
		String name = user.getNan();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetNan(){
		user.setNan("Prueba2");
		assertEquals("Prueba2", user.getNan());
	}

    @Test
	public void testGetPortId(){
		int id = user.getPortId();
		assertEquals(5, id);
	}

	@Test
	public void testSetPortId(){
		user.setPortId(6);
		assertEquals(6, user.getPortId());
	}

    @Test
	public void testGetInstitution(){
		String name = user.getInstitution();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetInstitution(){
		user.setInstitution("Prueba2");
		assertEquals("Prueba2", user.getInstitution());
	}

    @Test
	public void testGetLicense(){
		String name = user.getLicense();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetLicense(){
		user.setLicense("Prueba2");
		assertEquals("Prueba2", user.getLicense());
	}

    @Test
	public void testGetType(){
		Type type = user.getType();
		assertEquals(new Type(5, "Prueba"), type);
	}

    @Test
	public void testSetType(){
		user.setType(new Type(5, "Prueba2"));
		assertEquals(new Type(5, "Prueba2"), user.getType());
	}

    @Test
	public void testAddMessageSent(){
		user.messagesSent = new ArrayList<>();
		user.addMessageSent(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		assertEquals(1, user.getMessagesSent().size());
	}

    @Test
	public void testGetMessagesSent(){
		user.messagesSent = new ArrayList<>();
		user.addMessageSent(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		assertEquals(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0), user.getMessagesSent().get(0));
	}

    @Test
	public void testSetMessagesSent(){
		List<Message> messages = new ArrayList<>();
		messages.add(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		user.setMessagesSent(messages);
		Assert.assertTrue(messages.equals(user.getMessagesSent()));
	}

    @Test
	public void testAddMessageReceived(){
		user.messagesReceived = new ArrayList<>();
		user.addMessageReceived(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		assertEquals(1, user.getMessagesReceived().size());
	}

    @Test
	public void testGetMessagesReceived(){
		user.messagesReceived = new ArrayList<>();
		user.addMessageReceived(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		assertEquals(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0), user.getMessagesReceived().get(0));
	}

    @Test
	public void testSetMessagesReceived(){
		List<Message> messages = new ArrayList<>();
		messages.add(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		user.setMessagesReceived(messages);
		Assert.assertTrue(messages.equals(user.getMessagesReceived()));
	}

    @Test
	public void testAddBoat(){
		user.boats = new ArrayList<>();
		user.addBoat(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		assertEquals(1, user.getBoats().size());
	}

    @Test
	public void testGetBoats(){
		user.boats = new ArrayList<>();
		user.addBoat(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		assertEquals(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"), user.getBoats().get(0));
	}

    @Test
	public void testSetBoats(){
		List<Boat> boats = new ArrayList<>();
		boats.add(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		user.setBoats(boats);
		Assert.assertTrue(boats.equals(user.getBoats()));
	}

	@Test
	public void testToString(){
		String string = user.toString();
		assertEquals("U/5,Prueba/U", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(user.equals(user));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(user.hashCode()==user.getUserId());
	}

}
