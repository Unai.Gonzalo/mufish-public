package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;

public class ZoneTest {
    
    Zone zone;

    @Before
	public void setUp() throws Exception {
		zone = new Zone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1);
	}

	@After
	public void tearDown() throws Exception {
		zone = null;
	}

	@Test
	public void testGetId(){
		int id = zone.getZoneId();
		assertEquals(5, id);
	}

	@Test
	public void testGetFishQuantity(){
		int fq = zone.getFishQuantity();
		assertEquals(1, fq);
	}

	@Test
	public void testSetFishQuantity(){
		zone.setFishQuantity(6);
		assertEquals(6, zone.getFishQuantity());
	}

    @Test
	public void testGetPosX(){
		int posX = zone.getPosX();
		assertEquals(1, posX);
	}

	@Test
	public void testSetPosX(){
		zone.setPosX(2);
		assertEquals(2, zone.getPosX());
	}

    @Test
	public void testGetPosY(){
		int posY = zone.getPosY();
		assertEquals(1, posY);
	}

	@Test
	public void testSetPosY(){
		zone.setPosY(2);
		assertEquals(2, zone.getPosY());
	}

    @Test
	public void testGetRadio(){
		int radio = zone.getRadio();
		assertEquals(1, radio);
	}

	@Test
	public void testSetRadio(){
		zone.setRadio(2);
		assertEquals(2, zone.getRadio());
	}

    @Test
	public void testGetSea(){
		Sea sea = zone.getSea();
		assertEquals(new Sea(5, "Prueba"), sea);
	}

    @Test
	public void testSetSea(){
		zone.setSea(new Sea(5, "Prueba2"));
		assertEquals(new Sea(5, "Prueba2"), zone.getSea());
	}

    @Test
	public void testGetState(){
		State state = zone.getState();
		assertEquals(new State(5, "Prueba"), state);
	}

    @Test
	public void testSetState(){
		zone.setState(new State(5, "Prueba2"));
		assertEquals(new State(5, "Prueba2"), zone.getState());
	}

    @Test
	public void testGetFish(){
		Fish fish = zone.getFish();
		assertEquals(new Fish(5, "Prueba"), fish);
	}

    @Test
	public void testSetFish(){
		zone.setFish(new Fish(5, "Prueba2"));
		assertEquals(new Fish(5, "Prueba2"), zone.getFish());
	}

    @Test
	public void testAddBooking(){
		zone.bookings = new ArrayList<>();
		zone.addBooking(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")));
		assertEquals(1, zone.getBookings().size());
	}

    @Test
	public void testGetBookings(){
		zone.bookings = new ArrayList<>();
		zone.addBooking(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")));
		assertEquals(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")), zone.getBookings().get(0));
	}

    @Test
	public void testSetBookings(){
		List<Booking> bookings = new ArrayList<>();
		bookings.add(new Booking(5,new Date(0), "Prueba", 5, new Zone(5, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")));
		zone.setBookings(bookings);
		Assert.assertTrue(bookings.equals(zone.getBookings()));
	}

	@Test
	public void testToString(){
		String string = zone.toString();
		assertEquals("Z/1,1,1,Prueba,Prueba", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(zone.equals(zone));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(zone.hashCode()==zone.getZoneId());
	}

}
