package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;
import edu.mondragon.mufish.ServiceInterfaces.ZoneServInt;
import edu.mondragon.mufish.Services.ZoneService;
import edu.mondragon.mufish.Stubs.ZoneRepoStub;

public class ZoneServiceTest {

	ZoneServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new ZoneService(new ZoneRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void createZoneTest(){
    	service.createZone(new Sea(5, "Prueba"), 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1);
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void readZoneTest(){
    	Zone zone = service.readZone(5);
    	Zone zone2 = service.readZone(15);
        assertTrue(zone.getZoneId()==5);
        assertNull(zone2);
    }
    
    @Test
    public void updateZoneTest(){
    	service.updateZone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 2);
        assertTrue(service.readZone(5).getRadio() == 2);
    }
    
    @Test
    public void deleteZoneTest(){
    	service.deleteZone(5);
        assertTrue(service.list().size()==2);
    }
    
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	ZoneRepoStub repo = new ZoneRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new Zone()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }
}
