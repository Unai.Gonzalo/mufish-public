package com.example.pruebaMaven.dbTest.Services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.ServiceInterfaces.StateServInt;
import edu.mondragon.mufish.Services.StateService;
import edu.mondragon.mufish.Stubs.StateRepoStub;

public class StateServiceTest {

	StateServInt service;
	
	//SPRING BOOT MOCKEATZEKO METODOA (STUB)
	
	@Before
    public void setUp() throws Exception{
        service = new StateService(new StateRepoStub());
    }

    @After
	public void tearDown() throws Exception {
		service = null;
	}
    
    @Test
    public void listTest(){
        assertTrue(service.list().size()==3);
    }
    
    @Test
    public void createStateTest(){
    	service.createState("Prueba");
        assertTrue(service.list().size()==4);
    }
    
    @Test
    public void readStateTest(){
    	State state = service.readState(5);
    	State state2 = service.readState(15);
        assertTrue(state.getStateId()==5);
        assertNull(state2);
    }
    
    @Test
    public void updateStateTest(){
    	service.updateState(5,"Prueba2");
        assertTrue(service.readState(5).getName().equals("Prueba2"));
    }
    
    @Test
    public void deleteStateTest(){
    	service.deleteState(5);
        assertTrue(service.list().size()==2);
    }
    
    @Test
    public void overrodeRepoStubNonUsedMethods(){
    	StateRepoStub repo = new StateRepoStub();
    	repo.flush();
    	assertNull(repo.saveAndFlush(new State()));
    	assertNull(repo.saveAllAndFlush(new ArrayList<>()));
    	repo.deleteAllInBatch(new ArrayList<>());
    	repo.deleteAllByIdInBatch(new ArrayList<Integer>());
    	repo.deleteAllInBatch();
    	assertNull(repo.getOne(9));
    	assertNull(repo.getById(9));
    	assertNull(repo.getReferenceById(9));
    	//assertNull(repo.findAll(Example.of(null)));
    	//assertNull(repo.findAll(Example.of(null),Sort.unsorted()));
    	assertNull(repo.saveAll(new ArrayList<>()));
    	assertNull(repo.findAllById(new ArrayList<Integer>()));
    	assertFalse(repo.existsById(9));
    	assertEquals(repo.count(), 0);
    	repo.deleteById(9);
    	repo.deleteAllById(new ArrayList<Integer>());
    	repo.deleteAll(null);
    	assertNull(repo.findAll(Sort.unsorted()));
    	assertNull(repo.findAll(Pageable.unpaged()));
    	/*assertNull(repo.findOne(Example.of(null)));
    	assertNull(repo.findAll(Example.of(null), Pageable.unpaged()));
    	assertEquals(repo.count(Example.of(null)), 0);
    	assertFalse(repo.exists(Example.of(null)));
    	assertNull(repo.findBy(Example.of(null), null));*/
    }
}
