package com.example.pruebaMaven.dbTest.Models;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;

public class TypeTest {

    Type type;

	@Before
	public void setUp() throws Exception {
		type = new Type(5,"Prueba");
	}

	@After
	public void tearDown() throws Exception {
		type = null;
	}

	@Test
	public void testGetId(){
		int id = type.getTypeId();
		assertEquals(5, id);
	}

	@Test
	public void testGetName(){
		String name = type.getName();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetName(){
		type.setName("Prueba2");
		assertEquals("Prueba2", type.getName());
	}

	@Test
	public void testAddUser(){
		type.users = new ArrayList<>();
		type.addUser(new User(5, "Prueba", "Prueba", "", null, type, null, null, null));
		assertEquals(1, type.getUsers().size());
	}

	@Test
	public void testGetUsers(){
		type.users = new ArrayList<>();
		type.addUser(new User(5, "Prueba", "Prueba", "", null, type, null, null, null));
		assertEquals(new User(5, "Prueba", "Prueba", "", null, type, null, null, null), type.getUsers().get(0));
	}

	@Test
	public void testSetUsers(){
		List<User> users = new ArrayList<>();
		users.add(new User(5, "Prueba", "Prueba", "", null, type, null, null, null));
		type.setUsers(users);
		Assert.assertTrue(users.equals(type.getUsers()));
	}

	@Test
	public void testToString(){
		String string = type.toString();
		assertEquals("T/5,Prueba/T", string);
	}
	
	@Test
	public void testEquals(){
		assertTrue(type.equals(type));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(type.hashCode()==type.getTypeId());
	}

}
