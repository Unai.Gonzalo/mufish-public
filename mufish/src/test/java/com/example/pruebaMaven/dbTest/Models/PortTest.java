package com.example.pruebaMaven.dbTest.Models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;

public class PortTest {
    
    Port port;

    @Before
	public void setUp() throws Exception {
		port = new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1);
	}

	@After
	public void tearDown() throws Exception {
		port = null;
	}

	@Test
	public void testGetId(){
		int id = port.getPortId();
		assertEquals(5, id);
	}

	@Test
	public void testGetName(){
		String name = port.getName();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetName(){
		port.setName("Prueba2");
		assertEquals("Prueba2", port.getName());
	}

    @Test
	public void testGetCity(){
		String name = port.getCity();
		assertEquals("Prueba", name);
	}

	@Test
	public void testSetCity(){
		port.setCity("Prueba2");
		assertEquals("Prueba2", port.getCity());
	}


    @Test
	public void testGetSea(){
		Sea sea = port.getSea();
		assertEquals(new Sea(5, "Prueba"), sea);
	}

    @Test
	public void testSetSea(){
		port.setSea(new Sea(5, "Prueba2"));
		assertEquals(new Sea(5, "Prueba2"), port.getSea());
	}

    @Test
	public void testGetPosX(){
		int posX = port.getPosX();
		assertEquals(1, posX);
	}

	@Test
	public void testSetPosX(){
		port.setPosX(2);
		assertEquals(2, port.getPosX());
	}

    @Test
	public void testGetPosY(){
		int posY = port.getPosY();
		assertEquals(1, posY);
	}

	@Test
	public void testSetPosY(){
		port.setPosY(2);
		assertEquals(2, port.getPosY());
	}

    @Test
	public void testAddBoat(){
		port.boats = new ArrayList<>();
		port.addBoat(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		assertEquals(1, port.getBoats().size());
	}

    @Test
	public void testGetBoats(){
		port.boats = new ArrayList<>();
		port.addBoat(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		assertEquals(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"), port.getBoats().get(0));
	}

    @Test
	public void testSetBoats(){
		List<Boat> boats = new ArrayList<>();
		boats.add(new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba"));
		port.setBoats(boats);
		Assert.assertTrue(boats.equals(port.getBoats()));
	}

	@Test
	public void testToString(){
		String string = port.toString();
		assertEquals("P/5,Prueba,Prueba,1,1/P", string);
	}

	@Test
	public void testEquals(){
		assertTrue(port.equals(port));
	}
	
	@Test
	public void testHashCode(){
		assertTrue(port.hashCode()==port.getPortId());
	}
}
