package com.example.pruebaMaven.ControllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.webSocketConfig;
import edu.mondragon.mufish.Controllers.HomeController;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Services.BoatService;
import edu.mondragon.mufish.Services.PortService;
import edu.mondragon.mufish.Services.UserService;
import edu.mondragon.mufish.Stubs.BoatRepoStub;
import edu.mondragon.mufish.Stubs.HttpSessionStub;
import edu.mondragon.mufish.Stubs.PortRepoStub;
import edu.mondragon.mufish.Stubs.UserRepoStub;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.Zona;
import jakarta.servlet.http.HttpSession;

public class HomeControllerTest{

	
	HomeController controllerh;
	UserService serviceu;
	PortService servicep;
	BoatService serviceb;
	
	@Before
	public void setUp() throws Exception {
		controllerh = new HomeController();
		serviceu = new UserService(new UserRepoStub());
		servicep = new PortService(new PortRepoStub());
		serviceb = new BoatService(new BoatRepoStub());
    	controllerh.setBoatService(serviceb);
    	controllerh.setPortService(servicep);
    	controllerh.setUserService(serviceu);
	}
	
	@After
	public void tearDown() throws Exception {
		controllerh = null;
		serviceu = null;
		servicep = null;
		serviceb = null;
	}
  
	  @Test
	  public void contextLoads() throws Exception {
		ControladorDeZonas contz=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
		contz.annadirZona(new Zona(1, new Posicion(1,1), 10, "Bien", "Luna", 1));
		  webSocketConfig.setControladorDeZonas(contz);
		  HttpSession sesion = new HttpSessionStub();
		  User user = new User();
		  ModelAndView m = new ModelAndView();
		  user.setType(new Type(10,"fisher"));
		  
		  m = controllerh.home(sesion);
		  assertEquals("login", m.getViewName());
		  
		  sesion.setAttribute("user", user);
		  controllerh.home(sesion);
		  
		  user.setType(new Type(11,"coastguard"));
		  user.setPortId(1);
		  controllerh.home(sesion);
		  
		  user.setType(new Type(12,"fishing manager"));
		  controllerh.home(sesion);
		  
		  user.setType(new Type(13,"admin"));
		  m = controllerh.home(sesion);
		  assertEquals("home", m.getViewName());
		  
		  assertNotNull(controllerh);
	  }

	
    
}
