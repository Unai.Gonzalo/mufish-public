package com.example.pruebaMaven.ControllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.Controllers.LoginController;
import edu.mondragon.mufish.Services.BoatService;
import edu.mondragon.mufish.Services.MessageService;
import edu.mondragon.mufish.Services.PortService;
import edu.mondragon.mufish.Services.SeaService;
import edu.mondragon.mufish.Services.TypeService;
import edu.mondragon.mufish.Services.UserService;
import edu.mondragon.mufish.Stubs.BoatRepoStub;
import edu.mondragon.mufish.Stubs.HttpSessionStub;
import edu.mondragon.mufish.Stubs.MessageRepoStub;
import edu.mondragon.mufish.Stubs.PortRepoStub;
import edu.mondragon.mufish.Stubs.SeaRepoStub;
import edu.mondragon.mufish.Stubs.TypeRepoStub;
import edu.mondragon.mufish.Stubs.UserRepoStub;
import jakarta.servlet.http.HttpSession;

public class LoginControllerTest{
    
    LoginController loginController;
    UserService userService;
	PortService portService;
	BoatService boatService;
    MessageService messageService;
    SeaService seaService;
    TypeService typeService;

    @Before
	public void setUp() throws Exception {
		loginController = new LoginController();
		userService = new UserService(new UserRepoStub());
		portService = new PortService(new PortRepoStub());
		boatService = new BoatService(new BoatRepoStub());
        messageService = new MessageService(new MessageRepoStub());
        seaService = new SeaService(new SeaRepoStub());
        typeService = new TypeService(new TypeRepoStub());
    	loginController.setBoatService(boatService);
    	loginController.setPortService(portService);
    	loginController.setUserService(userService);
        loginController.setMessageService(messageService);
        loginController.setSeaService(seaService);
        loginController.setTypeService(typeService);
	}
	
	@After
	public void tearDown() throws Exception {
		loginController = null;
        userService = null;
        portService = null;
        boatService = null;
        messageService = null;
        seaService = null;
        typeService = null;
	}

    @Test
    public void testLogin() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        loginController.index(sesion);
        sesion.setAttribute("lang", null);
        sesion.setAttribute("notification", null);
        loginController.login(sesion);

        assertNotNull(loginController);
    }

    @Test
    public void testErrorPage() throws Exception {
        String result = loginController.error();

        assertEquals("error", result);
    }

    @Test
    public void testLogout() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("userType", "manager");
        RedirectView view = loginController.logout(sesion);

        assertEquals("/login", view.getUrl());
    }

    @Test
    public void testSignUp() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        ModelAndView view = loginController.signup(sesion);
        
        assertEquals("signUp", view.getViewName());
    }

    @Test
    public void testDeleteUser() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        RedirectView view = loginController.languageChange(sesion, 1);
        
        assertEquals("/home", view.getUrl());
    }

    @Test
    public void testCreateUser() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        RedirectView view = loginController.createUser(sesion, "ABC1234s", "nuevoUsuario", "nuevoUsuario", "1", "AADFF243", "", "");
        
        assertEquals("/login", view.getUrl());
    }

    @Test
    public void testUsernameValidation() throws Exception {
    	loginController.initialize();
        boolean result = loginController.usernameValidation("nuevoUsuario2");
        
        assertTrue(result);
    }

    @Test
    public void testIncorrectLogin() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        RedirectView view = loginController.login(sesion, "user123", "wrongPassword");

        assertEquals("/login", view.getUrl());
    }

    @Test
    public void testCorrectLogin() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        RedirectView view = loginController.login(sesion, "user123", "admin");

        assertEquals("/home", view.getUrl());
    }
}
