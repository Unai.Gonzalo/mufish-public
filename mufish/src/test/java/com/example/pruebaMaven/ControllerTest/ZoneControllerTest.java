package com.example.pruebaMaven.ControllerTest;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;
import edu.mondragon.mufish.webSocketConfig;
import edu.mondragon.mufish.Controllers.ZoneController;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Services.BoatService;
import edu.mondragon.mufish.Services.BookingService;
import edu.mondragon.mufish.Stubs.BoatRepoStub;
import edu.mondragon.mufish.Stubs.BookingRepoStub;
import edu.mondragon.mufish.Stubs.HttpSessionStub;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.Zona;
import jakarta.servlet.http.HttpSession;

public class ZoneControllerTest {
	
	ZoneController controller;
	BoatService serviceb;
	BookingService servicebk;
	
	@Before
	public void setUp() throws Exception {
		controller = new ZoneController();
		servicebk = new BookingService(new BookingRepoStub());
		serviceb = new BoatService(new BoatRepoStub());
    	controller.setBoatService(serviceb);
    	controller.setBookingService(servicebk);
	}
	
	@After
	public void tearDown() throws Exception {
		controller = null;
		serviceb = null;
		servicebk = null;
	}
	
	@Test
	public void bookZoneTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		User user = new User();
		sesion.setAttribute("user", user);
		m = controller.bookZone(sesion);
		assertEquals("bookZone", m.getViewName());
	}
	
	@Test
	public void createBoatTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		User user = new User();
		sesion.setAttribute("user", user);
		m = controller.createBoat(sesion, "lubina", 5, 5);
		assertEquals("home", m.getUrl());
	}
	
	@Test
	public void createFishingZoneTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.createFishingZone(sesion);
		assertEquals("createFishingZone", m.getViewName());
	}
	
	@Test
	public void createNewFishingZoneTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.createNewFishingZone(sesion, Integer.valueOf(1), Integer.valueOf(2), "3", "4", "lubina", Integer.valueOf(5));
		assertEquals("mapa", m.getUrl());
	}
	
	@Test
	public void deleteFishingZoneTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.deleteFishingZone(sesion);
		assertEquals("deleteFishingZone", m.getViewName());
	}
	
	@Test
	public void createRiskZoneTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.createRiskZone(sesion);
		assertEquals("createRiskZone", m.getViewName());
	}
	
	@Test
	public void createNewRiskZoneTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.createNewRiskZone(sesion, Integer.valueOf(1), Integer.valueOf(2), "3", "4", "tormenta");
		assertEquals("mapa", m.getUrl());
	}
	
	@Test
	public void deleteExistingFishingZoneTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		ControladorDeZonas contz=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
		contz.annadirZona(new Zona(1,new Posicion(44,44),1000,"Bien","Lubina",1));
		webSocketConfig.setControladorDeZonas(contz);
		m = controller.deleteExistingFishingZone(sesion, 1);
		assertEquals("mapa", m.getUrl());
	}
	
	@Test
	public void deleteExistingRiskZoneTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		ControladorDeZonas contz=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
		contz.annadirZonaPeligro(new Zona(1,new Posicion(44,44),1000,"Contaminado","null",-1));
		webSocketConfig.setControladorDeZonas(contz);
		m = controller.deleteExistingRiskZone(sesion, 1);
		assertEquals("mapa", m.getUrl());
	}
	
	@Test
	public void deleteHomeRiskZoneTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		ControladorDeZonas contz=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
		contz.annadirZonaPeligro(new Zona(1,new Posicion(44,44),1000,"Contaminado","null",-1));
		webSocketConfig.setControladorDeZonas(contz);
		m = controller.deleteHomeRiskZone(sesion, 1);
		assertEquals("home", m.getUrl());
	}
	
	@Test
	public void deleteRiskZoneTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.deleteRiskZone(sesion);
		assertEquals("deleteRiskZone", m.getViewName());
	}
	
	@Test
	public void deleteHomeFishingZoneTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		ControladorDeZonas contz=new ControladorDeZonas(new HandlerZonas(),new HandlerZonasPeligro());
		contz.annadirZona(new Zona(1,new Posicion(44,44),1000,"Bien","Lubina",1));
		webSocketConfig.setControladorDeZonas(contz);
		m = controller.deleteHomeFishingZone(sesion, 1);
		assertEquals("home", m.getUrl());
	}
}
