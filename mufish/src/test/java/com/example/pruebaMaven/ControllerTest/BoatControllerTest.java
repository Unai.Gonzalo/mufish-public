package com.example.pruebaMaven.ControllerTest;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.Controllers.BoatController;
import edu.mondragon.mufish.Services.BoatService;
import edu.mondragon.mufish.Services.PortService;
import edu.mondragon.mufish.Services.SeaService;
import edu.mondragon.mufish.Stubs.BoatRepoStub;
import edu.mondragon.mufish.Stubs.HttpSessionStub;
import edu.mondragon.mufish.Stubs.PortRepoStub;
import edu.mondragon.mufish.Stubs.SeaRepoStub;
import jakarta.servlet.http.HttpSession;

public class BoatControllerTest {

	
	BoatController controller;
	PortService portService;
	BoatService boatService;
    SeaService seaService;

    @Before
	public void setUp() throws Exception {
		controller = new BoatController();
		portService = new PortService(new PortRepoStub());
		boatService = new BoatService(new BoatRepoStub());
        seaService = new SeaService(new SeaRepoStub());
    	controller.setBoatService(boatService);
    	controller.setPortService(portService);
        controller.setSeaService(seaService);
	}
	
	@After
	public void tearDown() throws Exception {
		controller = null;
        portService = null;
        boatService = null;
        seaService = null;
	}
	
	@Test
	public void addBoatTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.addBoat(sesion);
		assertEquals("addBoat", m.getViewName() );
	}
	
	@Test
	public void createBoatTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.createBoat(sesion, "Bilbo", 1, "src");
		assertEquals("home", m.getUrl());
	}
	
	@Test
	public void handleBoatTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.handleBoat(sesion, "accion", "1");
		assertEquals( "home", m.getUrl());
	}
	
	@Test
	public void editBoatTest() throws Exception {
		
		ModelAndView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.editBoat(sesion, "1");
		assertEquals( "editBoat", m.getViewName());
	}
	
	@Test
	public void saveBoatTest() throws Exception {
		
		RedirectView m;
		HttpSession sesion = new HttpSessionStub();
		m = controller.saveBoat(sesion, "boat", 1, "src", 1, 1, 1, 1);
		assertEquals("home", m.getUrl());
	}
	
	
}
