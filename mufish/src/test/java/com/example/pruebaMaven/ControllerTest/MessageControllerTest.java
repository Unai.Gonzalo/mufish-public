package com.example.pruebaMaven.ControllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.Controllers.MessageController;
import edu.mondragon.mufish.Services.BoatService;
import edu.mondragon.mufish.Services.MessageService;
import edu.mondragon.mufish.Services.TypeService;
import edu.mondragon.mufish.Services.UserService;
import edu.mondragon.mufish.Stubs.BoatRepoStub;
import edu.mondragon.mufish.Stubs.HttpSessionStub;
import edu.mondragon.mufish.Stubs.MessageRepoStub;
import edu.mondragon.mufish.Stubs.TypeRepoStub;
import edu.mondragon.mufish.Stubs.UserRepoStub;
import jakarta.servlet.http.HttpSession;

public class MessageControllerTest{

    MessageController messageController;
    UserService userService;
	BoatService boatService;
    MessageService messageService;
    TypeService typeService;

    @Before
	public void setUp() throws Exception {
		messageController = new MessageController();
		userService = new UserService(new UserRepoStub());
		boatService = new BoatService(new BoatRepoStub());
        messageService = new MessageService(new MessageRepoStub());
        typeService = new TypeService(new TypeRepoStub());
    	messageController.setBoatService(boatService);
    	messageController.setUserService(userService);
        messageController.setMessageService(messageService);
        messageController.setTypeService(typeService);
	}
	
	@After
	public void tearDown() throws Exception {
		messageController = null;
        userService = null;
        boatService = null;
        messageService = null;
        typeService = null;
	}

    @Test
    public void testReceivedMessages() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("user", userService.readUser(5));
        ModelAndView view = messageController.receivedMessages(sesion);

        assertEquals("receivedMessages", view.getViewName());
    }

    @Test
    public void testSentMessages() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("user", userService.readUser(5));
        ModelAndView view = messageController.sentMessages(sesion);

        assertEquals("sentMessages", view.getViewName());
    }

    @Test
    public void testWriteMessages() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("user", userService.readUser(5));
        ModelAndView view = messageController.writeMessage(sesion);

        assertEquals("writeMessage", view.getViewName());
    }

    @Test
    public void testDeleteMessages() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("user", userService.readUser(5));
        RedirectView view = messageController.deleteMessage(sesion, 5);

        assertEquals("/goReferer", view.getUrl());
    }

    @Test
    public void testMessage() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        ModelAndView view = messageController.message(sesion);

        assertEquals("readMessage", view.getViewName());
    }

    @Test
    public void testHandleMessage() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("user", userService.readUser(5));
        RedirectView view = messageController.handleMessage(sesion, "Prueba", 6, "Prueba");
        assertEquals("/sentMessages", view.getUrl());
    }
    
    @Test
    public void testGoReferer() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("goBackLink", "home");
        RedirectView view = messageController.goReferer(sesion);
        assertEquals("home", view.getUrl());
    }

    @Test
    public void testSendLocationFisher() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("goBackLink", "home");
        RedirectView view = messageController.sendLocation(sesion, 1, 1);
        assertEquals("/receivedMessages", view.getUrl());
    }

    @Test
    public void testSendLocationCoastguard() throws Exception {
        HttpSession sesion = new HttpSessionStub();
        sesion.setAttribute("goBackLink", "home");
        RedirectView view = messageController.sendLocation(sesion, 1, 3);
        assertEquals("/receivedMessages", view.getUrl());
    }
    
}
