var userType;
var map = L.map('map').setView([44.1526452,-4.9149231], 8);

var getariaPuerto = L.marker([43.3056048,-2.201962],{title:'Getaria'}).addTo(map);
var bilbaoPuerto = L.marker([43.3614591,-3.0652418],{title:'Bilbao'}).addTo(map);
var pasajesPuerto = L.marker([43.3209889,-1.9234921],{title:'Pasaia'}).addTo(map);
var hondarribiPuerto = L.marker([43.3656428,-1.7855199],{title:'Hondarribi'}).addTo(map);
var santanderPuerto = L.marker([43.4315911,-3.8151578],{title:'Santander'}).addTo(map);
var llanesPuerto = L.marker([43.420579,-4.7522407],{title:'Llanes'}).addTo(map);
var viveroPuerto = L.marker([43.6785535,-7.6013295],{title:'Vivero'}).addTo(map);
var gijonPuerto = L.marker([43.5619698,-5.6995824],{title:'Gijón'}).addTo(map);
gijonPuerto._icon.classList.add("colorMarker0");
viveroPuerto._icon.classList.add("colorMarker20");
llanesPuerto._icon.classList.add("colorMarker40");
santanderPuerto._icon.classList.add("colorMarker60");
hondarribiPuerto._icon.classList.add("colorMarker80");
pasajesPuerto._icon.classList.add("colorMarker100");
bilbaoPuerto._icon.classList.add("colorMarker120");
getariaPuerto._icon.classList.add("colorMarker140");

L.tileLayer.provider('Esri.WorldStreetMap').addTo(map);
L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
});
var lat, lng;

map.addEventListener('mousemove', function(ev) {
    lat = ev.latlng.lat;
    lng = ev.latlng.lng;
});


