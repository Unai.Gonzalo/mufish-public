window.onload = function() {
    document.getElementById("userType").addEventListener("change", changeField);
    /*var array = [];
    array = tratarMensaje("B/12,13/37,97/B", "/", ",");
    console.log(array);*/
}

function tratarMensaje(cadenaADividir,separador, separador2) {
    var arrayDeCadenas = cadenaADividir.split(separador);
    arrayDeCadenas.splice(-1);
    var tipo = arrayDeCadenas[0];
    var arrayMensaje = [];

    switch(tipo){
        case 'B': 
            arrayMensaje = tratarBarco(arrayDeCadenas, separador2);
            break;
    }
    return arrayMensaje;
}

function tratarBarco(arrayDeCadenas, separador2){

    var pos, posXY;
    var arrayLatLng = [];

    for (var i=1; i < arrayDeCadenas.length; i++) {

        posXY = arrayDeCadenas[i].split(separador2);
        pos = new L.LatLng(posXY[0], posXY[1]);
        arrayLatLng.push(pos);
        //console.log(arrayLatLng[i-1]);
    }
    return arrayLatLng;
}

function changeField(event){
    // console.log("dentro");
    var e = event.target;
    var value = e.value;
    console.log(value);
    // "${userRole} = value";

    var field;
    
    switch(value){
        case '1': field = "license";
            break;
        case '2': field = "portId";
            break;
        case '3': field = "institution";
            break;
    }
    
    showSelection(field);
    //reloadCss();
}

function showSelection(field){
    var label = document.getElementById(field);
    var hideables = document.getElementsByClassName("changefield");

    for(var i = 0; i<hideables.length; i++){
        if(hideables.item(i)==label){
            hideables.item(i).classList.remove("hideable");
            console.log("igual");
        }
        else{
            hideables.item(i).classList.add("hideable");
            console.log("distinto");
        }
    }
}

function reloadCss()
{
    var links = document.getElementsByTagName("link");
    for (var cl in links)
    {
        var link = links[cl];
        if (link.id === "signUpCss"){
            link.href += "";
        }    
    }
}
