var userType;

window.onload = function() {
    let article = document.getElementsByTagName("article");
    setBackgroundColor(article);
}

function setBackgroundColor(article){
    switch(userType){
        case "fisher":
            article.classList.add('fisherBackground');
            break;
        case "manager":
            article.classList.add('managerBackground');
            break;
        case "coastguard":
            article.classList.add('coastguardBackground');
            break;
        case "admin":
            article.classList.add('adminBackground');
            break;
    }
}

function setUserType(type) {
    userType = type;
    console.log("Tipo de usuario: "+userType);
}