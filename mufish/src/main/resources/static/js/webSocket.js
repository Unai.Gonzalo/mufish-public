var idUsuario=-1;
var barcos=[];
var guardaCostas=[];
var zonas=[];
var zonasPeligro=[];
window.onload = function() {
	ws1 = new WebSocket('wss://mufish.duckdns.org:8080/mapaB');
    ws2=new WebSocket('wss://mufish.duckdns.org:8080/mapaGC');
    ws3 = new WebSocket('wss://mufish.duckdns.org:8080/mapaZ');
    
    ws3.addEventListener('open', (event) => {
        ws3.send("VM");
        if(idUsuario!=null){
            ws3.send(idUsuario);
        }
        ws3.send("VZ");
    });

    ws4=new WebSocket('wss://mufish.duckdns.org:8080/mapaZP');
    ws5=new WebSocket('wss://mufish.duckdns.org:8080/mapaUsuario');
    ws1.onmessage = function(data){
		dibujar(data.data);
	}
	ws2.onmessage = function(data){
		dibujarG(data.data);
	}
    ws3.onmessage = function(data){
		dibujarZ(data.data);
	}
	ws4.onmessage = function(data){
		dibujarZP(data.data);
	}
    ws5.onmessage = function(data){
		dibujarUsuario(data.data);
	}
}



function setActualUser(userId){
    idUsuario = userId;
    console.log(idUsuario);
}

function disconnect() {
    if (ws1 != null) {
        ws1.close();
    }
    if (ws2 != null) {
        ws2.close();
    }
    if (ws3 != null) {
        ws3.close();
    }
    if (ws4 != null) {
        ws4.close();
    }
    if (ws5 != null) {
        ws5.close();
    }
}
function encar() {
    if (ws5 != null) {
        ws5.send("E");
    }
}
function noencar() {
    if (ws5 != null) {
        ws5.send("NE");
    }
}
function pedirZona(id,barco) {
    if (ws3 != null) {
        ws3.send("P/"+id+",Lubina,100,"+barco);
    }
}
function recargarZonasUsuario(id) {
    if (ws3 != null) {
        ws3.send(id);
    }
    idUsuario=id;
}
function ubicacionGuarda() {
    if (ws2 != null) {
        ws2.send("B/88,44.8,-4.0,Guardacostas");
    }
}

function ubicacionBarco() {
    if (ws1 != null) {
        ws1.send("B/99,44.9,-4.5,Pescador");
    }
}
function dibujar(message) {
    var latlngArray=tratarMensaje(message,"/",",");
}
function dibujarG(message) {
    var arrayDeCadenas = message.split("/");
    arrayDeCadenas.splice(-1);
    var tipo = arrayDeCadenas[0];
    if(tipo=="B"){
        guardaCostas.forEach(element=>{
            map.removeLayer(element);
        })
        var valores;
        
        for (var i=1; i < arrayDeCadenas.length; i++) {
            valores = arrayDeCadenas[i].split(",");
            var marker = L.marker(new L.LatLng(valores[0], valores[1])).addTo(map);
            marker._icon.classList.add("colorMarker200");
            guardaCostas.push(marker);
        }
    }    
}
function dibujarZ(message) {
    var arrayDeCadenas = message.split("/");
    arrayDeCadenas.splice(-1);
    var tipo = arrayDeCadenas[0];
    var valores;
    if(tipo=="Z"){
        zonas.forEach(element=>{
            map.removeLayer(element);
        })
        for (var i=1; i < arrayDeCadenas.length; i++) {
            valores = arrayDeCadenas[i].split(",");
            if(valores[4]==='Mal oleaje'){
                colorEstado= 'red';
            }else if(valores[4]==='Contaminado'){
                colorEstado='black';
            }else if(valores[4]==='Mucho viento'){
                colorEstado='white';
            }else{
                colorEstado= 'green';
            }
            zonas.push(L.circle([valores[1],  valores[2]], {
                color:colorEstado,
                fillColor: colorEstado,
                fillOpacity: 0.5,
                radius: valores[3]
            }).bindTooltip(valores[0]+":"+valores[5]).addTo(map));
        }
    }else if(tipo=="S"){
        if(document.getElementById("legendZonesMissing") != null){
            document.getElementById("legendZonesMissing").innerHTML="";
            if(arrayDeCadenas.length==1){
                document.getElementById("legendZonesMissing").innerHTML="<p class='rojo'>No zones required<p>";
            }else{
                for (var i=1; i < arrayDeCadenas.length; i++) {
                    var para = document.createElement("p");
                    var node = document.createTextNode(arrayDeCadenas[i]);
                    para.appendChild(node);
                    var element = document.getElementById("legendZonesMissing");
                    element.appendChild(para);
                }
            }
        }
    }else if(tipo==idUsuario){
        if(document.getElementById("legendZonasReservadas") != null){
            document.getElementById("legendZonasReservadas").innerHTML="";
            var arrayDeCadenas = message.split("/");
            for(var i=1;i<arrayDeCadenas.length;i++){
                var para = document.createElement("p");
                var valores2 = arrayDeCadenas[i].split(",");
                var node = document.createTextNode("BoatId: "+valores2[0]+", Zone: "+parseFloat(valores2[1]).toFixed(2)+","+parseFloat(valores2[2]).toFixed(2));
                para.appendChild(node);
                var element = document.getElementById("legendZonasReservadas");
                element.appendChild(para);
            }
        }
    }
}
function dibujarZP(message) {
    zonasPeligro.forEach(element=>{
        map.removeLayer(element);
    })
    var arrayDeCadenas = message.split("/");
    arrayDeCadenas.splice(-1);
    var tipo = arrayDeCadenas[0];
    if(tipo=="Z"){
        zonasPeligro.forEach(element=>{
            map.removeLayer(element);
        })
        var valores;
        
        for (var i=1; i < arrayDeCadenas.length; i++) {
            valores = arrayDeCadenas[i].split(",");
            if(valores[4]==='Mal oleaje'){
                colorEstado= 'red';
            }else if(valores[4]==='Contaminado'){
                colorEstado='black';
            }else if(valores[4]==='Mucho viento'){
                colorEstado='white';
            }else{
                colorEstado= 'pink';
            }
            zonasPeligro.push(L.circle([valores[1],  valores[2]], {
                color:colorEstado,
                fillColor: colorEstado,
                fillOpacity: 0.5,
                radius: valores[3]
            }).bindTooltip(valores[0]+":"+valores[4]).addTo(map));
        }
    }    
}
function tratarMensaje(cadenaADividir,separador, separador2) {
    var arrayDeCadenas = cadenaADividir.split(separador);
    arrayDeCadenas.splice(-1);
    var tipo = arrayDeCadenas[0];
    var arrayMensaje = [];

    switch(tipo){
        case 'B': 
            arrayMensaje = tratarBarco(arrayDeCadenas, separador2);
            break;
        case 'Z': 
            arrayMensaje = tratarZona(arrayDeCadenas, separador2);
            break;
    }
}

function tratarBarco(arrayDeCadenas, separador2){
    barcos.forEach(element=>{
        map.removeLayer(element);
    })
    var valores;
    for (var i=1; i < arrayDeCadenas.length; i++) {
        valores = arrayDeCadenas[i].split(separador2);
        var marker = L.marker(new L.LatLng(valores[0], valores[1])).addTo(map);
        if(valores[2]=="Pescador"){
            marker._icon.classList.add("colorMarkerRed");
        }else{
            marker._icon.classList.add("colorMarkerPink");
        }
        barcos.push(marker);
    }
}
function tratarZona(arrayDeCadenas, separador2){
    
}