package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Repositories.FishRepository;
import edu.mondragon.mufish.ServiceInterfaces.FishServInt;
import edu.mondragon.mufish.Stubs.FishRepoStub;

@Service
public class FishService implements FishServInt{
    
    @Autowired
    private FishRepository fishRepository;
    
    public FishService(FishRepository fishrepository) {
		this.fishRepository = fishrepository;
	}

	@Override
    public List<Fish> list() {
        return fishRepository.findAll();
    }

    @Override
    public void createFish(String name) {
        Fish fish = new Fish();
        fish.setName(name);
        fishRepository.save(fish);
    }

    @Override
    public Fish readFish(Integer fishId) {
        Optional<Fish> fishes = fishRepository.findById(fishId);
        Fish fish;
        if(fishes.isPresent()){
            fish = fishes.get();
        }else{
            fish = null;
        }
        return fish;
    }

    @Override
    public void updateFish(Integer fishId, String name) {
        Fish fish = readFish(fishId);
        fish.setName(name);
        fishRepository.save(fish);
    }

    @Override
    public void deleteFish(Integer id){
        Fish fish = readFish(id);
        fishRepository.delete(fish);
    }
}
