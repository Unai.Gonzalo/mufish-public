package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Repositories.BoatRepository;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;

@Service
public class BoatService implements BoatServInt{
    
    @Autowired
    private BoatRepository boatRepository;
    
    public BoatService(BoatRepository boatRepository) {
        this.boatRepository = boatRepository;
    }

    public BoatService() {
    }

    @Override
    public List<Boat> list() {
        return boatRepository.findAll();
    }

    @Override
    public void createBoat(String name, Port port, User fisher, Integer posX, Integer posY, String url) {
        Boat boat = new Boat();
        boat.setName(name);
        boat.setPort(port);
        boat.setFisher(fisher);
        boat.setPosX(posX);
        boat.setPosY(posY);
        boat.setUrl(url);
        boatRepository.save(boat);
    }

    @Override
    public Boat readBoat(Integer boatId) {
        Optional<Boat> boats = boatRepository.findById(boatId);
        Boat boat;

        if(boats.isPresent()){
            boat = boats.get();
        }else{
            boat = null;
        }

        return boat;
    }

    @Override
    public List<Boat> readBoatsByUser(User user) {

        List<Boat> boats = boatRepository.findByFisher(user);
        if(boats.isEmpty()){
            boats = null;
        }

        return boats;
    }

    @Override
    public void updateBoat(Integer boatId, String name, Port port, User fisher, Integer posX, Integer posY, String url) {
        Boat boat = readBoat(boatId);
        boat.setName(name);
        boat.setPort(port);
        boat.setFisher(fisher);
        boat.setPosX(posX);
        boat.setPosY(posY);
        boat.setUrl(url);
        boatRepository.save(boat);
    }

    @Override
    public void deleteBoat(Integer id){
        Boat boat = readBoat(id);
        boatRepository.delete(boat);
    }
}