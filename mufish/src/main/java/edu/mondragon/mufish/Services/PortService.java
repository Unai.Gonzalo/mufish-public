package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Repositories.PortRepository;
import edu.mondragon.mufish.ServiceInterfaces.PortServInt;
import edu.mondragon.mufish.Stubs.PortRepoStub;

@Service
public class PortService implements PortServInt{
    
    @Autowired
    private PortRepository portRepository;
    
    public PortService(PortRepository portRepository) {
		this.portRepository = portRepository;
	}

	public PortService() {
    }

    @Override
    public List<Port> list() {
        return portRepository.findAll();
    }

    @Override
    public void createPort(String name, String city, Sea sea, Integer posX, Integer posY) {
        Port port = new Port();
        port.setName(name);
        port.setCity(city);
        port.setSea(sea);
        port.setPosX(posX);
        port.setPosY(posY);
        portRepository.save(port);
    }

    @Override
    public Port readPort(Integer portId) {
        Optional<Port> ports = portRepository.findById(portId);
        Port port;
        if(ports.isPresent()){
            port = ports.get();
        }else{
            port = null;
        }
        return port;
    }

    @Override
    public void updatePort(Integer boatId, String name, String city, Sea sea, Integer posX, Integer posY) {
        Port port = readPort(boatId);
        port.setName(name);
        port.setCity(city);
        port.setSea(sea);
        port.setPosX(posX);
        port.setPosY(posY);
        portRepository.save(port);
    }

    @Override
    public void deletePort(Integer id){
        Port port = readPort(id);
        portRepository.delete(port);
    }
}
