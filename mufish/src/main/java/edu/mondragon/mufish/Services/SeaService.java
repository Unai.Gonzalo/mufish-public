package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Repositories.SeaRepository;
import edu.mondragon.mufish.ServiceInterfaces.SeaServInt;

@Service
public class SeaService implements SeaServInt{
    
    @Autowired
    private SeaRepository seaRepository;
    
    public SeaService(SeaRepository seaRepository) {
		this.seaRepository = seaRepository;
	}

	public SeaService() {
    }

    public List<Sea> list() {
        return seaRepository.findAll();
    }

    public void createSea(String name) {
        Sea sea = new Sea();
        sea.setName(name);
        seaRepository.save(sea);
    }

    public Sea readSea(Integer seaId) {
        Optional<Sea> seas = seaRepository.findById(seaId);
        Sea sea;
        if(seas.isPresent()){
            sea = seas.get();
        }else{
            sea = null;
        }
        return sea;
    }
    
    public void updateSea(Integer seaId, String name) {
        Sea sea = readSea(seaId);
        sea.setName(name);
        seaRepository.save(sea);
    }

    public void deleteSea(Integer id){
        Sea sea = readSea(id);
        seaRepository.delete(sea);
    }

}
