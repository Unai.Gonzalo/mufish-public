package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Repositories.TypeRepository;
import edu.mondragon.mufish.ServiceInterfaces.TypeServInt;

@Service
public class TypeService implements TypeServInt{
    
    @Autowired
    private TypeRepository typeRepository;
    
    public TypeService(TypeRepository typeRepository) {
		this.typeRepository = typeRepository;
	}

	public TypeService() {
    }

    @Override
    public List<Type> list() {
        return typeRepository.findAll();
    }

    @Override
    public void createType(String name) {
        Type type = new Type();
        type.setName(name);
        typeRepository.save(type);
    }

    @Override
    public Type readType(Integer typeId) {
        Optional<Type> types = typeRepository.findById(typeId);
        Type type;
        if(types.isPresent()){
            type = types.get();
        }else{
            type = null;
        }
        return type;
    }

    @Override
    public void updateType(Integer typeId, String name) {
        Type type = readType(typeId);
        type.setName(name);
        typeRepository.save(type);
    }

    @Override
    public void deleteType(Integer id){
        Type type = readType(id);
        typeRepository.delete(type);
    }
}
