package edu.mondragon.mufish.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Repositories.UserRepository;
import edu.mondragon.mufish.Security.PasswordEncrypter;
import edu.mondragon.mufish.ServiceInterfaces.UserServInt;

@Service
public class UserService implements UserServInt{
    
    @Autowired
    private UserRepository userRepository;
    
    public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public UserService() {
    }

    @Override
    public List<User> list() {
        return userRepository.findAll();
    }

    @Override
    public List<User> listOthers(User user) {
        List<User> list = new ArrayList<>();
        for(User u: userRepository.findAll()){
            if(!(u.getUserId().equals(user.getUserId()))){
                list.add(u);
            }
        }
        return list;
    }

    @Override
    public void createUser(String username, String password, String nan,Type type, Integer portId, String institution, String license) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(PasswordEncrypter.generateSecurePassword(password));
        user.setSalt(PasswordEncrypter.getSalt());
        user.setNan(nan);
        user.setType(type);
        user.setPortId(portId);
        user.setInstitution(institution);
        user.setLicense(license);
        userRepository.save(user);
    }

    @Override
    public User readUser(Integer userId) {
        Optional<User> users = userRepository.findById(userId);
        User user;

        if(users.isPresent()){
            user = users.get();
        }else{
            user = null;
        }

        return user;
    }

    @Override
    public List<User> readUserByType(Type type) {
        List<User> users = userRepository.findUserByType(type);

        if(users.isEmpty()){
            users = null;
        }
        
        return users;
    }

    @Override
    public User readUser(String username, String password) {
        List<User> users = userRepository.findByUsernameAndPassword(username, password);
        User user;

        if(users.isEmpty()){
            user = null;
        }else{
            user = users.get(0);
        }
        return user;
    }

    @Override
    public User readUser(String username) {
        List<User> users = userRepository.findByUsername(username);
        User user;

        if(users.size()<=0){
            user = null;
        }else{
            user = users.get(0);
        }
        return user;
    }

    @Override
    public void updateUser(Integer userId, String username, String password, String nan,Type type, Integer portId, String institution, String license) {
        User user = readUser(userId);
        user.setUsername(username);
        user.setPassword(PasswordEncrypter.generateSecurePassword(password));
        user.setSalt(PasswordEncrypter.getSalt());
        user.setNan(nan);
        user.setType(type);
        user.setPortId(portId);
        user.setInstitution(institution);
        user.setLicense(license);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id){
        User user = readUser(id);
        userRepository.delete(user);
    }

}