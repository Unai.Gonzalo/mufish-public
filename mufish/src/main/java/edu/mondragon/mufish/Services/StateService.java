package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Repositories.StateRepository;
import edu.mondragon.mufish.ServiceInterfaces.StateServInt;

@Service
public class StateService implements StateServInt{
    
    @Autowired
    private StateRepository stateRepository;
    
    public StateService(StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}

	@Override
    public List<State> list() {
        return stateRepository.findAll();
    }

    @Override
    public void createState(String name) {
        State state = new State();
        state.setName(name);
        stateRepository.save(state);
    }

    @Override
    public State readState(Integer stateId) {
        Optional<State> states = stateRepository.findById(stateId);
        State state;
        if(states.isPresent()){
            state = states.get();
        }else{
            state = null;
        }
        return state;
    }

    @Override
    public void updateState(Integer stateId, String name) {
        State state = readState(stateId);
        state.setName(name);
        stateRepository.save(state);
    }

    @Override
    public void deleteState(Integer id){
        State state = readState(id);
        stateRepository.delete(state);
    }

}
