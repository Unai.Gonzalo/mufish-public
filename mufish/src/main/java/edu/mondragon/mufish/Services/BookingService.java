package edu.mondragon.mufish.Services;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Zone;
import edu.mondragon.mufish.Repositories.BookingRepository;
import edu.mondragon.mufish.ServiceInterfaces.BookingServInt;
import edu.mondragon.mufish.Stubs.BookingRepoStub;

@Service
public class BookingService implements BookingServInt{
    
    @Autowired
    private BookingRepository bookingRepository;
    
    public BookingService(BookingRepository bookingRepository) {
		this.bookingRepository = bookingRepository;
	}

	@Override
    public List<Booking> list() {
        return bookingRepository.findAll();
    }

    @Override
    public void createBooking(Date date, String objective, Integer quantity, Zone zone, Boat boat) {
        Booking booking = new Booking();
        booking.setDate(date);
        booking.setObjective(objective);
        booking.setQuantity(quantity);
        booking.setZone(zone);
        booking.setBoat(boat);
        bookingRepository.save(booking);
    }

    @Override
    public Booking readBooking(Integer bookingId) {
        Optional<Booking> bookings = bookingRepository.findById(bookingId);
        Booking booking;
        if(bookings.isPresent()){
            booking = bookings.get();
        }else{
            booking = null;
        }
        return booking;
    }

    @Override
    public void updateBooking(Integer bookingId,Date date, String objective, Integer quantity, Zone zone, Boat boat) {
        Booking booking = readBooking(bookingId);
        booking.setDate(date);
        booking.setObjective(objective);
        booking.setQuantity(quantity);
        booking.setZone(zone);
        booking.setBoat(boat);
        bookingRepository.save(booking);
    }

    @Override
    public void deleteBooking(Integer id){
        Booking booking = readBooking(id);
        bookingRepository.delete(booking);
    }
}
