package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;
import edu.mondragon.mufish.Repositories.ZoneRepository;
import edu.mondragon.mufish.ServiceInterfaces.ZoneServInt;

@Service
public class ZoneService implements ZoneServInt{
    
    @Autowired
    private ZoneRepository zoneRepository;
    
    public ZoneService(ZoneRepository zoneRepository) {
		this.zoneRepository = zoneRepository;
	}

	@Override
    public List<Zone> list() {
        return zoneRepository.findAll();
    }

    @Override
    public void createZone(Sea sea, Integer fishQuantity,State state, Fish fish,Integer posX, Integer posY, Integer radio) {
        Zone zone = new Zone();
        zone.setSea(sea);
        zone.setFishQuantity(fishQuantity);
        zone.setState(state);
        zone.setFish(fish);
        zone.setPosX(posX);
        zone.setPosY(posY);
        zone.setRadio(radio);
        zoneRepository.save(zone);
    }

    @Override
    public Zone readZone(Integer zoneId) {
        Optional<Zone> zones = zoneRepository.findById(zoneId);
        Zone zone;
        if(zones.isPresent()){
            zone = zones.get();
        }else{
            zone = null;
        }
        return zone;
    }

    @Override
    public void updateZone(Integer zoneId, Sea sea, Integer fishQuantity,State state, Fish fish,Integer posX, Integer posY, Integer radio) {
        Zone zone = readZone(zoneId);
        zone.setSea(sea);
        zone.setFishQuantity(fishQuantity);
        zone.setState(state);
        zone.setFish(fish);
        zone.setPosX(posX);
        zone.setPosY(posY);
        zone.setRadio(radio);
        zoneRepository.save(zone);
    }

    @Override
    public void deleteZone(Integer id){
        Zone zone = readZone(id);
        zoneRepository.delete(zone);
    }

}
