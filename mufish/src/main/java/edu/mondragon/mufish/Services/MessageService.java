package edu.mondragon.mufish.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Repositories.MessageRepository;
import edu.mondragon.mufish.ServiceInterfaces.MessageServInt;

@Service
public class MessageService implements MessageServInt{
    
    @Autowired
    private MessageRepository messageRepository;
    
    public MessageService(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

	public MessageService() {
    }

    @Override
    public List<Message> list() {
        return messageRepository.findAll();
    }

    @Override
    public List<Message> readReceivedMessagesByUser(User user) {
        List<Message> messages = messageRepository.findByReceiver(user);

        if(messages.isEmpty()){
            messages = null;
        }

        return messages;
    }

    @Override
    public List<Message> readSentMessagesByUser(User user) {
        List<Message> messages = messageRepository.findBySender(user);
        
        if(messages.isEmpty()){
            messages = null;
        }

        return messages;
    }

    @Override
    public void createMessage(User sender, User receiver, String notification, String title) {
        Message message = new Message();
        message.setSender(sender);
        message.setReceiver(receiver);
        message.setTitle(title);
        message.setNotification(notification);
        message.setTitle(title);
        message.setStateRead(0);
        messageRepository.save(message);
    }

    @Override
    public Message readMessage(Integer messageId) {
        Optional<Message> messages = messageRepository.findById(messageId);
        Message message;
        if(messages.isPresent()){
            message = messages.get();
        }else{
            message = null;
        }
        return message;
    }

    @Override
    public void updateMessage(Integer messageId, User sender, User receiver, String notification, String title, Integer stateRead) {
        Message message = readMessage(messageId);
        message.setSender(sender);
        message.setReceiver(receiver);
        message.setTitle(title);
        message.setNotification(notification);
        message.setTitle(title);
        message.setStateRead(stateRead);
        messageRepository.save(message);
    }

    @Override
    public void deleteMessage(Integer id){
        Message message = readMessage(id);
        messageRepository.delete(message);
    }

    @Override
    public void updateMessageStateToRead(Integer messageId) {
        Message message = readMessage(messageId);
        message.setStateRead(1);
        messageRepository.save(message);
    }

    @Override
    public void updateMessageStateToUnread(Integer messageId) {
        Message message = readMessage(messageId);
        message.setStateRead(0);
        messageRepository.save(message);
    }

}
