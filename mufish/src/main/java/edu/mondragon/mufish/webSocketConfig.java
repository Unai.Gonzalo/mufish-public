package edu.mondragon.mufish;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;
import edu.mondragon.mufish.simulacion.SimuladorGuardacostas;
import edu.mondragon.mufish.simulacion.SimuladorPescador;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;

@Configuration
@EnableWebSocket
public class webSocketConfig implements WebSocketConfigurer {
	List<SimuladorPescador> listaSimuladorPescador;
	List<SimuladorGuardacostas> listaSimuladorGuardacostas;
	Random random;
	SimuladorEncargado encargado;
	static ControladorDeZonas contZonas;
	HandlerUsuario handlerUsuario;
	
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		random=new Random();
		HandlerPescador hBarcos=new HandlerPescador();
		HandlerGuardacostas hGuardaCostas=new HandlerGuardacostas();
		HandlerZonas hZonas=new HandlerZonas();
		HandlerZonasPeligro hZonasPeligro=new HandlerZonasPeligro();
		contZonas=new ControladorDeZonas(hZonas,hZonasPeligro);
		hZonas.setContZonas(contZonas);
		hZonasPeligro.setContZonas(contZonas);
		registry.addHandler(hBarcos, "/mapaB");
		registry.addHandler(hGuardaCostas, "/mapaGC");
		registry.addHandler(hZonas, "/mapaZ");
		registry.addHandler(hZonasPeligro, "/mapaZP");
		
		iniciarPescadores(hBarcos);
		iniciarGuardacostas(hGuardaCostas);
		iniciarEncargado();
		registry.addHandler(handlerUsuario, "/mapaUsuario");
		
	}

	static public ControladorDeZonas getControladorDeZonas(){
		return contZonas;
	}
	static public void setControladorDeZonas(ControladorDeZonas contZ){
		//Test purpose
		contZonas=contZ;
	}
	private void iniciarPescadores(HandlerPescador handler) {
		listaSimuladorPescador=new ArrayList<>();
		
		
		for(int i=1;i<10;i++){
			Posicion puerto=generarPuerto();
			SimuladorPescador pesca=new SimuladorPescador(handler, i, 0.05f,new Barco(i+90,new Posicion(puerto.getPosX(),puerto.getPosY()),"Pescador"),puerto,contZonas);
			pesca.start();
			listaSimuladorPescador.add(pesca);
		}
	}
	private void iniciarGuardacostas(HandlerGuardacostas handler){
		listaSimuladorGuardacostas=new ArrayList<>();
		for(int i=1;i<4;i++){
			SimuladorGuardacostas guarda=new SimuladorGuardacostas(handler, i, 0.05f,new Barco(i-1,new Posicion(43.3656428f,-1.7855199f),"Guardacostas"),contZonas);
			guarda.start();
			listaSimuladorGuardacostas.add(guarda);
		}
	}
	private void iniciarEncargado(){
		encargado=new SimuladorEncargado(contZonas);
		encargado.start();
		handlerUsuario=new HandlerUsuario();
		handlerUsuario.setEncargado(encargado);
	}
	private Posicion generarPuerto() {
        Posicion puerto=new Posicion(-1f,-1f);
                switch(random.nextInt(7)){
                    case 0:
						puerto=new Posicion(43.3056048f,-2.201962f);
                        break;
                    case 1:
						puerto=new Posicion(43.3614591f,-3.0652418f);
                        break;
                    case 2:
						puerto=new Posicion(43.3209889f,-1.9234921f);
                        break;
                    case 3:
						puerto=new Posicion(43.3656428f,-1.7855199f);
                        break;
					case 4:
						puerto=new Posicion(43.4315911f,-3.8151578f);
                        break;
					case 5:
						puerto=new Posicion(43.420579f,-4.7522407f);
                        break;
					case 6:
						puerto=new Posicion(43.6785535f,-7.6013295f);
                        break;
					case 7:
						puerto=new Posicion(43.5619698f,-5.6995824f);
                        break;
                }
        return puerto;
    }
}
