package edu.mondragon.mufish.Models;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "sea")
public class Sea {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer seaId;
    private String name;

    @OneToMany(mappedBy = "sea", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Port> ports;

    @OneToMany(mappedBy = "sea", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Zone> zones;

    public Sea(){};

    public Sea(Integer seaId, String name) {
        this.seaId = seaId;
        this.name = name;
    }

    public void addZone(Zone zone){
        zones.add(zone);
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }

    public void addPort(Port port){
        ports.add(port);
    }

    public List<Port> getPorts() {
        return ports;
    }

    public void setPorts(List<Port> ports) {
        this.ports = ports;
    }

    public Integer getSeaId() {
        return seaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "S/" + seaId + "," + name + "/S";
    }

    @Override
public boolean equals(Object arg0) {
    boolean igual = false;
    if(arg0 != null && ((Sea)arg0).getSeaId() == this.getSeaId()){
        igual = true;
    }
    return igual;
}
    
    @Override
    public int hashCode() {
        return seaId;
    }
}
