package edu.mondragon.mufish.Models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "message")
public class Message {

    @Id 
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer messageId;

  @ManyToOne()
    @JoinColumn(name = "senderId")
    private User sender;

  @ManyToOne()
    @JoinColumn(name = "receiverId")
    private User receiver;

  private String title;
  private String notification;
  private Integer stateRead;

  public Message(){};

  public Message(Integer messageId, User sender, User receiver, String notification, String title, Integer stateRead) {
    this.messageId = messageId;
    this.sender = sender;
    this.receiver = receiver;
    this.notification = notification;
    this.title = title;
    this.stateRead = stateRead;
}


public Integer getStateRead() {
    return stateRead;
  }

  public void setStateRead(Integer stateRead) {
    this.stateRead = stateRead;
  }

public Integer getMessageId() {
    return messageId;
}

public User getSender() {
    return sender;
}

public void setSender(User sender) {
    this.sender = sender;
}

public User getReceiver() {
    return receiver;
}

public void setReceiver(User receiver) {
    this.receiver = receiver;
}

public String getNotification() {
    return notification;
}

public void setNotification(String notification) {
    this.notification = notification;
}

public String getTitle() {
  return title;
}

public void setTitle(String title) {
  this.title = title;
}

@Override
  public String toString() {
    return "M/" + messageId + "," + sender.getUsername() + "," + receiver.getUsername() + "," + notification + "/M";
  }

  @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((Message)arg0).getMessageId() == this.getMessageId()){
            igual = true;
        }
        return igual;
    }
  
  @Override
  public int hashCode() {
    return messageId;
  }
}
