package edu.mondragon.mufish.Models;

import java.sql.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer bookingId;

    @ManyToOne
    @JoinColumn(name = "zoneId")
    Zone zone;
    
    @ManyToOne
    @JoinColumn(name = "boatId")
    Boat boat;

    private Date date;
    private String objective;
    private Integer quantity;

    public Booking(){}

    public Booking(Integer bookingId, Date date, String objective, Integer quantity, Zone zone, Boat boat) {
        this.bookingId = bookingId;
        this.date = date;
        this.objective = objective;
        this.quantity = quantity;
        this.zone = zone;
        this.boat = boat;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public Boat getBoat() {
        return boat;
    }

    public void setBoat(Boat boat) {
        this.boat = boat;
    };

    @Override
    public String toString() {
        return "BK/" + date + "," + objective + "," + quantity + "," + boat.getBoatId() + "/BK";
    }

    @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((Booking)arg0).getBookingId() == this.getBookingId()){
            igual = true;
        }
        return igual;
    }

    @Override
    public int hashCode() {
        return bookingId;
    }
}
