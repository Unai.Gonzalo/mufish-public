package edu.mondragon.mufish.Models;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "type")
public class Type {

    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer typeId;
    private String name;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<User> users;

    public Type(){};

    public Type(Integer typeId, String name) {
        this.typeId = typeId;
        this.name = name;
    }

    public void addUser(User user) {
        users.add(user);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "T/" + typeId + "," + name + "/T";
    }

    @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((Type)arg0).getTypeId() == this.getTypeId()){
            igual = true;
        }
        return igual;
    }
    
    @Override
    public int hashCode() {
        return typeId;
    }
}
