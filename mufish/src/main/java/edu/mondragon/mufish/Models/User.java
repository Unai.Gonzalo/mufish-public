package edu.mondragon.mufish.Models;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "user")
public class User {

@Id 
@GeneratedValue(strategy = GenerationType.AUTO)
private Integer userId;

private String username;
private String password;
private String salt;
private String nan;

    @ManyToOne()
    @JoinColumn(name = "typeId")
    private Type type;

private Integer portId;
private String institution;
private String license;

@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL, orphanRemoval = true)
public List<Message> messagesSent;

@OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL, orphanRemoval = true)
public List<Message> messagesReceived;

@OneToMany(mappedBy = "fisher", cascade = CascadeType.ALL, orphanRemoval = true)
public List<Boat> boats;

public User(){};

public User(Integer userId, String username, String password, String salt, String nan, Type type, Integer portId,
        String institution, String license) {
    this.userId = userId;
    this.username = username;
    this.password = password;
    this.salt = salt;
    this.nan = nan;
    this.type = type;
    this.portId = portId;
    this.institution = institution;
    this.license = license;
}

public void addBoat(Boat boat) {
    boats.add(boat);
}

public List<Boat> getBoats() {
    return boats;
}

public void setBoats(List<Boat> boats) {
    this.boats = boats;
}

public void addMessageSent(Message message) {
    messagesSent.add(message);
}

public void addMessageReceived(Message message) {
    messagesReceived.add(message);
}

public List<Message> getMessagesSent() {
    return messagesSent;
}

public List<Message> getMessagesReceived() {
    return messagesReceived;
}

public void setMessagesSent(List<Message> messages) {
    this.messagesSent = messages;
}

public void setMessagesReceived(List<Message> messages) {
    this.messagesReceived = messages;
}

public Integer getUserId() {
    return userId;
}

public String getUsername() {
    return username;
}

public void setUsername(String username) {
    this.username = username;
}

public String getPassword() {
    return password;
}

public void setSalt(String salt) {
    this.salt = salt;
}

public String getSalt() {
    return salt;
}

public void setPassword(String password) {
    this.password = password;
}

public String getNan() {
    return nan;
}

public void setNan(String nan) {
    this.nan = nan;
}

public Type getType() {
    return type;
}

public void setType(Type type) {
    this.type = type;
}

public Integer getPortId() {
    return portId;
}

public void setPortId(Integer portId) {
    this.portId = portId;
}

public String getInstitution() {
    return institution;
}

public void setInstitution(String institution) {
    this.institution = institution;
}

public String getLicense() {
    return license;
}

public void setLicense(String license) {
    this.license = license;
}

@Override
public String toString() {
    return "U/" + userId + "," + username + "/U";
}

@Override
public boolean equals(Object arg0) {
    boolean igual = false;
    if(arg0 != null && ((User)arg0).getUserId() == this.getUserId()){
        igual = true;
    }
    return igual;
}

@Override
public int hashCode() {
	// TODO Auto-generated method stub
	return userId;
}
}