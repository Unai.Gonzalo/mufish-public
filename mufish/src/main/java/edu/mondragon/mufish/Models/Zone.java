package edu.mondragon.mufish.Models;

import java.util.List;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "zone")
public class Zone {
    
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer zoneId;

    @ManyToOne()
    @JoinColumn(name = "seaId")
    private Sea sea;
    
    private Integer fishQuantity;

    @ManyToOne()
    @JoinColumn(name = "stateId")
    private State state;

    @ManyToOne()
    @JoinColumn(name = "fishId")
    private Fish fish;

    private Integer posX;
    private Integer posY;
    private Integer radio;

    @OneToMany(mappedBy = "zone")
    public
        List<Booking> bookings;

    public Zone(){};

    public Zone(Integer zoneId, Sea sea, Integer fishQuantity, State state, Fish fish, Integer posX,
            Integer posY, Integer radio) {
        this.zoneId = zoneId;
        this.sea = sea;
        this.fishQuantity = fishQuantity;
        this.state = state;
        this.fish = fish;
        this.posX = posX;
        this.posY = posY;
        this.radio = radio;
    }

    public void addBooking(Booking booking){
        bookings.add(booking);
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public Integer getZoneId() {
        return zoneId;
    }

    public Sea getSea() {
        return sea;
    }

    public void setSea(Sea sea) {
        this.sea = sea;
    }

    public Integer getFishQuantity() {
        return fishQuantity;
    }

    public void setFishQuantity(Integer fishQuantity) {
        this.fishQuantity = fishQuantity;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Fish getFish() {
        return fish;
    }

    public void setFish(Fish fish) {
        this.fish = fish;
    }

    public Integer getPosX() {
        return posX;
    }

    public void setPosX(Integer posX) {
        this.posX = posX;
    }

    public Integer getPosY() {
        return posY;
    }

    public void setPosY(Integer posY) {
        this.posY = posY;
    }

    public Integer getRadio() {
        return radio;
    }

    public void setRadio(Integer radio) {
        this.radio = radio;
    }

    @Override
    public String toString() {
        return "Z/" + posX + "," + posY + "," + radio + "," + state.getName() + "," + fish.getName();
    }

    @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((Zone)arg0).getZoneId() == this.getZoneId()){
            igual = true;
        }
        return igual;
    }

    @Override
    public int hashCode() {
        return zoneId;
    }
}
