package edu.mondragon.mufish.Models;

import java.util.List;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "boat")
public class Boat {

  @Id 
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer boatId;

  private String name;

  @ManyToOne()
    @JoinColumn(name = "portId")
    private Port port;

  @ManyToOne()
    @JoinColumn(name = "fisherId")
    private User fisher;

  private Integer posX;
  private Integer posY;
  private String url;

  @OneToMany(mappedBy = "boat")
public
    List<Booking> bookings;

  public Boat(){};

  public Boat(Integer boatId, String name, Port port, User fisher, Integer posX, Integer posY, String url) {
    this.boatId = boatId;
    this.name = name;
    this.port = port;
    this.fisher = fisher;
    this.posX = posX;
    this.posY = posY;
    this.url = url;
  }

  public void addBooking(Booking booking){
    bookings.add(booking);
  }

  public List<Booking> getBookings() {
    return bookings;
  }

  public void setBookings(List<Booking> bookings) {
    this.bookings = bookings;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getBoatId() {
    return boatId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Port getPort() {
    return port;
  }

  public void setPort(Port port) {
    this.port = port;
  }

  public User getFisher() {
    return fisher;
  }
  
  public void setFisher(User fisher) {
    this.fisher = fisher;
  }

  public Integer getPosX() {
    return posX;
  }

  public void setPosX(Integer posX) {
    this.posX = posX;
  }

  public Integer getPosY() {
    return posY;
  }

  public void setPosY(Integer posY) {
    this.posY = posY;
  }

  @Override
  public String toString() {
    return "B/" + boatId + "," + posX + "," + posY + "," + name + "/B";
  }

  @Override
  public boolean equals(Object arg0) {
    boolean igual = false;
    if( arg0 != null && ((Boat)arg0).getBoatId() == this.getBoatId()){
        igual = true;
    }
    return igual;
}

@Override
public int hashCode() {
	// TODO Auto-generated method stub
	return boatId;
}
}



