package edu.mondragon.mufish.Models;

import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "state")
public class State {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer stateId;
    private String name;

    @OneToMany(mappedBy = "state", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Zone> zones;

    public State(){};

    public State(Integer stateId, String name) {
        this.stateId = stateId;
        this.name = name;
    }

    public void addZone(Zone zone){
        zones.add(zone);
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }

    public Integer getStateId() {
        return stateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ST/" + stateId + "," + name + "/ST";
    }

    @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((State)arg0).getStateId() == this.getStateId()){
            igual = true;
        }
        return igual;
    }
    
    @Override
    public int hashCode() {
        return stateId;
    }
}
