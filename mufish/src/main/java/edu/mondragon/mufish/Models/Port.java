package edu.mondragon.mufish.Models;

import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "port")
public class Port {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer portId;
    private String name;
    private String city;

    @ManyToOne()
    @JoinColumn(name = "seaId")
    private Sea sea;

    private Integer posX;
    private Integer posY;

    @OneToMany(mappedBy = "port", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Boat> boats;

    public Port(){};

    public Port(Integer portId, String name, String city, Sea sea, Integer posX, Integer posY) {
        this.portId = portId;
        this.name = name;
        this.city = city;
        this.sea = sea;
        this.posX = posX;
        this.posY = posY;
    }

    public void addBoat(Boat boat) {
        boats.add(boat);
    }

    public List<Boat> getBoats() {
        return boats;
    }

    public void setBoats(List<Boat> boats) {
        this.boats = boats;
    }

    public Integer getPortId() {
        return portId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Sea getSea() {
        return sea;
    }

    public void setSea(Sea sea) {
        this.sea = sea;
    }

    public Integer getPosX() {
        return posX;
    }

    public void setPosX(Integer posX) {
        this.posX = posX;
    }

    public Integer getPosY() {
        return posY;
    }

    public void setPosY(Integer posY) {
        this.posY = posY;
    }

    @Override
    public String toString() {
        return "P/" + portId + "," + name + "," + city + "," + posX + "," + posY + "/P";
    }

    @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((Port)arg0).getPortId() == this.getPortId()){
            igual = true;
        }
        return igual;
    }

    @Override
    public int hashCode() {
        return portId;
    }

}
