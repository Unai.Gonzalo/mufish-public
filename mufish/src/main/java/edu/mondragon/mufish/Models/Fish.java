package edu.mondragon.mufish.Models;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "fish")
public class Fish {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer fishId;
    private String name;

    @OneToMany(mappedBy = "fish", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Zone> zones;

    public Fish(){};

    public Fish(Integer fishId, String name) {
        this.fishId = fishId;
        this.name = name;
    }

    public void addZone(Zone zone){
        zones.add(zone);
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }

    public Integer getFishId() {
        return fishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "F/" + fishId + "," + name + "/F";
    }

    @Override
    public boolean equals(Object arg0) {
        boolean igual = false;
        if(arg0 != null && ((Fish)arg0).getFishId() == this.getFishId()){
            igual = true;
        }
        return igual;
    }
    
    @Override
    public int hashCode() {
        return fishId;
    }
}
