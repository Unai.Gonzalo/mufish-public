package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.User;

public interface MessageServInt {
    public List<Message> list();
    public List<Message> readReceivedMessagesByUser(User user);
    public List<Message> readSentMessagesByUser(User user);
    public void createMessage(User sender, User receiver, String notification, String title);
    public Message readMessage(Integer messageId);
    public void updateMessage(Integer messageId, User sender, User receiver, String notification, String title, Integer stateRead);
    public void deleteMessage(Integer id);
    public void updateMessageStateToRead(Integer messageId);
    public void updateMessageStateToUnread(Integer messageId);
}
