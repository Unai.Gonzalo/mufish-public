package edu.mondragon.mufish.ServiceInterfaces;

import java.sql.Date;
import java.util.List;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Zone;

public interface BookingServInt {
    public void createBooking(Date date, String objective, Integer quantity, Zone zone, Boat boat);
    public Booking readBooking(Integer bookingId);
    public void updateBooking(Integer bookingId,Date date, String objective, Integer quantity, Zone zone, Boat boat);
    public void deleteBooking(Integer id);
    public List<Booking> list();
}
