package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.User;

public interface BoatServInt {
    public void createBoat(String name, Port port, User fisher, Integer posX, Integer posY, String url);
    public Boat readBoat(Integer boatId);
    public List<Boat> readBoatsByUser(User user);
    public void updateBoat(Integer boatId, String name, Port port, User fisher, Integer posX, Integer posY, String url);
    public void deleteBoat(Integer id);
    public List<Boat> list();
}
