package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;

public interface PortServInt {
    public List<Port> list();
    public void createPort(String name, String city, Sea sea, Integer posX, Integer posY);
    public Port readPort(Integer portId);
    public void updatePort(Integer boatId, String name, String city, Sea sea, Integer posX, Integer posY);
    public void deletePort(Integer id);
}
