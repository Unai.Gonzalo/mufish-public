package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Type;

public interface TypeServInt {
    public List<Type> list();
    public void createType(String name);
    public Type readType(Integer typeId);
    public void updateType(Integer typeId, String name);
    public void deleteType(Integer id);
}
