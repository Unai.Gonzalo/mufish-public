package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;

public interface UserServInt {
    public List<User> list();
    public List<User> listOthers(User user);
    public void createUser(String username, String password, String nan,Type type, Integer portId, String institution, String license);
    public User readUser(Integer userId);
    public List<User> readUserByType(Type type);
    public User readUser(String username, String password);
    public User readUser(String username);
    public void updateUser(Integer userId, String username, String password, String nan,Type type, Integer portId, String institution, String license);
    public void deleteUser(Integer id);
}
