package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Sea;

public interface SeaServInt {
    public List<Sea> list();
    public void createSea(String name);
    public Sea readSea(Integer seaId);
    public void updateSea(Integer seaId, String name);
    public void deleteSea(Integer id);
}
