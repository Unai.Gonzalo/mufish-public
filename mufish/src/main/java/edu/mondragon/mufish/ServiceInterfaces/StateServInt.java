package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.State;

public interface StateServInt {
    public List<State> list();
    public void createState(String name);
    public State readState(Integer stateId);
    public void updateState(Integer stateId, String name);
    public void deleteState(Integer id);
}
