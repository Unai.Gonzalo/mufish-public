package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;

public interface ZoneServInt {
    public List<Zone> list();
    public void createZone(Sea sea, Integer fishQuantity,State state, Fish fish,Integer posX, Integer posY, Integer radio);
    public Zone readZone(Integer zoneId);
    public void updateZone(Integer zoneId, Sea sea, Integer fishQuantity,State state, Fish fish,Integer posX, Integer posY, Integer radio);
    public void deleteZone(Integer id);
}
