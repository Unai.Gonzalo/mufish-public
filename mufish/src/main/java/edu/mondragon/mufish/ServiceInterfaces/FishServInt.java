package edu.mondragon.mufish.ServiceInterfaces;

import java.util.List;

import edu.mondragon.mufish.Models.Fish;

public interface FishServInt {
    public List<Fish> list();
    public void createFish(String name);
    public Fish readFish(Integer fishId);
    public void updateFish(Integer fishId, String name);
    public void deleteFish(Integer id);
}
