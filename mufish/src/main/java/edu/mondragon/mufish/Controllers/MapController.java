package edu.mondragon.mufish.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import jakarta.servlet.http.HttpSession;

@Controller
public class MapController {

	@GetMapping("/mapa")
	public ModelAndView mapa(HttpSession httpSession) {
		return new ModelAndView("mapa");
	}

}