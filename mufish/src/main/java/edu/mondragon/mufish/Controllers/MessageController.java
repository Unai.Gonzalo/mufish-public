package edu.mondragon.mufish.Controllers;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.ThreadUsuarioBarco;
import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;
import edu.mondragon.mufish.ServiceInterfaces.MessageServInt;
import edu.mondragon.mufish.ServiceInterfaces.TypeServInt;
import edu.mondragon.mufish.ServiceInterfaces.UserServInt;
import edu.mondragon.mufish.simulacion.Barco;
import edu.mondragon.mufish.simulacion.Posicion;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
public class MessageController {
	Random random = new Random();
    
    @Autowired
	MessageServInt messageService;

    @Autowired
	TypeServInt typeService;

    @Autowired
	BoatServInt boatService;

    @Autowired
	UserServInt userService;

    @GetMapping("/receivedMessages")
	public ModelAndView receivedMessages(HttpSession httpSession) {
		User user = (User) httpSession.getAttribute("user");

		List<Message> userMessages = messageService.readReceivedMessagesByUser(user);
		httpSession.setAttribute("messages", userMessages);
		return new ModelAndView("receivedMessages");
	}

	@GetMapping("/sentMessages")
	public ModelAndView sentMessages(HttpSession httpSession) {
		User user = (User) httpSession.getAttribute("user");
		List<Message> userMessages = messageService.readSentMessagesByUser(user);
		httpSession.setAttribute("messages", userMessages);
		return new ModelAndView("sentMessages");
	}

	@GetMapping("/writeMessage")
	public ModelAndView writeMessage(HttpSession httpSession) {
		User user = (User) httpSession.getAttribute("user");
		List<Boat> boatList;
		boatList = boatService.readBoatsByUser(user);
        httpSession.setAttribute("userBoatList",boatList);
		if(boatList != null){
            httpSession.setAttribute("visible",1);
		}
        else{
            httpSession.setAttribute("visible",0);
        }
		List<User> userList = userService.listOthers(user);
		httpSession.setAttribute("userList",userList);
		return new ModelAndView("writeMessage");
	}

	@GetMapping("/deleteMessage")
	public RedirectView deleteMessage(HttpSession httpSession, @RequestParam(name="messageId") Integer messageId) {
		RedirectView view = new RedirectView();
		
		messageService.deleteMessage(messageId);

		view.setUrl("/goReferer");
		
		return view;
	}

    @GetMapping("/readMessage")
	public RedirectView seeMessage(HttpSession httpSession, HttpServletRequest request, @RequestParam(name="messageId") Integer messageId) {
		RedirectView view = new RedirectView();
		view.setUrl("/message");
		String path = request.getHeader("Referer").replace("https://mufish.duckdns.org:8080", "");
		httpSession.setAttribute("goBackLink", path);
		List<Message> userMessages = (List<Message>) httpSession.getAttribute("messages");
		if(userMessages != null){
			Message message = userMessages.get(messageId);
			User user = (User) httpSession.getAttribute("user");
			if(!message.getSender().getUserId().equals(user.getUserId())){
				messageService.updateMessageStateToRead(message.getMessageId());
			}
			httpSession.setAttribute("message",message);
		}
		return view;
	}

	@GetMapping("/message")
	public ModelAndView message(HttpSession httpSession) {
		return new ModelAndView("readMessage");
	}

	@GetMapping("/handleMessage")
    public RedirectView handleMessage(HttpSession httpSession,
            @RequestParam(name="messageText") String message,
            @RequestParam(name="messageDestination") Integer userId, 
            @RequestParam(name="messageTitle") String title) {
        RedirectView view = new RedirectView();
        view.setUrl("/sentMessages");
        User destinatary = userService.readUser(userId);
        if(destinatary != null){
            User sender = (User) httpSession.getAttribute("user");
            messageService.createMessage(sender, destinatary, message, title);
        }
        return view;
    }

	@GetMapping("/sendLocation")
	public RedirectView sendLocation(HttpSession httpSession, 
			@RequestParam(name="boatId") Integer boatId,
			@RequestParam(name="typeId") Integer typeId) {
		RedirectView view = new RedirectView();
		view.setUrl("/receivedMessages");
		String tipo = null;
		
		if(typeId == 1){
			tipo = "Pescador";
		}else if(typeId == 3){
			tipo = "Guardacostas";
		}

		if(tipo != null){
			Barco barco = new Barco(boatId,new Posicion(43.7f+random.nextFloat()*2,-1.7038f-random.nextFloat()*6),tipo);
			new ThreadUsuarioBarco(barco);
		}
		
		return view;
	}

	@GetMapping("/changeMessageState")
	public RedirectView changeMessageState(HttpSession httpSession, HttpServletRequest request, 
			@RequestParam(name="messageId") Integer messageId) {
		RedirectView view = new RedirectView();
		String path = request.getHeader("Referer").replace("https://mufish.duckdns.org:8080", "");
		view.setUrl(path);
		if(messageService.readMessage(messageId).getStateRead() == 0){
			messageService.updateMessageStateToRead(messageId);
		}else{
			messageService.updateMessageStateToUnread(messageId);
		}
		return view;
	}

    @GetMapping("/goReferer")
	public RedirectView goReferer(HttpSession httpSession) {
		RedirectView view = new RedirectView();
		view.setUrl((String) httpSession.getAttribute("goBackLink"));
		return view;
	}

	public void setUserService(UserServInt userService) {
		this.userService = userService;
	}


	public void setBoatService(BoatServInt boatService) {
		this.boatService = boatService;
	}

	public void setTypeService(TypeServInt typeService) {
		this.typeService = typeService;
	}

	public void setMessageService(MessageServInt messageService) {
		this.messageService = messageService;
	}

}
