package edu.mondragon.mufish.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.ThreadUsuario;
import edu.mondragon.mufish.ThreadUsuarioZona;
import edu.mondragon.mufish.webSocketConfig;
import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;
import edu.mondragon.mufish.ServiceInterfaces.BookingServInt;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.Zona;
import jakarta.servlet.http.HttpSession;

@Controller
public class ZoneController {
	@Autowired
	BoatServInt boatService;

	@Autowired
	BookingServInt bookingService;

    @GetMapping("/bookZone")
	public ModelAndView bookZone(HttpSession httpSession) {
		User user = (User) httpSession.getAttribute("user");
		List<Boat> boatList = boatService.readBoatsByUser(user);
		httpSession.setAttribute("userBoatList", boatList);
		
		return new ModelAndView("bookZone");
	}

	@GetMapping("/requestZone")
	public RedirectView createBoat(HttpSession httpSession,
		@RequestParam(name="fishtype") String fishtype,
		@RequestParam(name="amount") Integer amount,
		@RequestParam(name="ship") Integer boatId) {

		amount = amount * 100;
		// Boat boat = boatService.readBoat(boatId);
		// Date date = new Date();
		User user = (User)httpSession.getAttribute("user");
		RedirectView view = new RedirectView();
		view.setUrl("home");

		new ThreadUsuario(user.getUserId(),fishtype,amount,boatId);		

		return view;
	}

	@GetMapping("/createFishingZone")
	public ModelAndView createFishingZone(HttpSession httpSession) {
		return new ModelAndView("createFishingZone");
	}

	@GetMapping("/createNewFishingZone")
	public RedirectView createNewFishingZone(HttpSession httpSession,
			@RequestParam(name="zoneId") Integer zoneId,
			@RequestParam(name="radio") Integer radio,
			@RequestParam(name="posX") String posX, 
			@RequestParam(name="posY") String posY, 
			@RequestParam(name="fishtype") String nombrePez, 
			@RequestParam(name="maxFish") Integer maxPeces) {
		RedirectView view = new RedirectView();
		view.setUrl("mapa");
		String estado = "Bien";
		
		Zona zona = new Zona(zoneId, new Posicion(Float.parseFloat(posX), Float.parseFloat(posY)), radio, estado, nombrePez, maxPeces * 100);
		new ThreadUsuarioZona(zona, "Create_Fishing_Zone");
		return view;
	}

	@GetMapping("/deleteFishingZone")
	public ModelAndView deleteFishingZone(HttpSession httpSession) {
		List<Zona> lista = ControladorDeZonas.getStaticListaZonas();
		httpSession.setAttribute("zones", lista);	
		return new ModelAndView("deleteFishingZone");
	}

	@GetMapping("/createRiskZone")
	public ModelAndView createRiskZone(HttpSession httpSession) {
		return new ModelAndView("createRiskZone");
	}

	@GetMapping("/createNewRiskZone")
	public RedirectView createNewRiskZone(HttpSession httpSession,
			@RequestParam(name="zoneId") Integer zoneId,
			@RequestParam(name="radio") Integer radio,
			@RequestParam(name="posX") String posX, 
			@RequestParam(name="posY") String posY, 
			@RequestParam(name="estado") String estado) {
		RedirectView view = new RedirectView();
		view.setUrl("mapa");

		Zona zona = new Zona(zoneId, new Posicion(Float.parseFloat(posX), Float.parseFloat(posY)), radio, estado, null, -1);
		new ThreadUsuarioZona(zona, "Create_Risk_Zone");
		
		
		return view;
	}

	@GetMapping("/deleteExistingFishingZone")
	public RedirectView deleteExistingFishingZone(HttpSession httpSession,
			@RequestParam(name="zoneId") Integer zoneId) {
		RedirectView view = new RedirectView();
		view.setUrl("mapa");
		
		Optional<Zona> z=webSocketConfig.getControladorDeZonas().getListaZonas().stream().filter(a->a.getId()==zoneId).findFirst();
		if(z.isPresent()){
			new ThreadUsuarioZona(z.get(),"Delete_Fishing_Zone");
		}
		return view;
	}

	@GetMapping("/deleteExistingRiskZone")
	public RedirectView deleteExistingRiskZone(HttpSession httpSession,
			@RequestParam(name="zoneId") Integer zoneId) {
		RedirectView view = new RedirectView();
		view.setUrl("mapa");
		Optional<Zona> z=webSocketConfig.getControladorDeZonas().getListaZonasPeligro().stream().filter(a->a.getId()==zoneId).findFirst();
		if(z.isPresent()){
			new ThreadUsuarioZona(z.get(),"Delete_Risk_Zone");
		}
		
		return view;
	}

	@GetMapping("/deleteRiskZone")
	public ModelAndView deleteRiskZone(HttpSession httpSession) {
		List<Zona> lista = ControladorDeZonas.getStaticListaZonasPeligro();
		httpSession.setAttribute("zones", lista);
		
		return new ModelAndView("deleteRiskZone");
	}

	@GetMapping("/deleteHomeRiskZone")
	public RedirectView deleteHomeRiskZone(HttpSession httpSession,
			@RequestParam(name="zoneId") Integer zoneId) {
		RedirectView view = new RedirectView();
		view.setUrl("home");
		Optional<Zona> z=webSocketConfig.getControladorDeZonas().getListaZonasPeligro().stream().filter(a->a.getId()==zoneId).findFirst();
		if(z.isPresent()){
			new ThreadUsuarioZona(z.get(),"Delete_Risk_Zone");
		}
		
		return view;
	}

	@GetMapping("/deleteHomeFishingZone")
	public RedirectView deleteHomeFishingZone(HttpSession httpSession,
			@RequestParam(name="zoneId") Integer zoneId) {
		RedirectView view = new RedirectView();
		view.setUrl("home");
		
		Optional<Zona> z=webSocketConfig.getControladorDeZonas().getListaZonas().stream().filter(a->a.getId()==zoneId).findFirst();
		if(z.isPresent()){
			new ThreadUsuarioZona(z.get(),"Delete_Fishing_Zone");
		}
		return view;
	}

	public void setBoatService(BoatServInt boatService) {
		this.boatService = boatService;
	}

	public void setBookingService(BookingServInt bookingService) {
		this.bookingService = bookingService;
	}

}
