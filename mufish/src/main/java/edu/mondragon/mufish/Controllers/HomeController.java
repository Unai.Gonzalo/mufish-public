package edu.mondragon.mufish.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;
import edu.mondragon.mufish.ServiceInterfaces.PortServInt;
import edu.mondragon.mufish.ServiceInterfaces.UserServInt;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;
import edu.mondragon.mufish.simulacion.Zona;
import jakarta.servlet.http.HttpSession;

@Controller
public class HomeController {

	@Autowired
	BoatServInt boatService;

	@Autowired
	PortServInt portService;

	@Autowired
	UserServInt userService;

	@GetMapping("/home")
	public ModelAndView home(HttpSession httpSession) {
		String url = "home";
		User user = (User) httpSession.getAttribute("user");
		if(user == null){
			url = "login";
		}else if(user.getType().getName().equals("fisher")){
			List<Boat> boatList = boatService.readBoatsByUser(user);
			httpSession.setAttribute("boatList", boatList);
			SimuladorEncargado.setActivo(true);
			httpSession.setAttribute("userType", "fisher");
		}else if(user.getType().getName().equals("coastguard")){
			List<Boat> boatList = boatService.readBoatsByUser(user);
			String portName = portService.readPort(user.getPortId()).getName();
			SimuladorEncargado.setActivo(true);
			httpSession.setAttribute("portName", portName);
			httpSession.setAttribute("boatList", boatList);
			httpSession.setAttribute("userType", "coastguard");
		}else if(user.getType().getName().equals("fishing manager")){
			SimuladorEncargado.setActivo(false);
			List<Zona> listaZonas = new ArrayList<>(ControladorDeZonas.getStaticListaZonas());
			List<Zona> listaZonasPeligro = new ArrayList<>(ControladorDeZonas.getStaticListaZonasPeligro());
			listaZonas.addAll(listaZonasPeligro);
			httpSession.setAttribute("listaZonas", listaZonas);
			httpSession.setAttribute("zoneListSize", listaZonas.size());
			httpSession.setAttribute("userType", "manager");
		}else if(user.getType().getName().equals("admin")){
			SimuladorEncargado.setActivo(true);
			List<User> userList = userService.listOthers(user);
			httpSession.setAttribute("userList", userList);
			httpSession.setAttribute("userType", "admin");
		}
		return new ModelAndView(url);
	}

	public void setUserService(UserServInt userService) {
		this.userService = userService;
	}


	public void setBoatService(BoatServInt boatService) {
		this.boatService = boatService;
	}

	public void setPortService(PortServInt portService) {
		this.portService = portService;
	}
	
}