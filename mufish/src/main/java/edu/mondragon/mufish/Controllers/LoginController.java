package edu.mondragon.mufish.Controllers;

import java.util.Enumeration;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Security.PasswordEncrypter;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;
import edu.mondragon.mufish.ServiceInterfaces.MessageServInt;
import edu.mondragon.mufish.ServiceInterfaces.PortServInt;
import edu.mondragon.mufish.ServiceInterfaces.SeaServInt;
import edu.mondragon.mufish.ServiceInterfaces.TypeServInt;
import edu.mondragon.mufish.ServiceInterfaces.UserServInt;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
public class LoginController {

	@Autowired
	BoatServInt boatService;

	@Autowired
	UserServInt userService;

	@Autowired
	TypeServInt typeService;

	@Autowired
	SeaServInt seaService;

	@Autowired
	PortServInt portService;

	@Autowired
	MessageServInt messageService;

    @GetMapping("/")
	public RedirectView index(HttpSession httpSession) {
		RedirectView view = new RedirectView();
		view.setUrl("/login");
		
		// initialize();
		return view;
	}

	public void initialize() {
		typeService.createType("fisher");
		typeService.createType("admin");
		typeService.createType("coastguard");
		typeService.createType("fishing manager");

		seaService.createSea("Cantabric Sea");

		portService.createPort("Bilboko Portua", "Bilbo", seaService.readSea(1), 1, 1);
		portService.createPort("Getariako Portua", "Getaria", seaService.readSea(1), 10, 10);
		portService.createPort("Hondarribiako Portua", "Hondarribia", seaService.readSea(1), 20, 20);
		portService.createPort("Pasaiako Portua", "Pasaia", seaService.readSea(1), 230, 230);

		userService.createUser("fisher1", "fisher1", "12345672L", typeService.readType(1), null , null, "clase 2");
		userService.createUser("admin", "admin", "12345671L", typeService.readType(2), null , null, null);
		userService.createUser("coastguard1", "coastguard1", "12345673L", typeService.readType(3), portService.readPort(1).getPortId() , null, null);
		userService.createUser("manager1", "manager1", "12345674L", typeService.readType(4), null , "Gobierno Vasco", null);
		userService.createUser("fisher2", "fisher2", "12345675L", typeService.readType(1), null , null, "clase 2");
		userService.createUser("coastguard2", "coastguard2", "12345677L", typeService.readType(3), portService.readPort(1).getPortId() , null, null);
		userService.createUser("manager2", "manager2", "12345678L", typeService.readType(4), null , "Institucion Nacional de Pesca", null);

		boatService.createBoat("Joxe Mari III", portService.readPort(1), userService.readUser(1), 1, 1, "joxe_mari_iii.jpg");
		boatService.createBoat("Elkano", portService.readPort(2), userService.readUser(1), 1, 1, "elkano.jpg");
		boatService.createBoat("Lazkao Txiki", portService.readPort(3), userService.readUser(1), 1, 1, "lazkao_txiki.jpg");
		boatService.createBoat("Urkullu II", portService.readPort(1), userService.readUser(1), 1, 1, "urkullu.jpg");
		boatService.createBoat("Greenpeace", portService.readPort(1), userService.readUser(3), 1, 1, "greenpeace.jpg");
		boatService.createBoat("Lazkao Txiki", portService.readPort(1), userService.readUser(5), 1, 1, "lazkao_txiki.jpg");
		boatService.createBoat("Ertzaintza", portService.readPort(2), userService.readUser(3), 1, 1, "ertzaintza.jpg");
		
		messageService.createMessage(userService.readUser(1), userService.readUser(2), "Diaz cant`t logout", "Title1");
        messageService.createMessage(userService.readUser(2), userService.readUser(1), "Diaz, I´m working on it", "Title2");
        messageService.createMessage(userService.readUser(1), userService.readUser(2), "I´m Diaz, Ander said me that he couldn´t book a zone", "Title3");
        messageService.createMessage(userService.readUser(2), userService.readUser(1), "Okey Diaz, i´ll solve it", "Title4");
	}

	@GetMapping("/login")
	public ModelAndView login(HttpSession httpSession) {
		String language = (String) httpSession.getAttribute("lang");
		if(language == null){
			httpSession.setAttribute("lang", "eng");
		}
		String notification = (String) httpSession.getAttribute("notification");
		if(notification == null){
			httpSession.setAttribute("notification","NO"); //no notifications
		}
		return new ModelAndView("login");
	}

	@GetMapping("/loginUser")
	public RedirectView login(HttpSession httpSession,
			@RequestParam(name="username") String username, 
			@RequestParam(name="password") String password) {
		
		RedirectView view = new RedirectView();
		httpSession.setAttribute("notification","NO");
		User user = checkCorrectLogin(username, password);

		if(user != null){
			httpSession.setAttribute("user", user);
			view.setUrl("/home");
		}else{
			httpSession.setAttribute("notification","USERNAME OR PASSWORD WRONG");
			httpSession.setAttribute("notificationType","alert");
			view.setUrl("/login");
		}
		return view;
	}

	public User checkCorrectLogin(String username, String password){
        User user = userService.readUser(username);
        if(user == null){
            user = null;
        }else{
			if(!PasswordEncrypter.verifyUserPassword(password, user.getPassword(), user.getSalt())){
				user = null;
			}
		}
        
        return user;
    }


	@PostMapping("/error")
	public String error(){
		return "error";
	}

	@GetMapping("/logout")
	public RedirectView logout(HttpSession httpSession) {
		RedirectView view = new RedirectView();
		String userType = (String) httpSession.getAttribute("userType");
		if(userType.equals("manager")){
			SimuladorEncargado.setActivo(true);
		}
		view.setUrl("/login");
		removeSessionAtributtes(httpSession);
		httpSession.setAttribute("notification","Correct logout!");
		httpSession.setAttribute("notificationType","notification");
		return view;
	}

	public void removeSessionAtributtes(HttpSession httpSession){
		Enumeration<String> list = httpSession.getAttributeNames();
		if(list != null){
			while(list.hasMoreElements()){
				httpSession.removeAttribute(list.nextElement());
			}
		}
	}

	@GetMapping("/signup")
	public ModelAndView signup(HttpSession httpSession) {
		httpSession.setAttribute("notification","NO");
		return new ModelAndView("signUp");
	}

	@GetMapping("/createUser")
	public RedirectView createUser(HttpSession httpSession,
		@RequestParam(name="dni") String DNI,
		@RequestParam(name="username") String username,
		@RequestParam(name="password") String password,
		@RequestParam(name="userType") String userType,
		@RequestParam(name="license") String license,
		@RequestParam(name="portId") String portId,
		@RequestParam(name="institution") String institution) {
		
		RedirectView view = new RedirectView();
		view.setUrl("/login");

		Integer portIdInteger = null;
		Integer userTypeInteger;

		if(usernameValidation(username)){
			try{
				portIdInteger = Integer.parseInt(portId);
			}catch(Exception e){}

			userTypeInteger = Integer.parseInt(userType);
			switch(userTypeInteger){
				case 1: 
					portIdInteger = null;
					institution = null;
					break;
				case 2:
					license = null;
					institution = null;
					break;
				case 3:
					license = null;
					portIdInteger = null;
					break;
				default: 
					httpSession.setAttribute("notification","Error selecting user type!");
					return view;
			}
			Type type = typeService.readType(userTypeInteger);
			userService.createUser(username ,password ,DNI , type, portIdInteger,institution, license);
			httpSession.setAttribute("notification","User succesfully created!");
			httpSession.setAttribute("notificationType","notification");
		}else{
			httpSession.setAttribute("notification","Username already exists! Try another one");
			httpSession.setAttribute("notificationType","alert");
		}
		return view;
	}

	public boolean usernameValidation(String username){
		boolean value = true;
		List<User> list = userService.list();
		for(User u: list){
			if(u.getUsername().equals(username)){
				value = false;
			}
		}
		return value;
	}

	@GetMapping("/language")
	public RedirectView languageChange(HttpSession httpSession, HttpServletRequest request,  @RequestParam(name="lang", required=false, defaultValue="eng") String language) {
		RedirectView view = new RedirectView();
		String path = request.getHeader("Referer").replace("https://mufish.duckdns.org:8080", "");
		view.setUrl(path);
		httpSession.setAttribute("lang", language);
		return view;
	}

	@GetMapping("/deleteUser")
	public RedirectView languageChange(HttpSession httpSession,  @RequestParam(name="id") Integer userId) {
		RedirectView view = new RedirectView();
		view.setUrl("/home");
		userService.deleteUser(userId);
		return view;
	}

	public void setUserService(UserServInt userService) {
		this.userService = userService;
	}


	public void setBoatService(BoatServInt boatService) {
		this.boatService = boatService;
	}

	public void setPortService(PortServInt portService) {
		this.portService = portService;
	}

	public void setTypeService(TypeServInt typeService) {
		this.typeService = typeService;
	}

	public void setMessageService(MessageServInt messageService) {
		this.messageService = messageService;
	}

	public void setSeaService(SeaServInt seaService) {
		this.seaService = seaService;
	}

}