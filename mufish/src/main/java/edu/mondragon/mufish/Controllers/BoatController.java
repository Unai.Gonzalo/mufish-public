package edu.mondragon.mufish.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.ServiceInterfaces.BoatServInt;
import edu.mondragon.mufish.ServiceInterfaces.PortServInt;
import edu.mondragon.mufish.ServiceInterfaces.SeaServInt;
import jakarta.servlet.http.HttpSession;

@Controller
public class BoatController {

	@Autowired
	BoatServInt boatService;

	@Autowired
	SeaServInt seaService;

	@Autowired
	PortServInt portService;
    
    @GetMapping("/addBoat")
	public ModelAndView addBoat(HttpSession httpSession) {
		List<Port> portList = portService.list();
		httpSession.setAttribute("portList", portList);
		return new ModelAndView("addBoat");
	}

	@GetMapping("/createBoat")
	public RedirectView createBoat(HttpSession httpSession,
		@RequestParam(name="portName") String portName,
		@RequestParam(name="portId") Integer portId,
		@RequestParam(name="imageSrc") String imageSrc) {

		User user = (User)httpSession.getAttribute("user");
		RedirectView view = new RedirectView();
		view.setUrl("home");
		
		boatService.createBoat(portName, portService.readPort(portId),user,10,10, imageSrc);
		//TEMA APARTE, NOTIF DE DIFERENTE COLOR SEGUN EXITO/NO EXITO?
		return view;
	}

	@GetMapping("/boat")
	public RedirectView handleBoat(HttpSession httpSession, 
			@RequestParam(name="action") String action, 
			@RequestParam(name="id") String id) {
		RedirectView view = new RedirectView();
		view.setUrl("home");
		Integer boatId = 0;
		try{
			boatId = Integer.valueOf(id);
		}catch(Exception e){
			action = "nothing";
		}
		
		switch(action){
			case "remove":
				boatService.deleteBoat(boatId);
				break;
			case "edit":
				view.setUrl("editBoat?id="+id);
				break;
			default: 
				break;
		}
		return view;
	}

	@GetMapping("/editBoat")
	public ModelAndView editBoat(HttpSession httpSession, 
			@RequestParam(name="id") String id) {
		List<Port> portList = portService.list();
		httpSession.setAttribute("portList", portList);
		Integer boatId = 0;
		try{
			boatId = Integer.valueOf(id);
		}catch(Exception e){
			boatId = null;
		}
		Boat boat = boatService.readBoat(boatId);
		httpSession.setAttribute("editableBoat", boat);
		return new ModelAndView("editBoat");
	}

	@GetMapping("/saveBoat")
	public RedirectView saveBoat(HttpSession httpSession, 
			@RequestParam(name="boatName") String boatName,
			@RequestParam(name="portId") Integer portId,
			@RequestParam(name="imageSrc") String imageSrc, 
			@RequestParam(name="boatId") Integer boatId, 
			@RequestParam(name="posX") Integer posX, 
			@RequestParam(name="posY") Integer posY, 
			@RequestParam(name="userId") Integer userId) {
		RedirectView view = new RedirectView();
		view.setUrl("home");
		try{
			Port port = portService.readPort(portId);
			User user = (User) httpSession.getAttribute("user");
			boatService.updateBoat(boatId, boatName, port, user, posX, posY, imageSrc);
		}catch(Exception e){

		}
		return view;
	}

	public void setBoatService(BoatServInt boatService) {
		this.boatService = boatService;
	}

	public void setSeaService(SeaServInt seaService) {
		this.seaService = seaService;
	}

	public void setPortService(PortServInt portService) {
		this.portService = portService;
	}

}
