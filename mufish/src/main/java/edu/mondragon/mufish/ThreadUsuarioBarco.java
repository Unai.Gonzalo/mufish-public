package edu.mondragon.mufish;
import java.io.IOException;
import java.util.Random;
import edu.mondragon.mufish.simulacion.Barco;

public class ThreadUsuarioBarco extends Thread   {

    Random random= new Random(); 
    Barco barco;
    public ThreadUsuarioBarco(Barco barco){
        this.barco=barco;
        this.start();
    }

    @Override
    public void run() {
            if(barco.getTipo().equals("Pescador")){
                try {
                    
                    HandlerPescador.simularActuBarco(barco.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                try {
                    HandlerGuardacostas.simularActuBarco(barco.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

}

