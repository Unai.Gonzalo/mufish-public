package edu.mondragon.mufish.Filters;

import org.springframework.web.servlet.HandlerInterceptor;

import edu.mondragon.mufish.Models.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class LoggedUserFilter implements HandlerInterceptor  {

    private static Logger log = LoggerFactory.getLogger(LoggedUserFilter.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
        HttpSession httpSession = request.getSession();
        User loggedUser = (User) httpSession.getAttribute("user");
        if(loggedUser == null){
            httpSession.setAttribute("notification","Log in to enter MUFISH!");
		    httpSession.setAttribute("notificationType","alert");
            response.sendRedirect("/login");
        }else if(loggedUser.getType().getName().equals("admin") || loggedUser.getType().getName().equals("fisher")){
            //ADMIN ACCESO A TODO
        }else{
            String path = request.getRequestURL().toString().replace("https://mufish.duckdns.org:8080", "");
            if(path.equals("/bookZone")){
                String destinyPath = request.getHeader("Referer").replace("https://mufish.duckdns.org:8080", "");
                response.sendRedirect(destinyPath);
            }
        }
        return true;
    }
    
}
