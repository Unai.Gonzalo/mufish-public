package edu.mondragon.mufish;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.mondragon.mufish.simulacion.ControladorDeDibujables;
import edu.mondragon.mufish.simulacion.Posicion;
import edu.mondragon.mufish.simulacion.SimuladorEncargado;



@Component
public class HandlerUsuario extends TextWebSocketHandler {
	SimuladorEncargado encargado;

	public void setEncargado(SimuladorEncargado encargado){
		this.encargado=encargado;
	}
	List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
	ControladorDeDibujables controlador=new ControladorDeDibujables();
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {
		if(message.getPayload().charAt(0)=='B'){
			if(message.getPayload().charAt(1)=='/'){
				String[] valores=message.getPayload().toString().split("/");
				valores=valores[1].split(",");
				Posicion pos=new Posicion(Float.valueOf(valores[0]),Float.valueOf(valores[1]));
			}
		}else if(message.getPayload().charAt(0)=='G'){
			if(message.getPayload().charAt(1)=='/'){
				String[] valores=message.getPayload().split("/");
				valores=valores[1].split(",");
				Posicion pos=new Posicion(Float.valueOf(valores[0]),Float.valueOf(valores[1]));
			}
		}
	}
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {

		sessions.add(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.remove(session);
	}
}