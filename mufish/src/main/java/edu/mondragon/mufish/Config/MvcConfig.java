package edu.mondragon.mufish.Config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import edu.mondragon.mufish.Filters.LoggedUserFilter;

@Configuration
@ComponentScan("edu.mondragon.mufish.Controllers")
public class MvcConfig implements WebMvcConfigurer {
    
    public MvcConfig(){
        super();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoggedUserFilter()).addPathPatterns("/**")
        .excludePathPatterns("/login", "/", "/loginUser", "/signup", "/createUser", "/language", "/error", "/css/**", "/images/**", "/js/**");
    }

}
