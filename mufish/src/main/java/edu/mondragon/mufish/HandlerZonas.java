package edu.mondragon.mufish;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Semaphore;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.mondragon.mufish.simulacion.ControladorDeDibujables;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Zona;



@Component
public class HandlerZonas extends TextWebSocketHandler{
	ControladorDeZonas contZonas;
	public void setContZonas(ControladorDeZonas contZonas) {
		this.contZonas = contZonas;
	}
	
	
	List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
	ControladorDeDibujables controlador=new ControladorDeDibujables();
	List<ThreadUsuario> listaUsuarios=new ArrayList<>();
	Semaphore mutex=new Semaphore(1);

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {
				String str="";
			if(message.getPayload().charAt(0)=='V' && message.getPayload().charAt(1)=='M'){
				contZonas.verZonas();
			}else if(message.getPayload().charAt(0)=='V' && message.getPayload().charAt(1)=='Z'){
				contZonas.verZonasEnFalta();
			}else{
				try{
					str=message.getPayload();
					for (Entry<Integer, Zona>  entry : contZonas.getMapaZonasPorBarcoDeUsuario().get(Integer.valueOf(message.getPayload())).entrySet()) {
						Zona z=(Zona) entry.getValue();
						str=str+"/"+entry.getKey()+","+z.getPosicion().getPosX()+","+z.getPosicion().getPosY();
					}
				}catch(NullPointerException e){
					str = "No tienes zonas";
				}
				
			}
			for(int i=0;i<sessions.size();i++) {
				WebSocketSession webSocketSession =(WebSocketSession) sessions.get(i);
					webSocketSession.sendMessage(new TextMessage(str));
			}
	}
	public synchronized void simularCreacionDeZonas(List<Zona> listaZonas) throws IOException{
		String str2=controlador.listaZonasToString(listaZonas);
		
		for(int i=0;i<sessions.size();i++) {
			WebSocketSession webSocketSession =(WebSocketSession) sessions.get(i);
			webSocketSession.sendMessage(new TextMessage(str2));
		}
	}
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {

		sessions.add(session);
		simularCreacionDeZonas(contZonas.getListaZonas());
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.remove(session);
	}
    public synchronized void dibujarZonasFalta(List<String> listaZonasEnFalta) {
		String str2="S/";
		for(String s:listaZonasEnFalta){
			
			str2=str2+s+"/";
		}
		for(int i=0;i<sessions.size();i++) {
			WebSocketSession webSocketSession =(WebSocketSession) sessions.get(i);
			try {
				webSocketSession.sendMessage(new TextMessage(str2));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

    }
	
	
}