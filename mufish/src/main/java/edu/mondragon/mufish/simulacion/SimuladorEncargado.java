package edu.mondragon.mufish.simulacion;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Random;


public class SimuladorEncargado extends Thread implements PropertyChangeListener   {

    Random random= new Random(); 
    ControladorDeZonas contZonas;
    String estado="";
    static Boolean activo;
    List<Zona> zonasConPeligro;

    public SimuladorEncargado(ControladorDeZonas contZonas){
        super("Encargado");
        activo=true;
        zonasConPeligro = new ArrayList<>();
        this.contZonas=contZonas;
        contZonas.annadirListener(this);
    }

    static public void setActivo(Boolean estado){
        activo = estado;
    }
    static public Boolean getActivo(){
        return activo;
    }
    @Override
    public void run() {
        try {
            Thread.sleep(3000);
            while (!isInterrupted()) {
                if(Boolean.TRUE.equals(activo)){
                revisarEstadoZonas();
                Thread.sleep(3000);
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    

    public void revisarEstadoZonas() throws InterruptedException {
        try{
            contZonas.revisarEstadosZonas();
        }catch (ConcurrentModificationException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("actualizarLista")){
            zonasConPeligro=contZonas.getListaZonasPeligro();
        }
    }
}

