package edu.mondragon.mufish.simulacion;

import java.util.ArrayList;
import java.util.List;

public class ControladorDeDibujables  {

    List<Barco> listaBarcos;

    public ControladorDeDibujables() {
        listaBarcos=new ArrayList<>();
    }

    public String cambiarListaBarcos(String str){
        
        Barco barco=Barco.obtenerBarcoConString(str, "/", ",");
        for(int i=0;i<listaBarcos.size();i++){
            if(listaBarcos.get(i).getId()==barco.getId()){
                listaBarcos.remove(i);
            }
        }
        listaBarcos.add(barco);
        String s=obtenerString();
        return s;
    }

    public String obtenerString() {
        String str="B";
        for(Barco b:listaBarcos){
            str=str+"/"+String.valueOf(b.getPosicion().getPosX())+","+String.valueOf(b.getPosicion().getPosY()+","+b.getTipo());
        }
        str=str+"/B";
        return str;
    }

    public String listaZonasToString(List<Zona> listaZonas) {
        String str="Z";
        for(Zona z:listaZonas){
            str+=z.toString();
        }
        str+="/Z";
        return str;
    }

}
