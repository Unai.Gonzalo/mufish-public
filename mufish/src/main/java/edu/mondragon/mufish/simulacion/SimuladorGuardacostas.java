package edu.mondragon.mufish.simulacion;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.mondragon.mufish.HandlerGuardacostas;



public class SimuladorGuardacostas extends Thread implements PropertyChangeListener   {
    HandlerGuardacostas handler;
    Random random= new Random(); 
    Barco barco;
    Posicion objetivo;
    String estado;
    float speed;
    int numeroDeVuetas=0;
    float desplazamientoX,desplazamientoY;
    ControladorDeZonas contZonas;
    float maxAnchura=-7.27319f;
    float minAnchura=-1.7038f;
    float maxAltura=45.1797f;
    float minAltura=43.550143064473f;
    List<Zona> zonasConPeligro;
    String enPuerto="En puerto";
    int contador=0;
    int id;
    public SimuladorGuardacostas(HandlerGuardacostas handler,int id,float speed,Barco barco,ControladorDeZonas contZonas){
        super("Guardacostas"+id);
        this.id=id;
        zonasConPeligro=new ArrayList<>();
        this.contZonas=contZonas;
        contZonas.annadirListener(this);
        estado=enPuerto;
        this.speed=speed;
        objetivo=new Posicion(5f,1f);
        this.handler=handler;
        this.barco=barco;
        try {
            handler.simularActuBarco(barco.toString());
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public float calculosTrayecto() {
        float h=(float)Math.sqrt((float) (Math.pow(Math.abs(barco.getPosicion().getPosX()-objetivo.getPosX()),2)+Math.pow(Math.abs(barco.getPosicion().getPosY()-objetivo.getPosY()),2)));
        float camino=h/speed;
        desplazamientoX=(objetivo.getPosX()-barco.getPosicion().getPosX())/camino;
        desplazamientoY=(objetivo.getPosY()-barco.getPosicion().getPosY())/camino;
        return h;
    }

    

    @Override
    public void run() {
        try {
            Thread.sleep((long)5000+this.id*1000);
            while (!isInterrupted()) {
                maquinaDeEstados();
                Thread.sleep(300);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean barcoEnPosicion(float distanciaMax) {
        return objetivo.distancia(barco.getPosicion())>distanciaMax;
    }
    public void maquinaDeEstados() throws InterruptedException, IOException {
        switch(estado){
            case "En puerto":
                    Thread.sleep(random.nextInt(50)*(long)100);
                    objetivo=new Posicion(45.1797f,-1.7038f);
                    calculosTrayecto();
                    estado="Buscando por el mar";
                break;
            case "Buscando por el mar":
                if(contador==10){
                    contador=0;
                    for(int i=0;i<zonasConPeligro.size()-1;i++){
                        if(zonasConPeligro.get(i).getPosicion().distancia(barco.getPosicion())<0.1 && random.nextInt(2)==1){
                            contZonas.eliminarZonaPeligro(zonasConPeligro.get(i));
                        }
                    }
                }else{
                    contador++;
                }
                if(random.nextInt(250)==1){
                    contZonas.annadirZonaPeligro(new Zona(-1,new Posicion(barco.getPosicion().getPosX(), barco.getPosicion().getPosY()) , random.nextInt(500)*50, generarEstadoZona(),null,-1));
                }
                haciaObjetivo();
                break;
            case "Envia informacion del estado":
                haciaObjetivo();
                break;
            case "Vuelta puerto":
                haciaPuerto();
                break;
            default:
                break;
        }
        
    }

    private String generarEstadoZona() {
        String estadoZona;
        switch(random.nextInt(3)){
            case 0:
            estadoZona="Contaminado";
                break;
            case 1:
            estadoZona="Mucho viento";
                break;
            case 2:
            estadoZona="Mal oleaje";
                break;
            default:
            estadoZona="Regular";
            break;
        }
        return estadoZona;
    }

    

    private void haciaPuerto() throws InterruptedException {
        if(barcoEnPosicion(0.03f)){
            
            barco.getPosicion().incX(desplazamientoX);
            barco.getPosicion().incY(desplazamientoY);
            barco.getPosicion().setPosX((barco.getPosicion().getPosX()));
            barco.getPosicion().setPosY((barco.getPosicion().getPosY()));
            
            try {
                handler.simularActuBarco(barco.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Thread.sleep(5000);
            estado=enPuerto;
        }
    }
    public void setEstado(String estado){
        this.estado=estado;
    }
    public String getEstado(){
        return estado;
    }
    private void haciaObjetivo() throws InterruptedException {
        
        if(barcoEnPosicion(0.03f)){
            barco.getPosicion().incX(desplazamientoX);
            barco.getPosicion().incY(desplazamientoY);
            barco.getPosicion().setPosX((barco.getPosicion().getPosX()));
            barco.getPosicion().setPosY((barco.getPosicion().getPosY()));
            
            try {
                handler.simularActuBarco(barco.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            
            if(numeroDeVuetas==6){
                objetivo=new Posicion(43.3656428f,-1.7855199f);
                calculosTrayecto();
                estado="Vuelta puerto";
                numeroDeVuetas=0;
            }else{
                if(numeroDeVuetas%2==1){
                    objetivo=new Posicion(objetivo.getPosX()-((float)(maxAltura-minAltura)/6),minAnchura);
                    calculosTrayecto();
                }else{
                    objetivo=new Posicion(objetivo.getPosX()-((float)(maxAltura-minAltura)/6),maxAnchura);
                    calculosTrayecto();
                }
                numeroDeVuetas++;
            }
            
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("actualizarLista")){
            zonasConPeligro=contZonas.getListaZonasPeligro();
        }
        
    }
}
