package edu.mondragon.mufish.simulacion;

public class Barco {
    int id;
    Posicion posicion;
    String tipo;
    public Barco(int id,Posicion posicion,String tipo){
        this.id=id;
        this.posicion=posicion;
        this.tipo=tipo;
    }
    public static Barco  obtenerBarcoConString(String str, String separador, String separador2){
        String[] valores = str.split(separador);
        String[] valores2 = valores[1].split(separador2);
        return (new Barco(Integer.valueOf(valores2[0]),new Posicion(Float.valueOf(valores2[1]),  Float.valueOf(valores2[2])),valores2[3]));
    }
    public int getId() {
        return id;
    }
    public Posicion getPosicion() {
        return posicion;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }
    @Override
    public String toString() {
        return "B/" + id + "," + posicion.getPosX() + "," + posicion.getPosY() +","+tipo+ "/";
    }
    
    @Override
    public int hashCode() {
        return id;
    }
    
    @Override
    public boolean equals(Object arg0) {
        boolean value=false;
        if(arg0 instanceof Barco){
            Barco barcoNuevo=(Barco)arg0;
            if(barcoNuevo.getId()==id && barcoNuevo.getPosicion().getPosX()==posicion.getPosX() && barcoNuevo.getPosicion().getPosY()==posicion.getPosY() && tipo.equals(barcoNuevo.getTipo())){
                value=true;
            }
        }
        return value;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
