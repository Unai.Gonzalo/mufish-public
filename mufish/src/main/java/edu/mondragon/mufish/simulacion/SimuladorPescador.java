package edu.mondragon.mufish.simulacion;

import java.io.IOException;
import java.util.Random;

import edu.mondragon.mufish.HandlerPescador;

public class SimuladorPescador extends Thread {
    HandlerPescador handler;
    Random random= new Random(); 
    Barco barco;
    String pez;
    int cantidadPeces;
    Posicion puerto;
    Posicion objetivo;
    String estado;
    float speed;
    float desplazamientoX,desplazamientoY;
    ControladorDeZonas contZonas;
    public SimuladorPescador(HandlerPescador handler,int id,float speed,Barco barco,Posicion puerto,ControladorDeZonas contZonas) {
        super("SimuladorPescador"+id);
        pez="Lubina";
        this.contZonas=contZonas;
        estado="Sin Objetivo";
        cantidadPeces=100;
        objetivo=new Posicion(5f,1f);
        this.speed=speed;
        this.puerto=puerto;
        this.handler=handler;
        this.barco=barco;
        try {
            handler.simularActuBarco(barco.toString());
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public float calculosTrayecto() {
        
        float h=(float)Math.sqrt((float) (Math.pow(Math.abs(barco.getPosicion().getPosX()-objetivo.getPosX()),2)+Math.pow(Math.abs(barco.getPosicion().getPosY()-objetivo.getPosY()),2)));
        float camino=h/speed;
        desplazamientoX=(objetivo.getPosX()-barco.getPosicion().getPosX())/camino;
        desplazamientoY=(objetivo.getPosY()-barco.getPosicion().getPosY())/camino;
        return h;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            while (!isInterrupted()) {
                maquinaDeEstados();
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void setEstado(String estado){
        this.estado=estado;
    }
    public String getEstado(){
        return estado;
    }
    public boolean barcoEnPosicion(float distanciaMax){
        return objetivo.distancia(barco.getPosicion())>distanciaMax;
    }
    public void maquinaDeEstados() throws InterruptedException, IOException {
        switch(estado){
            case "Sin Objetivo":
                    pez=generarPez();
                    int a=random.nextInt(5);
                    while(a!=1){
                        Thread.sleep(random.nextInt(500)+(long)100);
                        a=random.nextInt(5);
                    }
                    estado="Con Objetivo";
                break;
            case "Con Objetivo":
                Zona zonaObj=contZonas.pedirZona(this);
                while(zonaObj==null){
                    System.out.println(this.getName()+" = esta en bucle ");
                    zonaObj=contZonas.pedirZona(this);
                }
                objetivo=new Posicion(zonaObj.getPosicion().getPosX(),zonaObj.getPosicion().getPosY());
                calculosTrayecto();
                estado="Hacia Objetivo";
                break;
            case "Hacia Objetivo":
                haciaObjetivo();
                break;
            case "Pescando":
                Thread.sleep(500);
                objetivo.setPosX(puerto.getPosX());
                objetivo.setPosY(puerto.getPosY());
                calculosTrayecto();
                estado="Vuelta Puerto";
                break;
            case "Vuelta Puerto":
                haciaPuerto();
                break;
        }
        
    }
    private String generarPez() {
        String pez="";
                switch(random.nextInt(5)){
                    case 0:
                        pez="Lubina";
                        break;
                    case 1:
                        pez="Gallo";
                        break;
                    case 2:
                        pez="Sardina";
                        break;
                    case 3:
                        pez="Luna";
                        break;
                    case 4:
                        pez="Tiburon";
                        break;
                }
        return pez;
    }
    private void haciaPuerto() throws InterruptedException {
        if(barcoEnPosicion(0.03f)){
            
            barco.getPosicion().incX(desplazamientoX);
            barco.getPosicion().incY(desplazamientoY);
            barco.getPosicion().setPosX((barco.getPosicion().getPosX()));
            barco.getPosicion().setPosY((barco.getPosicion().getPosY()));
            
            try {
                handler.simularActuBarco(barco.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            estado="Sin Objetivo";
        }
    }

    private void haciaObjetivo() throws InterruptedException {
        if(barcoEnPosicion(0.03f)){
            barco.getPosicion().incX(desplazamientoX);
            barco.getPosicion().incY(desplazamientoY);
            barco.getPosicion().setPosX((barco.getPosicion().getPosX()));
            barco.getPosicion().setPosY((barco.getPosicion().getPosY()));
            
            try {
                handler.simularActuBarco(barco.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            estado="Pescando";
        }
    }


    public String getPez() {
        return pez;
    }

    public void setPez(String pez) {
        this.pez = pez;
    }

    public int getCantidadPeces() {
        return cantidadPeces;
    }

    public void setCantidadPeces(int cantidadPeces) {
        this.cantidadPeces = cantidadPeces;
    }
    
}
