package edu.mondragon.mufish.simulacion;

public class Posicion {
    float posX;
    float posY;

    public Posicion(float posX, float posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public float getPosX() {
        return posX;
    }

    public float getPosY() {
        return posY;
    }


    public void setPosX(float posX) {
        this.posX = posX;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    @Override
    public int hashCode() {

        return (this.posX + " " + this.posY).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Posicion)) return false;
        Posicion posicion = (Posicion) obj;
        return (this.posX == posicion.posX) && (this.posY == posicion.posY);
    }

    public float distancia(Posicion p) {
        return (float) Math.sqrt(Math.pow(this.posX - p.posX, 2.0) + Math.pow(this.posY - p.posY, 2.0));
    }

    @Override
    public String toString() {
        return "(" + posX + "," + posY + ")";
    }

    public void incX(float i) {
        this.posX += (float)i;
    }

    public void incY(float i) {
        this.posY += (float)i;
    }
}