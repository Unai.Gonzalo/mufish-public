package edu.mondragon.mufish.simulacion;

public class Zona {
    int id;
    int radio;
    String estado;
    Posicion posicion;
    String pez;
    int maxPeces;
    
    public Zona(int id,Posicion pos,int radio,String estado,String pez, int maxPeces){
        this.pez=pez;
        this.id=id;
        this.maxPeces=maxPeces;
        this.posicion=pos;
        this.radio=radio;
        this.estado=estado;
    }
    
    public int getId() {
        return id;
    }
    
    public int getRadio() {
        return radio;
    }
    public String getEstado() {
        return estado;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public void setRadio(int radio) {
        this.radio = radio;
    }
    public void setEstado(String estado){
        this.estado=estado;
    }
    public Posicion getPosicion() {
        return posicion;
    }
    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }
    @Override
    public String toString() {
        return "/" +id+"," + posicion.getPosX() + "," + posicion.getPosY() +"," + radio +"," + estado + ","+ pez;
    }
    public String getPez() {
        return pez;
    }

    public void setPez(String pez) {
        this.pez = pez;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object arg0) {
        boolean value=false;
        if(arg0 instanceof Zona){
            Zona zonaNueva=(Zona)arg0;
            if(zonaNueva.getId()==id && zonaNueva.getPosicion().getPosX()==posicion.getPosX() &&
                zonaNueva.getPosicion().getPosY()==posicion.getPosY() && zonaNueva.getRadio()==radio &&
                zonaNueva.getEstado().equals(estado)){
            value=true;
        }
        }
        return value;
    }

    public int getMaxPeces() {
        return maxPeces;
    }

    public void setMaxPeces(int maxPeces) {
        this.maxPeces = maxPeces;
    }

    
}
