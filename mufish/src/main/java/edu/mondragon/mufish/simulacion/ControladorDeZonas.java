package edu.mondragon.mufish.simulacion;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Semaphore;

import edu.mondragon.mufish.HandlerZonas;
import edu.mondragon.mufish.HandlerZonasPeligro;

public class ControladorDeZonas{
    //Variables para las ids
    Integer contadorIdZonas=0;
    Integer contadorIdZonasPeligro=0;
    //Listas de zonas y mapas para la gestion de las zonas
    public static List<Zona> staticListaZonas;
    public static List<Zona> staticListaZonasPeligro;
    Map<Integer,Map<Integer,Zona>> mapaZonasPorBarcoDeUsuario=new HashMap<>();
    public List<Zona> listaZonas;
    public List<Zona> listaZonasPeligro;
    public List<String> listaZonasEnFalta;
    Map<String,Semaphore> mapaPezCola;
    //Handlers
    HandlerZonas handlerZonas;
    HandlerZonasPeligro handlerZonasPeligro;
    //Semaforos para la sincronizacion
    Semaphore mutexZ;
    Semaphore mutexZP;
    Semaphore mutexMapa;
    Semaphore mutexMapaUsuarioBarcoZona;
    //Utiles necesarios para diferentes funcionalidades
    Random random;
    PropertyChangeSupport conector;

    public ControladorDeZonas(HandlerZonas handlerZonas,HandlerZonasPeligro handlerZonasPeligro) {
        this.handlerZonas=handlerZonas;
        this.handlerZonasPeligro=handlerZonasPeligro;
        mutexZ=new Semaphore(1);
        mutexMapa=new Semaphore(1);
        mutexZP=new Semaphore(1);
        mutexMapaUsuarioBarcoZona=new Semaphore(1);
        mapaPezCola=new HashMap<>();
        listaZonas=new ArrayList<>();
        listaZonasPeligro=new ArrayList<>();
        random=new Random();
        conector=new PropertyChangeSupport(this);
        listaZonasEnFalta=new ArrayList<>();
    }

    public List<Zona> getListaZonas() {
        return listaZonas;
    }
    public static List<Zona> getStaticListaZonas() {
        return staticListaZonas;
    }

    public static List<Zona> getStaticListaZonasPeligro() {
        return staticListaZonasPeligro;
    }

    public List<Zona> getListaZonasPeligro() {
        return listaZonasPeligro;
    }

    public void annadirListener(PropertyChangeListener listener){
        conector.addPropertyChangeListener(listener);
    }

    public void eliminarListener(PropertyChangeListener listener){
        conector.removePropertyChangeListener(listener);
    }

    public void actualizarLista(){
        this.staticListaZonas = listaZonas;
        this.staticListaZonasPeligro = listaZonasPeligro;
    }

    public Map<Integer, Map<Integer, Zona>> getMapaZonasPorBarcoDeUsuario() {
        return mapaZonasPorBarcoDeUsuario;
    }

    public void guardarZonaPorBarcoDeUsuario(Integer userId,Integer barcoId, Zona zona) throws InterruptedException{
        //Esta funcion guarda las zonas asignadas a los barcos de cada usuario para ello usa el "mapaZonasPorBarcoDeUsuario"
        //parametros(id usuario, id barco, Zona)
		mutexMapaUsuarioBarcoZona.acquire();
        if(!mapaZonasPorBarcoDeUsuario.containsKey(userId)){
            Map<Integer,Zona> mapa=new HashMap<>();
            mapa.put(barcoId, zona);
            mapaZonasPorBarcoDeUsuario.put(userId, mapa);
        }else{
            if(!mapaZonasPorBarcoDeUsuario.get(userId).containsKey(barcoId)){
                mapaZonasPorBarcoDeUsuario.get(userId).put(barcoId,zona);
            }
        }
		mutexMapaUsuarioBarcoZona.release();
    }

    
    public Zona pedirZona(SimuladorPescador pescador) throws InterruptedException, IOException{
        Zona zonaFinal=null;

        mirarSiExistePezEnLaCola(pescador.getPez());

        mapaPezCola.get(pescador.getPez()).acquire();
        mutexZ.acquire();

        zonaFinal=obtenerZonaPorPezYCantidad(pescador.getPez(),pescador.getCantidadPeces());
        
        //Si existe zona con el pez objetivo pero no tiene cantidad suficiente va a dar null ZonaFinal por ello esta este if
        if(zonaFinal==null && !listaZonasEnFalta.contains(pescador.getPez())){
            mutexMapa.acquire();
            listaZonasEnFalta.add(pescador.getPez());
            mutexMapa.release();
            handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
        }
        gestionDeTokensFinal(pescador.getPez());
        mutexZ.release();
        actualizarLista();
        return zonaFinal;
    }
    public Zona pedirZonaUsuario(String pez, Integer cantidadDePeces) throws InterruptedException {
        Zona zonaFinal=null;

        mirarSiExistePezEnLaCola(pez);

        mapaPezCola.get(pez).acquire();
        mutexZ.acquire();

        zonaFinal=obtenerZonaPorPezYCantidad(pez, cantidadDePeces);

        //Si existe zona con el pez objetivo pero no tiene cantidad suficiente va a dar null ZonaFinal por ello esta este if
        if(zonaFinal==null && !listaZonasEnFalta.contains(pez)){
            mutexMapa.acquire();
            listaZonasEnFalta.add(pez);
            mutexMapa.release();
            handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
        }
        gestionDeTokensFinal(pez);
        mutexZ.release();
        return zonaFinal;
    }
    private void gestionDeTokensFinal(String pez) {
        if(listaZonas.stream().filter(a->a.getPez().equals(pez) && a.getMaxPeces()>=100).count()!=0){
            mapaPezCola.get(pez).release();
        }else{
            if(!listaZonasEnFalta.contains(pez)){
                listaZonasEnFalta.add(pez);
                handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
            }
        }
    }

    private Zona obtenerZonaPorPezYCantidad(String pez, int cantidadPeces) {
        Zona zonaFinal=null;
        for(Zona zona: listaZonas){
            if(zona.getPez().equals(pez) && zona.getMaxPeces() >= cantidadPeces){
                zona.setMaxPeces(zona.getMaxPeces()-cantidadPeces);
                zonaFinal=zona;
                break;
            }
        }
        return zonaFinal;
    }

    private void mirarSiExistePezEnLaCola(String pez) throws InterruptedException {
        mutexMapa.acquire();
        if(!mapaPezCola.containsKey(pez)){
            mapaPezCola.put(pez,new Semaphore(0));
            if(!listaZonasEnFalta.contains(pez)){
                listaZonasEnFalta.add(pez);
                handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
            }
        }
        mutexMapa.release();
    }

    public void annadirZonaPeligro(Zona zona) throws InterruptedException, IOException{
        mutexZP.acquire();
        zona.setId(contadorIdZonasPeligro++);
        listaZonasPeligro.add(zona);
        handlerZonasPeligro.simularCreacionDeZonasPeligro(listaZonasPeligro);
        conector.firePropertyChange("actualizarLista", 0, -1);
        mutexZP.release();
        actualizarLista();
    }
    public void eliminarZonaPeligro(Zona zona) throws InterruptedException, IOException{
        mutexZP.acquire();
        listaZonasPeligro.remove(zona);
        handlerZonasPeligro.simularCreacionDeZonasPeligro(listaZonasPeligro);
        conector.firePropertyChange("actualizarLista", 0, -1);
        mutexZP.release();
        actualizarLista();
    }
    public void annadirZona(Zona zona) throws InterruptedException, IOException{
        mutexZ.acquire();
        listaZonas.add(zona);
        mutexZ.release();
        handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
        mutexMapa.acquire();
        if(!mapaPezCola.containsKey(zona.getPez())){
            mapaPezCola.put(zona.getPez(), new Semaphore(1,true));    
        }else{
            int cont=0;
            for(Zona z:listaZonas){
                if(z.getPez().equals((zona.getPez())) && z.getMaxPeces()>=100){
                    cont++;
                }
            }
            if(cont==1){
                mapaPezCola.get(zona.getPez()).release();
            }
        }
        listaZonasEnFalta.remove(zona.getPez());
        mutexMapa.release();
        handlerZonas.simularCreacionDeZonas(listaZonas);
        actualizarLista();
    }
    public void eliminarZona(Zona zonaElim) throws InterruptedException, IOException{
        
        int i=0;
        mutexZ.acquire();
        for(Zona zona:listaZonas){
            if(zonaElim.getPez().equals(zona.getPez())){
                i++;
            }
        }
        if(i==1 && !mapaPezCola.get(zonaElim.getPez()).hasQueuedThreads()){
            mapaPezCola.remove(zonaElim.getPez());
        }
        listaZonas.remove(zonaElim);
        mutexZ.release();
        handlerZonas.simularCreacionDeZonas(listaZonas);
        actualizarLista();
    }

    public void revisarEstadosZonas() throws InterruptedException, IOException {
        List<Zona> listaAeliminar=new ArrayList<>();
        List<String> listaDuplicada=new ArrayList<>(listaZonasEnFalta);
        if(listaDuplicada.size()!=0){
        for(String s:listaDuplicada){
            annadirZona(new Zona(contadorIdZonas++,new Posicion(43.7f+random.nextFloat()*2,-1.7038f-random.nextFloat()*6),random.nextInt(5000)+5000,"Bien",s,random.nextInt(10)*100+1000));
        }
        listaZonasEnFalta=new ArrayList<>();
        handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
        }
        for(Zona z:listaZonas){
            if(z.getMaxPeces()<100){
                listaAeliminar.add(z);
            }
        }
        for(Zona z:listaAeliminar){
            this.eliminarZona(z);
        }
        
    }
    public void verZonasEnFalta(){
        handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
    }
    

    public void verZonas() throws IOException {
        handlerZonas.dibujarZonasFalta(listaZonasEnFalta);
        handlerZonas.simularCreacionDeZonas(listaZonas);
    }
}
