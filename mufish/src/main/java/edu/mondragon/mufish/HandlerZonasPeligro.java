package edu.mondragon.mufish;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.mondragon.mufish.simulacion.ControladorDeDibujables;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Zona;



@Component
public class HandlerZonasPeligro extends TextWebSocketHandler {
	ControladorDeZonas contZonas;
	public void setContZonas(ControladorDeZonas contZonas) {
		this.contZonas = contZonas;
	}

	List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
	ControladorDeDibujables controlador=new ControladorDeDibujables();
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {
	}
	public synchronized void simularCreacionDeZonasPeligro(List<Zona> listaZonas) throws IOException{
		String str2=controlador.listaZonasToString(listaZonas);
		
		for(int i=0;i<sessions.size();i++) {
			WebSocketSession webSocketSession =(WebSocketSession) sessions.get(i);
			webSocketSession.sendMessage(new TextMessage(str2));
		}
	}
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {

		sessions.add(session);
		simularCreacionDeZonasPeligro(contZonas.getListaZonasPeligro()); 
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		// TODO Auto-generated method stub
		sessions.remove(session);
	}
	
}