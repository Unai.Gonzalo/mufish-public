package edu.mondragon.mufish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MufishApplication {

	public static void main(String[] args) {
		SpringApplication.run(MufishApplication.class, args);
	}
}
