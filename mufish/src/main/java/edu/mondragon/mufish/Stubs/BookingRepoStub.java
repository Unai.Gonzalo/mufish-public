package edu.mondragon.mufish.Stubs;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.Booking;
import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Zone;
import edu.mondragon.mufish.Repositories.BookingRepository;

public class BookingRepoStub implements BookingRepository{
	
	List<Booking> bookings;
	
	public BookingRepoStub() {
		bookings = new ArrayList<>();
		bookings.add(new Booking(5,new Date(0), "Prueba", 5, new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")));
		bookings.add(new Booking(6,new Date(0), "Prueba", 5, new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")));
		bookings.add(new Booking(7,new Date(0), "Prueba", 5, new Zone(1, new Sea(5, "Prueba"), 5, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1), new Boat(5, "Prueba", new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1), new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), 1, 1, "Prueba")));
    }

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Booking> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<Booking> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Booking getOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Booking getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Booking getReferenceById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Booking> findAll() {
		// TODO Auto-generated method stub
		return bookings;
	}

	@Override
	public List<Booking> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> S save(S entity) {
		// TODO Auto-generated method stub
		Booking booking = new Booking(8,entity.getDate(), entity.getObjective(), entity.getQuantity(), entity.getZone(), entity.getBoat());
        bookings.add(booking);
        return (S) booking;
	}

	@Override
	public Optional<Booking> findById(Integer id) {
		// TODO Auto-generated method stub
		Optional<Booking> opt = Optional.empty();
        for(Booking booking: bookings){
            if(booking.getBookingId().equals(id)){
                opt = Optional.of(booking);
            }
        }
        return opt;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Booking entity) {
		// TODO Auto-generated method stub
		bookings.remove(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends Booking> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Booking> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Booking> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public <S extends Booking> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Booking> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Booking> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <S extends Booking, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		// TODO Auto-generated method stub
		return null;
	}

}
