package edu.mondragon.mufish.Stubs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;

import edu.mondragon.mufish.Models.Fish;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Models.State;
import edu.mondragon.mufish.Models.Zone;
import edu.mondragon.mufish.Repositories.ZoneRepository;

public class ZoneRepoStub implements ZoneRepository{
	
	List<Zone> zones;
	
	public ZoneRepoStub() {
		zones = new ArrayList<>();
		zones.add(new Zone(5, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
		zones.add(new Zone(6, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
		zones.add(new Zone(7, new Sea(5, "Prueba"), 1, new State(5, "Prueba"), new Fish(5, "Prueba"), 1, 1, 1));
    }

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Zone> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<Zone> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Zone getOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Zone getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Zone getReferenceById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Zone> findAll() {
		// TODO Auto-generated method stub
		return zones;
	}

	@Override
	public List<Zone> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> S save(S entity) {
		// TODO Auto-generated method stub
		Zone zone = new Zone(8, entity.getSea(), entity.getFishQuantity(), entity.getState(), entity.getFish(), entity.getPosX(), entity.getPosY(), entity.getRadio());
		zones.add(zone);
        return (S) zone;
	}

	@Override
	public Optional<Zone> findById(Integer id) {
		// TODO Auto-generated method stub
		Optional<Zone> opt = Optional.empty();
        for(Zone zone: zones){
            if(zone.getZoneId().equals(id)){
                opt = Optional.of(zone);
            }
        }
        return opt;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Zone entity) {
		// TODO Auto-generated method stub
		zones.remove(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends Zone> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Zone> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Zone> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public <S extends Zone> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Zone> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Zone> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <S extends Zone, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		// TODO Auto-generated method stub
		return null;
	}

}
