package edu.mondragon.mufish.Stubs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;

import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Repositories.UserRepository;

public class UserRepoStub implements UserRepository{
	
	List<User> users;
	
	public UserRepoStub() {
		users = new ArrayList<>();
		users.add(new User(1, "user123", "5vlP9JfDQUTbBw+5jIYsIngzqt1lM+Y1TbV2Av7AK9o=", "ErtJA0Ntxmiin3Ihq0TXn8Aovbu37L", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
		users.add(new User(5, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
		users.add(new User(6, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
		users.add(new User(7, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, "Prueba", "Prueba"));
    }

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends User> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<User> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getReferenceById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return users;
	}

	@Override
	public List<User> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> S save(S entity) {
		// TODO Auto-generated method stub
		User user = new User(8, entity.getUsername(), entity.getPassword(), entity.getSalt(), entity.getNan(), entity.getType(), entity.getPortId(), entity.getInstitution(), entity.getLicense());
		users.add(user);
        return (S) user;
	}

	@Override
	public Optional<User> findById(Integer id) {
		// TODO Auto-generated method stub
		Optional<User> opt = Optional.empty();
        for(User user: users){
            if(user.getUserId().equals(id)){
                opt = Optional.of(user);
            }
        }
        return opt;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(User entity) {
		// TODO Auto-generated method stub
		users.remove(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends User> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<User> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<User> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends User> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <S extends User, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByUsernameAndPassword(String username, String password) {
		// TODO Auto-generated method stub
		List<User> foundUsers = new ArrayList<>();
        for(User user: users){
            if(user.getUsername().equals(username) && user.getPassword().equals(password)){
            	foundUsers.add(user);
            }
        }
        return foundUsers;
	}

	@Override
	public List<User> findByUsername(String username) {
		// TODO Auto-generated method stub
		List<User> foundUsers = new ArrayList<>();
        for(User user: users){
            if(user.getUsername().equals(username)){
            	foundUsers.add(user);
            }
        }
        return foundUsers;
	}

	@Override
	public List<User> findUserByType(Type type) {
		// TODO Auto-generated method stub
		List<User> foundUsers = new ArrayList<>();
        for(User user: users){
            if(user.getType().equals(type)){
            	foundUsers.add(user);
            }
        }
        return foundUsers;
	}

}
