package edu.mondragon.mufish.Stubs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;

import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.Type;
import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Repositories.MessageRepository;

public class MessageRepoStub implements MessageRepository{
	
	List<Message> messages;
	
	public MessageRepoStub() {
		messages = new ArrayList<>();
		messages.add(new Message(5, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		messages.add(new Message(6, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
		messages.add(new Message(7, new User(1, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "prueba"), 5, null, null), new User(2, "Prueba", "Prueba", "Prueba", "Prueba", new Type(5, "Prueba"), 5, null, null), "Prueba", "Prueba", 0));
    }

	@Override
	public void flush() {
		// TODO Auto-generated method stub
	}

	@Override
	public <S extends Message> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<Message> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Message getOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Message getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Message getReferenceById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> findAll() {
		// TODO Auto-generated method stub
		return messages;
	}

	@Override
	public List<Message> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> S save(S entity) {
		// TODO Auto-generated method stub
		Message message = new Message(8, entity.getSender(), entity.getReceiver(), entity.getNotification(), entity.getTitle(), entity.getStateRead());
		messages.add(message);
        return (S) message;
	}

	@Override
	public Optional<Message> findById(Integer id) {
		// TODO Auto-generated method stub
		Optional<Message> opt = Optional.empty();
        for(Message message: messages){
            if(message.getMessageId().equals(id)){
                opt = Optional.of(message);
            }
        }
        return opt;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Message entity) {
		// TODO Auto-generated method stub
		messages.remove(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends Message> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Message> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Message> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public <S extends Message> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Message> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Message> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <S extends Message, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> findBySender(User user) {
		// TODO Auto-generated method stub
		List<Message> lista = new ArrayList<>();
		for(Message message: messages) {
			if(message.getSender().equals(user)) {
				lista.add(message);
			}
		}
		return lista;
	}

	@Override
	public List<Message> findByReceiver(User user) {
		// TODO Auto-generated method stub
		List<Message> lista = new ArrayList<>();
		for(Message message: messages) {
			if(message.getReceiver().equals(user)) {
				lista.add(message);
			}
		}
		return lista;
	}

}
