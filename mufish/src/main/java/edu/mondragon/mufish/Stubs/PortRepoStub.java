package edu.mondragon.mufish.Stubs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;

import edu.mondragon.mufish.Models.Port;
import edu.mondragon.mufish.Models.Sea;
import edu.mondragon.mufish.Repositories.PortRepository;

public class PortRepoStub implements PortRepository{
	
	List<Port> ports;
	
	public PortRepoStub() {
		ports = new ArrayList<>();
		ports.add(new Port(1, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1));
		ports.add(new Port(5, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1));
		ports.add(new Port(6, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1));
		ports.add(new Port(7, "Prueba", "Prueba", new Sea(5, "Prueba"), 1, 1));
    }

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Port> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<Port> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Port getOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Port getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Port getReferenceById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Port> findAll() {
		// TODO Auto-generated method stub
		return ports;
	}

	@Override
	public List<Port> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> S save(S entity) {
		// TODO Auto-generated method stub
		Port port = new Port(8, entity.getName(), entity.getCity(), entity.getSea(), entity.getPosX(), entity.getPosY());
		ports.add(port);
        return (S) port;
	}

	@Override
	public Optional<Port> findById(Integer id) {
		// TODO Auto-generated method stub
		Optional<Port> opt = Optional.empty();
        for(Port port: ports){
            if(port.getPortId().equals(id)){
                opt = Optional.of(port);
            }
        }
        return opt;
	}

	@Override
	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Port entity) {
		// TODO Auto-generated method stub
		ports.remove(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends Port> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Port> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Port> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	@Override
	public <S extends Port> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Port> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Port> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <S extends Port, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		// TODO Auto-generated method stub
		return null;
	}

}
