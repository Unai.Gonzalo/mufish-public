package edu.mondragon.mufish;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.mondragon.mufish.simulacion.ControladorDeDibujables;
import edu.mondragon.mufish.simulacion.ControladorDeZonas;



@Component
public class HandlerGuardacostas extends TextWebSocketHandler {
	ControladorDeZonas contZonas;
	public void setContZonas(ControladorDeZonas contZonas) {
		this.contZonas = contZonas;
	}

	static List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
	static ControladorDeDibujables controlador=new ControladorDeDibujables();
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {
				String str="";
		if(message.getPayload().charAt(0)=='B'){
			str=controlador.cambiarListaBarcos(message.getPayload());
		}
		for(int i=0;i<sessions.size();i++) {
			WebSocketSession webSocketSession =(WebSocketSession) sessions.get(i);
				webSocketSession.sendMessage(new TextMessage(str));
		}
	}
	public static synchronized void simularActuBarco(String str) throws IOException{
		
		String str2=controlador.cambiarListaBarcos(str);
		for(int i=0;i<sessions.size();i++) {
			WebSocketSession webSocketSession =(WebSocketSession) sessions.get(i);
			
			webSocketSession.sendMessage(new TextMessage(str2));
		}
	}
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {

		sessions.add(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		// TODO Auto-generated method stub
		sessions.remove(session);
	}
	
}