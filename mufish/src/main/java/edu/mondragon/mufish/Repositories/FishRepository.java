package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Fish;

public interface FishRepository extends JpaRepository<Fish, Integer>{ 
}
