package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Sea;
import java.util.Optional;

public interface SeaRepository extends JpaRepository<Sea, Integer>{
}
