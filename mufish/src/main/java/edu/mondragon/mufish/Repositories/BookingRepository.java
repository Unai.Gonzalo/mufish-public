package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Booking;


public interface BookingRepository extends JpaRepository<Booking, Integer>{ 
}
