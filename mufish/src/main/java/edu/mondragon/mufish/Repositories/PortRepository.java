package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Port;
import java.util.Optional;

public interface PortRepository extends JpaRepository<Port, Integer>{ 
}
