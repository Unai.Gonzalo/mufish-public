package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Type;
import java.util.Optional;

public interface TypeRepository extends JpaRepository<Type, Integer>{ 
}
