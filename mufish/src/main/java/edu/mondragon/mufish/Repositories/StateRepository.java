package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.State;

public interface StateRepository extends JpaRepository<State, Integer>{ 
}