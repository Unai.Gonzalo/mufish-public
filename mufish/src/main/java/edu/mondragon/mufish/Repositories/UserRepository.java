package edu.mondragon.mufish.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.User;
import edu.mondragon.mufish.Models.Type;

public interface UserRepository extends JpaRepository<User, Integer>{ 
    List<User> findByUsernameAndPassword(String username, String password);
    List<User> findByUsername(String username);
    List<User> findUserByType(Type type);
}
