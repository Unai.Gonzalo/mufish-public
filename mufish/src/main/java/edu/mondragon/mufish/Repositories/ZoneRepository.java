package edu.mondragon.mufish.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Zone;

public interface ZoneRepository extends JpaRepository<Zone, Integer>{ 
}
