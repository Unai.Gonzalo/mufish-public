package edu.mondragon.mufish.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Message;
import edu.mondragon.mufish.Models.User;

public interface MessageRepository extends JpaRepository<Message, Integer>{ 
    List<Message> findBySender(User user);
    List<Message> findByReceiver(User user);
}
