package edu.mondragon.mufish.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.mondragon.mufish.Models.Boat;
import edu.mondragon.mufish.Models.User;

public interface BoatRepository extends JpaRepository<Boat, Integer>{ 

    public List<Boat> findByFisher(User fisher);

}
