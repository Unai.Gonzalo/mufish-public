package edu.mondragon.mufish;
import java.io.IOException;
import java.util.Random;

import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Zona;



public class ThreadUsuarioZona extends Thread {

    Random random= new Random();
    String function;
    ControladorDeZonas contz;
    Zona zona;
    public ThreadUsuarioZona(Zona zona, String function){
        this.zona=zona;
        this.function = function;
        contz=webSocketConfig.getControladorDeZonas();
        this.start();
    }

    @Override
    public void run() { 
        try {
            if(function.equals("Create_Fishing_Zone")){
                contz.annadirZona(zona);
            }else if(function.equals("Delete_Fishing_Zone")){
                contz.eliminarZona(zona);
            }else if(function.equals("Create_Risk_Zone")){
                contz.annadirZonaPeligro(zona);
            }else if(function.equals("Delete_Risk_Zone")){
                contz.eliminarZonaPeligro(zona);
            }
            
        } catch (InterruptedException e) {
            this.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

