package edu.mondragon.mufish;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;

import edu.mondragon.mufish.simulacion.ControladorDeZonas;
import edu.mondragon.mufish.simulacion.Zona;


public class ThreadUsuario extends Thread   {

    Random random= new Random(); 
    Integer id;
    String pez;
    Integer cantidad;
    Integer barco;
    PropertyChangeSupport conector;
    public ThreadUsuario(Integer id, String pez, Integer cantidad,Integer barco){
        super("Usuario "+id);
        this.id=id;
        this.pez=pez;
        this.barco=barco;
        this.cantidad=cantidad;
        this.start();
    }

    @Override
    public void run() {
        try {
            Zona z=null;
            System.out.println(pez+" "+cantidad);
            while (!isInterrupted() && z==null) {
                z=webSocketConfig.getControladorDeZonas().pedirZonaUsuario(pez,cantidad);
            }
            webSocketConfig.getControladorDeZonas().guardarZonaPorBarcoDeUsuario(id,barco,z);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}

